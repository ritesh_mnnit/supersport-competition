﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSport.CompetitionLib.Exceptions
{
    public class ValidationException: Exception
    {
        public List<ValidationMessage> ValidationList { get; set; }
        public ValidationException() : base() {}
        public ValidationException(string s) : base(s) {}
        public ValidationException(string s, Exception ex) : base(s, ex) { }

        public ValidationException(List<ValidationMessage> validationList)
            : base("There were some errors with your request, please see the errors list included with this exception.")
        {
            ValidationList = validationList;
        }

    }
}
