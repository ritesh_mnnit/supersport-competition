﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SuperSport.CompetitionLib.Exceptions
{
    public class ValidationMessage
    {
        public string ErrorMessage { get; set; }
        public string Code { get; set; }

        public ValidationMessage(string errorMessage, string code)
        {
            this.Code = code;
            this.ErrorMessage = errorMessage;
        }

        public override string ToString()
        {
            return string.Format("Error {0}: {1}", Code, ErrorMessage);
        }
    }
}
