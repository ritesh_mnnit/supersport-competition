﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Api.DI;
using NUnit.Framework;
using StructureMap;

namespace Test
{
    [TestFixture]
    public class DITests
    {
        [Test]
        public void Test()
        {
            StructureMapContainer.Initialise();
            ObjectFactory.AssertConfigurationIsValid();
        }
    }
}
