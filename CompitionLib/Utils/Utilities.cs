﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Text.RegularExpressions;

namespace SuperSport.CompetitionLib.Utils
{
    internal static class Utilities
    {
        /// <summary>
        /// Strips all none digit characters from a string and returns the new string
        /// </summary>
        /// <param name="value">string that will be stripped</param>
        /// <returns>stripped string</returns>
        internal static string NumberCleanup(string value)
        {
            string pattern = @"[^a-zA-Z0-9]";
            string tmpString = Regex.Replace(value, pattern, "");

            return tmpString;
        }
    }
}
