﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.Models;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    class CompetitionStatsDAL
    {
        private const string SQL_STATS_TOTAL_ENTRIES = "SELECT COUNT(*) AS TotalEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id)#countryQuery";
        private const string SQL_STATS_UNIQUE_ENTRIES = "SELECT COUNT(DISTINCT contactNo) AS UniqueEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id)#countryQuery";
        private const string SQL_STATS_OPTIN_ENTRIES = "SELECT COUNT(*) AS OptInEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id) AND (receive_marketing = 1)#countryQuery";
        private const string SQL_STATS_UNIQUE_OPTIN_ENTRIES = "SELECT COUNT (DISTINCT contactNo) AS UniqueOptInEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id) AND (receive_marketing = 1)#countryQuery";
        private const string SQL_STATS_TOTAL_SOURCE_ENTRIES = "SELECT COUNT(*) AS WebEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id) AND (source=@source)#countryQuery";
        private const string SQL_STATS_UNIQUE_SOURCE_ENTRIES = "SELECT COUNT(DISTINCT contactNo) AS UniqueWebEntries FROM SuperSport.dbo.comp_entries WHERE (date_entered BETWEEN @startDate AND @endDate) AND (comp_id=@comp_id) AND (source=@source)#countryQuery";
        private const string SQL_STATS_MAIN_COUNTRIES = "SELECT TOP 10 COUNT(a.Id) As Total, a.Country_code, b.name FROM SuperSport.dbo.comp_entries a INNER JOIN SuperSport.dbo.comp_countrycodes b ON b.code = a.country_code WHERE (a.comp_id=@comp_id) AND (a.date_entered BETWEEN @startDate AND @endDate) GROUP BY a.Country_code, b.name ORDER BY Total DESC";

        public CompetitionStats GetStats(int competitionId, DateTime fromDate, DateTime toDate, string countryCode)
        {
            CompetitionStats stats = new CompetitionStats();

            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@comp_id", competitionId));
            parm.Add(new SqlParameter("@startDate", fromDate));
            parm.Add(new SqlParameter("@endDate", toDate));

            string countryCodeQuery = string.Empty;

            if (!String.IsNullOrEmpty(countryCode))
            {
                countryCodeQuery = " AND (country_code = '" + countryCode + "')";
            }

            //Total entries
            stats.TotalEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_TOTAL_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));
            stats.UniqueEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_UNIQUE_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));
            stats.OptInEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_OPTIN_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));
            stats.UniqueOptInEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_UNIQUE_OPTIN_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));

            if (String.IsNullOrEmpty(countryCode))
            {
                using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_MAIN_COUNTRIES.ToString(), parm.ToArray()))
                {
                    if (rdr.HasRows)
                    {
                        stats.mainCountries = new List<Country>();
                    }
                    while (rdr.Read())
                    {
                        Country country = new Country();
                        country.Id = Convert.ToInt32(rdr["Total"]);
                        country.CountryCode = rdr["country_code"].ToString();
                        country.CountryName = rdr["name"].ToString();

                        stats.mainCountries.Add(country);
                    }
                }
            }

            parm.Add(new SqlParameter("@source", Convert.ToInt32(CompetitionEntry.EntryLocation.Web)));
            stats.WebEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_TOTAL_SOURCE_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));
            stats.UniqueWebEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_UNIQUE_SOURCE_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));

            parm.RemoveAt(parm.Count - 1);
            parm.Add(new SqlParameter("@source", Convert.ToInt32(CompetitionEntry.EntryLocation.Mobile)));
            stats.MobileEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_TOTAL_SOURCE_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));
            stats.UniqueMobileEntries = Convert.ToInt32(SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_STATS_UNIQUE_SOURCE_ENTRIES.Replace("#countryQuery", countryCodeQuery), parm.ToArray()));

            return stats;
        }
    }
}