﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.Models;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    class CompetitionWinnerDAL
    {
        private const string SQL_DRAW_WINNER = "SELECT Top 1 id, comp_id, question_id FROM SuperSport.dbo.comp_entries WHERE comp_id = @comp_id AND (date_entered BETWEEN @startdate AND @enddate) ";
        private const string SQL_DRAW_WINNER_ANSWER = " AND (answer = @answer) ";
        private const string SQL_DRAW_WINNER_OPT_IN = " AND (receive_marketing = 1) ";
        private const string SQL_DRAW_WINNER_EXCLUSION_LIST = " AND ((idnumber != #idnumber) OR (email != #email)) ";
        private const string SQL_DRAW_WINNER_RANDOMIZER = " ORDER BY NEWID()";

        private const string SQL_UPDATE_WINNER = "UPDATE SuperSport.dbo.comp_CompetitionWinner set comment=@comment, claimedWinner=@claimedWinner WHERE id=@id";
        private const string SQL_SAVE_WINNER = "INSERT INTO SuperSport.dbo.comp_CompetitionWinner (CompetitionId, EntryId, QuestionId, ClaimedWinner, Comment) VALUES (@CompetitionId, @EntryId, @QuestionId, @ClaimedWinner, @Comment); SELECT @@IDENTITY AS 'Identity';";

        private const string SQL_SELECT_COMPETITION_WINNERS = "SELECT * FROM SuperSport.dbo.comp_CompetitionWinner WHERE competitionid=@competitionid ORDER BY createdDate DESC";

        private const string SQL_SELECT_EXCLUSTION_LIST = "SELECT * FROM SuperSport.dbo.comp_CompetitionWinner WHERE (createdDate > DATEADD(MONTH,-3,GETDATE())) AND (claimedWinner = 1)";

        private const string SQL_SELECT_PREVIOUSCOMPETITION_WINNERS = "SELECT C.comp AS Competition ,CE.[name]+' ' +CE.[surname] AS Winners, C.active, CE.date_entered, CW.updatedDate as CompetitionWinnerUpdatedDate FROM SuperSport.dbo.comp_CompetitionWinner CW inner join SuperSport.dbo.[comp_entries] CE on CW.entryId = CE.id inner join SuperSport.dbo.comp_competitions C ON C.id = CW.competitionId inner join SuperSport.dbo.comp_questions CQ on CQ.id = CW.questionId WHERE year(CW.createdDate) = @year AND(claimedWinner = 1) and year(CE.date_entered) = @year order by CE.date_entered desc";

        private const string SQL_SELECT_Optin_WINNERS = "select e.name,e.surname,e.contactno as contactnumber,e.idnumber,e.email,e.country_code,e.city,e.answer,e.connectid,e.date_entered as Dateentered from supersport.dbo.comp_entries e  where e.id in (select min(id) from supersport.dbo.comp_entries where receive_marketing=1 and  date_entered >= @startdate and date_entered <= @enddate and comp_id = @id group by idnumber,comp_id)";

        private const string SQL_SELECT_MultichoiceOpt_WINNERS = "select e.name,e.surname,e.contactno as contactnumber,e.idnumber,e.email,e.country_code,e.city,e.answer,e.connectid,e.date_entered as Dateentered from supersport.dbo.comp_entries e  where e.id in (select min(id) from supersport.dbo.comp_entries where multichoice_marketing=1 and  date_entered >= @startdate and date_entered <= @enddate and comp_id = @id group by idnumber,comp_id)";

        public List<CompetitionWinner> DrawWinner(int competitionId, int numberOfWinners, DateTime startDate, DateTime endDate, string answer, bool optInRequired)
        {
            List<CompetitionWinner> winners = new List<CompetitionWinner>();

            List<CompetitionWinner> exclustionList = new List<CompetitionWinner>();
            exclustionList = this.GetWinnerExclustionList();

            int i = 0;
            do
            {
                //build query
                StringBuilder cmd = new StringBuilder();
                cmd.Append(SQL_DRAW_WINNER);

                //draw a list of winners from the list of entries
                List<SqlParameter> parmList = new List<SqlParameter>();

                parmList.Add(new SqlParameter("@comp_id", competitionId));
                parmList.Add(new SqlParameter("@startdate", startDate));
                parmList.Add(new SqlParameter("@enddate", endDate));

                if (!string.IsNullOrEmpty(answer))
                {
                    cmd.Append(SQL_DRAW_WINNER_ANSWER);
                    parmList.Add(new SqlParameter("@answer", answer));
                }

                if (optInRequired)
                {
                    cmd.Append(SQL_DRAW_WINNER_OPT_IN);
                }

                foreach (CompetitionWinner exclude in winners)
                {
                    cmd.Append(SQL_DRAW_WINNER_EXCLUSION_LIST.Replace("#idnumber", "'" + exclude.EntryDetails.IdNumber + "'").Replace("#email", "'" + exclude.EntryDetails.Email + "'"));
                }

                foreach (CompetitionWinner exclude in exclustionList)
                {
                    cmd.Append(SQL_DRAW_WINNER_EXCLUSION_LIST.Replace("#idnumber", "'" + exclude.EntryDetails.IdNumber + "'").Replace("#email", "'" + exclude.EntryDetails.Email + "'"));
                }

                cmd.Append(SQL_DRAW_WINNER_RANDOMIZER);

                string test = cmd.ToString();

                //Execute a query to read the competition details/draw winners
                using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, cmd.ToString(), parmList.ToArray()))
                {
                    while (rdr.Read())
                    {
                        CompetitionWinner winner = new CompetitionWinner();
                        winner.ClaimedWinner = false;
                        winner.Comment = string.Empty;
                        winner.CompetitionId = Convert.ToInt32(rdr["comp_id"]);
                        winner.EntryId = Convert.ToInt32(rdr["id"]);
                        winner.QuestionId = Convert.ToInt32(rdr["question_id"]);
                        winner.CreatedDate = DateTime.Now;
                        //save the winners
                        winner.Id = this.saveWinner(winner);

                        winners.Add(winner);
                    }
                }

                i++;
            }
            while (i < numberOfWinners);

            return winners;
        }

        private int saveWinner(CompetitionWinner winner)
        {
            int id = 0;

            //draw a list of winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            parmList.Add(new SqlParameter("@CompetitionId", winner.CompetitionId));
            parmList.Add(new SqlParameter("@EntryId", winner.EntryId));
            parmList.Add(new SqlParameter("@QuestionId", winner.QuestionId));
            parmList.Add(new SqlParameter("@ClaimedWinner", winner.ClaimedWinner));
            parmList.Add(new SqlParameter("@Comment", winner.Comment));

            object winnerId = null;

            winnerId = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_SAVE_WINNER, parmList.ToArray());

            if ((winnerId != null) && (winnerId != DBNull.Value))
            {
                id = Convert.ToInt32(winnerId);
            }

            return id;
        }

        public bool UpdateWinner(int winnerId, bool claimedWinner, string comment)
        {
            bool success = true;

            //draw a list of winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            parmList.Add(new SqlParameter("@id", winnerId));
            parmList.Add(new SqlParameter("@claimedWinner", claimedWinner));
            parmList.Add(new SqlParameter("@comment", comment));
            
            SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_UPDATE_WINNER, parmList.ToArray());

            return success;
        }

        public List<CompetitionWinner> GetWinners(int competitionId)
        {
            List<CompetitionWinner> winners = new List<CompetitionWinner>();

            //draw a list of winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            parmList.Add(new SqlParameter("@CompetitionId", competitionId));

            //Execute a query to read the competition details/draw winners
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COMPETITION_WINNERS, parmList.ToArray()))
            {
                while (rdr.Read())
                {
                    CompetitionWinner winner = new CompetitionWinner();
                    winner.ClaimedWinner = Convert.ToBoolean(rdr["claimedWinner"]);
                    winner.Comment = Convert.ToString(rdr["comment"]);
                    winner.CompetitionId = Convert.ToInt32(rdr["competitionId"]);
                    winner.EntryId = Convert.ToInt32(rdr["entryId"]);
                    winner.QuestionId = Convert.ToInt32(rdr["questionId"]);
                    winner.Id = Convert.ToInt32(rdr["id"]);
                    winner.CreatedDate = Convert.ToDateTime(rdr["createdDate"]);
                    winner.UpdatedDate = Convert.ToDateTime(rdr["updatedDate"]);

                    winners.Add(winner);
                }
            }

            return winners;
        }

        private List<CompetitionWinner> GetWinnerExclustionList()
        {
            List<CompetitionWinner> winners = new List<CompetitionWinner>();

            //draw a list of winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            //Execute a query to read the competition details/draw winners
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_EXCLUSTION_LIST, parmList.ToArray()))
            {
                while (rdr.Read())
                {
                    CompetitionWinner winner = new CompetitionWinner();
                    winner.ClaimedWinner = Convert.ToBoolean(rdr["claimedWinner"]);
                    winner.Comment = Convert.ToString(rdr["comment"]);
                    winner.CompetitionId = Convert.ToInt32(rdr["competitionId"]);
                    winner.EntryId = Convert.ToInt32(rdr["entryId"]);
                    winner.QuestionId = Convert.ToInt32(rdr["questionId"]);
                    winner.Id = Convert.ToInt32(rdr["id"]);
                    winner.CreatedDate = Convert.ToDateTime(rdr["createdDate"]);
                    winner.UpdatedDate = Convert.ToDateTime(rdr["updatedDate"]);

                    winners.Add(winner);
                }
            }

            return winners;
        }


        public List<PreviousCompetitionWinner> GetPreviousWinners(int year)
        {
            List<PreviousCompetitionWinner> previouswinners = new List<PreviousCompetitionWinner>();

            //////draw a list of winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            parmList.Add(new SqlParameter("@year", year));

            //////Execute a query to read the competition details/ draw winners
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_PREVIOUSCOMPETITION_WINNERS, parmList.ToArray()))
            {
                while (rdr.Read())
                {
                    PreviousCompetitionWinner pwinner = new PreviousCompetitionWinner();
                    pwinner.CompetitionName = Convert.ToString(rdr["Competition"]);
                    pwinner.PreviousWinners = Convert.ToString(rdr["Winners"]);
                    previouswinners.Add(pwinner);
                }
            }

            return previouswinners;
        }


        public DataTable GetoptWinners(int competitionId, DateTime startDate, DateTime endDate)
        {
            
            //draw a list of opt in winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();
                       
            parmList.Add(new SqlParameter("@startdate", startDate));
            parmList.Add(new SqlParameter("@enddate", endDate));
            parmList.Add(new SqlParameter("@id", competitionId));
            DataSet ds = new DataSet("New_DataSet");
            System.Data.DataTable dt = new DataTable("New_DataTable");
            

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_Optin_WINNERS, parmList.ToArray()))
            {
                dt.Load(rdr);
            }



            return dt;
        }


        public DataTable MultichoiceOpt(int competitionId, DateTime startDate, DateTime endDate)
        {

            //draw a list of opt in winners from the list of entries
            List<SqlParameter> parmList = new List<SqlParameter>();

            parmList.Add(new SqlParameter("@startdate", startDate));
            parmList.Add(new SqlParameter("@enddate", endDate));
            parmList.Add(new SqlParameter("@id", competitionId));
            DataSet ds = new DataSet("New_DataSet");
            System.Data.DataTable dt = new DataTable("New_DataTable");

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_MultichoiceOpt_WINNERS, parmList.ToArray()))
            {
                dt.Load(rdr);
            }



            return dt;
        }

    }
}