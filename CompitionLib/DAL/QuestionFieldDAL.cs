﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.DBUtility;
using SuperSport.CompetitionLib.Models;
using System.Data.SqlClient;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    public class QuestionFieldDAL
    {
        private const string SQL_SELECT_INPUT_FIELDS = "SELECT * FROM SuperSport.dbo.comp_questionFields";

        /// <summary>
        /// Gets all the input fields required for a question
        /// </summary>
        /// <returns></returns>
        public List<InputField> GetInputFields()
        {
            List<InputField> fields = new List<InputField>();

            List<SqlParameter> parms = new List<SqlParameter>();

            //Execute a query to read the question details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_INPUT_FIELDS, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    InputField field = new InputField();

                    field.Id = Convert.ToInt32(rdr["id"]); ;
                    field.FieldInputType = (InputField.FieldType)Enum.Parse(typeof(InputField.FieldType), rdr["fieldType"].ToString());
                    field.FieldText = rdr["fieldText"].ToString();
                    field.Required = true;

                    fields.Add(field);
                }
            }

            return fields;
        }
    }
}
