﻿using System;
using System.Collections.Generic;
using System.Text;
using SuperSport.CompetitionLib.Models;
using System.Data.SqlClient;
using SuperSport.DBUtility;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    class AnswerDAL
    {
        private const string SQL_SELECT_ANSWERLIST_BY_QUESTIONID = "SELECT * FROM SuperSport.dbo.comp_answers WHERE question_id=@quesId";
        private const string SQL_INSERT_ANSWER = "INSERT INTO SuperSport.dbo.comp_answers (question_id,answer,active, extendedText, isCorrect) VALUES (@question_id, @answer,@active, @extendedText, @isCorrect);SELECT @@IDENTITY AS 'Identity';";
        private const string SQL_UPDATE_ANSWER = "UPDATE SuperSport.dbo.comp_answers SET question_id=@question_id, answer=@answer, extendedText=@extendedText, active=@active, isCorrect=@isCorrect WHERE id=@id";
        private const string SQL_ANSWER_VOTES = "SELECT COUNT(a.Id) As Total FROM SuperSport.dbo.comp_entries a WHERE (a.question_id = @question_id AND answer = @answer)";

        /// <summary>
        /// Reads the defined answers for a question based on the unique question Id
        /// </summary>
        /// <param name="quesId">unique Answer Id</param>
        /// <returns>List of Answer Entities</returns>
        public List<Answer> GetAnswers(int quesId)
        {
            List<Answer> answers = new List<Answer>();

            List<SqlParameter> parms = new List<SqlParameter>();

            parms.Add(new SqlParameter("@quesId", quesId));

            //Execute a query to read the answer details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_ANSWERLIST_BY_QUESTIONID, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Answer ans = new Answer();

                    ans.Id = Convert.ToInt32(rdr["id"]);
                    ans.AnswerText = rdr["answer"].ToString();
                    ans.ExtendedText = rdr["extendedText"].ToString();
                    ans.Active = Convert.ToBoolean(rdr["active"]);
                    ans.QuestionId = quesId;
                    ans.isCorrect = Convert.ToBoolean(rdr["isCorrect"]);

                    answers.Add(ans);
                }
            }

            return answers;
        }

        /// <summary>
        /// Saves an answer entity to database and return sucecss result as boolean
        /// </summary>
        /// <param name="answer">Answer entity</param>
        /// <returns>bool, true if success, false if failed to save to database</returns>
        public int Save(Answer answer)
        {
            int id = 0;

            //save competition
            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@question_id", answer.QuestionId));
            sqlParms.Add(new SqlParameter("@answer", answer.AnswerText));
            sqlParms.Add(new SqlParameter("@id", answer.Id));
            sqlParms.Add(new SqlParameter("@active", answer.Active));
            sqlParms.Add(new SqlParameter("@extendedText", answer.ExtendedText));
            sqlParms.Add(new SqlParameter("@isCorrect", answer.isCorrect));

            //try update first
            if (SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_UPDATE_ANSWER, sqlParms.ToArray()) == 0)
            {
                sqlParms.Remove(new SqlParameter("@id", answer.Id));
                object compId = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_ANSWER, sqlParms.ToArray());
                if ((compId != null) && (compId != DBNull.Value))
                {
                    id = Convert.ToInt32(compId);
                }
            }
            else
            {
                id = answer.Id;
            }

            return id;
        }

        public int GetVotes(Answer answer)
        {
            int total = 0;

            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@question_id", answer.QuestionId));
            sqlParms.Add(new SqlParameter("@answer", answer.AnswerText));

            object answerTotal = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_ANSWER_VOTES, sqlParms.ToArray());
            total = Convert.ToInt32(answerTotal);

            return total;
        }
    }
}
