﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.Models;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    class QuestionDAL
    {
        private const string SQL_SELECT_QUESTIONLIST_BY_COMPID = "SELECT * FROM SuperSport.dbo.comp_questions WHERE comp_id=@compId";

        private const string SQL_INSERT_QUESTION = "INSERT INTO SuperSport.dbo.comp_questions (comp_id, question, questionFormat, date_start, date_end, active, requiredAnswersTotal) VALUES (@comp_id, @question, @questionFormat, @date_start, @date_end, @active, @requiredAnswersTotal);SELECT @@IDENTITY AS 'Identity';";
        private const string SQL_UPDATE_QUESTION = "UPDATE SuperSport.dbo.comp_questions SET comp_id=@comp_id, question=@question, questionFormat=@questionFormat, date_start=@date_start, date_end=@date_end, active=@active, requiredAnswersTotal=@requiredAnswersTotal WHERE id=@id";

        private static readonly AnswerDAL ansDAL = new AnswerDAL();


        /// <summary>
        /// Reads the questions for a competition based on the unique comp ID
        /// </summary>
        /// <param name="compId">unique comp ID</param>
        /// <returns>List of Question Entities</returns>
        public List<Question> GetQuestions(int compId)
        {
            List<Question> questions = new List<Question>();

            List<SqlParameter> parms = new List<SqlParameter>();

            parms.Add(new SqlParameter("@compId", compId));

            //Execute a query to read the question details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_QUESTIONLIST_BY_COMPID, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Question quest = new Question();

                    quest.Id = Convert.ToInt32(rdr["id"]);
                    quest.QuestionText = rdr["question"].ToString();
                    quest.DateStart = Convert.ToDateTime(rdr["date_start"]);
                    quest.DateEnd = Convert.ToDateTime(rdr["date_end"]);
                    quest.Active = Convert.ToBoolean(rdr["active"]);
                    quest.RequiredAnswersTotal = Convert.ToInt32(rdr["requiredAnswersTotal"]);
                    quest.QuestionFormat = (SuperSport.CompetitionLib.Models.Question.QuestionType)Enum.Parse(typeof(SuperSport.CompetitionLib.Models.Question.QuestionType), rdr["QuestionFormat"].ToString());

                    //get answers for question
                    quest.Answers = ansDAL.GetAnswers(quest.Id);


                    questions.Add(quest);
                }
            }

            return questions;
        }

        /// <summary>
        /// Saves a question to the database
        /// </summary>
        /// <param name="question">Question entity</param>
        /// <returns>Boolean, true if succes, false if failed to save to database</returns>
        public int Save(Question question)
        {
            int id = 0;

            //save competition
            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@comp_id", question.CompetitionId));
            sqlParms.Add(new SqlParameter("@question", question.QuestionText));
            sqlParms.Add(new SqlParameter("@questionFormat", question.QuestionFormat));
            sqlParms.Add(new SqlParameter("@date_start", question.DateStart));
            sqlParms.Add(new SqlParameter("@date_end", question.DateEnd));
            sqlParms.Add(new SqlParameter("@active", question.Active));
            sqlParms.Add(new SqlParameter("@requiredAnswersTotal", question.RequiredAnswersTotal));
            sqlParms.Add(new SqlParameter("@id", question.Id));

            //try update first
            if (SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_UPDATE_QUESTION, sqlParms.ToArray()) == 0)
            {
                sqlParms.Remove(new SqlParameter("@id", question.Id));
                object compId = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_QUESTION, sqlParms.ToArray());
                if ((compId != null) && (compId != DBNull.Value))
                {
                    id = Convert.ToInt32(compId);
                }
            }
            else
            {
                id = question.Id;
            }

            return id;
        }
    }
}
