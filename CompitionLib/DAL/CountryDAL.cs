﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.Models;
using System.Data.SqlClient;
using SuperSport.DBUtility;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    class CountryDAL
    {
        private const string SQL_INSERT_COMP_COUNTRIES = "INSERT INTO SuperSport.dbo.comp_countries (comp_id, country_code) VALUES (@comp_id, @country_code)";
        private const string SQL_DELETE_COMP_COUNTRIES = "DELETE FROM SuperSport.dbo.comp_countries WHERE comp_id=@comp_id";
        private const string SQL_SELECT_COMP_COUNTRIES = "SELECT a.*, b.Name FROM SuperSport.dbo.comp_countries a INNER JOIN SuperSport.dbo.comp_countrycodes b ON b.code = a.country_code WHERE a.comp_id=@comp_id";

        private const string SQL_SELECT_COUNTRIES = "SELECT * FROM SuperSport.dbo.comp_countryCodes";


        /// <summary>
        /// Reads all the countries available
        /// </summary>
        /// <returns>List of Country Entities</returns>
        public List<Country> GetCountries()
        {
            List<Country> countries = new List<Country>();

            //Create a parameter
            List<SqlParameter> parm = new List<SqlParameter>();

            //Execute a query to read the country details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COUNTRIES, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    Country country = new Country();

                    country.Id = Convert.ToInt32(rdr["id"]);
                    country.CountryName = Convert.ToString(rdr["name"]);
                    country.CountryCode = Convert.ToString(rdr["code"]);
                    country.IsSouthernAfrica = Convert.ToBoolean(rdr["saf"]);
                    country.IsSubSaharaCountry = Convert.ToBoolean(rdr["ssa"]);

                    if (country.CountryCode != "ZA")
                    {
                        countries.Add(country);
                    }
                    else
                    {
                        countries.Insert(0, country);
                    }
                }
            }

            return countries;
        }

        public List<Country> GetCompetitionCountries(int competitionId)
        {
            List<Country> compCountries = new List<Country>();

            //Create a parameter
            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@comp_id", competitionId));

            //Execute a query to read the country details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COMP_COUNTRIES, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Country country = new Country();

                    country.Id = Convert.ToInt32(rdr["id"]);
                    country.CountryCode = Convert.ToString(rdr["country_code"]);
                    country.CountryName = Convert.ToString(rdr["Name"]);

                    compCountries.Add(country);
                }
            }

            return compCountries;
        }

        public bool SaveCompetitionCountries(int competitionId, List<Country> countries)
        {
            bool success = true;

            //Create a parameter
            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@comp_id", competitionId));

            SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_DELETE_COMP_COUNTRIES, parms.ToArray());



            if (countries != null)
            {
                foreach (Country c in countries)
                {
                    parms = new List<SqlParameter>();
                    parms.Add(new SqlParameter("@comp_id", competitionId));
                    parms.Add(new SqlParameter("@country_code", c.CountryCode));
                    SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_COMP_COUNTRIES, parms.ToArray());
                }
            }

            return success;
        }
    }
}
