﻿using System;
using System.Collections.Generic;
using System.Text;
using SuperSport.CompetitionLib.Models;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;
using SuperSport.CompetitionLib.DAL;

namespace SuperSport.CompetitionLib.DAL
{
    class CompetitionDAL
    {
        //Implement queries here
        private const string SQL_SELECT_COMP_BY_ID = "SELECT * FROM SuperSport.dbo.comp_competitions WHERE id=@id";
        private const string SQL_SELECT_COMP_ID_BY_URLNAME = "SELECT TOP 1 id FROM SuperSport.dbo.comp_competitions WHERE urlname=@urlname";
        private const string SQL_SELECT_COMPLIST_BY_SEARCH = "SELECT a.comp_id,a.country_code, b.* FROM [SuperSport].[dbo].[comp_countries]  a INNER JOIN SuperSport.dbo.comp_competitions b ON b.id = a.comp_id WHERE comp like @compName";

        ////New change
        ////private const string SQL_INSERT_COMP = "INSERT INTO SuperSport.dbo.comp_competitions (comp,date_start,date_end,header,footer,header_mobi,footer_mobi,thumbnail,active,modified,modifiedby, urlname,exturl,extmobiurl,prizes,disclaimer,require_login,allow_multiple_entries_per_day,entry_response,optin_message, feature,marketing_optin,marketing_message, live_video_streaming_replacement, competition_sport, video_category_url) VALUES (@comp,@date_start,@date_end,@header,@footer,@header_mobi,@footer_mobi,@thumbnail,@active,@modified,@modifiedby,@url_name,@ext_url,@ext_mobiurl,@prizes,@disclaimer,@require_login,@allow_multiple_entries_per_day,@entry_response,@optin_message, @feature,@marketing_optin,@marketing_message, @live_video_streaming_replacement, @competition_sport, @video_category_url);SELECT @@IDENTITY AS 'Identity';";
        private const string SQL_INSERT_COMP = "INSERT INTO SuperSport.dbo.comp_competitions (comp,date_start,date_end,header,footer,header_mobi,footer_mobi,thumbnail,active,modified,modifiedby, urlname,exturl,extmobiurl,prizes,disclaimer,require_login,allow_multiple_entries_per_day,entry_response,optin_message, feature,marketing_optin,marketing_message, live_video_streaming_replacement, competition_sport, video_category_url,rich_media,multichoice_optin,mobile_thumbnail, ageverificationrequired) VALUES (@comp,@date_start,@date_end,@header,@footer,@header_mobi,@footer_mobi,@thumbnail,@active,@modified,@modifiedby,@url_name,@ext_url,@ext_mobiurl,@prizes,@disclaimer,@require_login,@allow_multiple_entries_per_day,@entry_response,@optin_message, @feature,@marketing_optin,@marketing_message, @live_video_streaming_replacement, @competition_sport, @video_category_url, @rich_media, @multichoice_optin, @mobile_thumbnail, @ageverificationrequired);SELECT @@IDENTITY AS 'Identity';";

        ////New change
        ////private const string SQL_UPDATE_COMP = "UPDATE SuperSport.dbo.comp_competitions SET comp=@comp,date_start=@date_start,date_end=@date_end,header=@header,footer=@footer,header_mobi=@header_mobi,footer_mobi=@footer_mobi,thumbnail=@thumbnail,active=@active,modified=@modified,modifiedby=@modifiedby,urlname=@url_name,exturl=@ext_url,extmobiurl=@ext_mobiurl,prizes=@prizes,disclaimer=@disclaimer,require_login=@require_login,allow_multiple_entries_per_day=@allow_multiple_entries_per_day, entry_response=@entry_response, optin_message=@optin_message, feature=@feature, marketing_optin=@marketing_optin, marketing_message=@marketing_message, live_video_streaming_replacement=@live_video_streaming_replacement, competition_sport=@competition_sport, video_category_url=@video_category_url WHERE id=@id";
        private const string SQL_UPDATE_COMP = "UPDATE SuperSport.dbo.comp_competitions SET comp=@comp,date_start=@date_start,date_end=@date_end,header=@header,footer=@footer,header_mobi=@header_mobi,footer_mobi=@footer_mobi,thumbnail=@thumbnail,active=@active,modified=@modified,modifiedby=@modifiedby,urlname=@url_name,exturl=@ext_url,extmobiurl=@ext_mobiurl,prizes=@prizes,disclaimer=@disclaimer,require_login=@require_login,allow_multiple_entries_per_day=@allow_multiple_entries_per_day, entry_response=@entry_response, optin_message=@optin_message, feature=@feature, marketing_optin=@marketing_optin, marketing_message=@marketing_message, live_video_streaming_replacement=@live_video_streaming_replacement, competition_sport=@competition_sport, video_category_url=@video_category_url, rich_media = @rich_media, multichoice_optin = @multichoice_optin, mobile_thumbnail = @mobile_thumbnail, ageverificationrequired = @ageverificationrequired WHERE id=@id";
        private const string SQL_SELECT_COUNT_COMP_URLNAME = "SELECT * FROM SuperSport.dbo.comp_competitions WHERE urlname=@urlname AND id != @id";
        private const string SQL_SELECT_FEATURES_COMPS = " SELECT a.*, b.* FROM [SuperSport].[dbo].[comp_countries]  a INNER JOIN SuperSport.dbo.comp_competitions b ON b.id = a.comp_id  WHERE (b.active = 1)  AND (a.country_code = @country)   ORDER BY a.Id";
        private const string SQL_SELECT_SPORTS = "SELECT * FROM SuperSportZone.dbo.zonesports";
        private static readonly QuestionDAL questDAL = new QuestionDAL();
        private static readonly AnswerDAL ansDAL = new AnswerDAL();
        private static readonly CountryDAL countryDAL = new CountryDAL();
        private static readonly QuestionFieldDAL questionFieldDAL = new QuestionFieldDAL();

        /// <summary>
        /// Saves (updates or inserts) a competition entity to database
        /// </summary>
        /// <param name="comp">Comepetition</param>
        /// <returns>unique id of saved competition</returns>
        public int Save(Competition competition)
        {
            int id = 0;

            //save competition
            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@id", competition.Id));
            sqlParms.Add(new SqlParameter("@comp", competition.Name));
            sqlParms.Add(new SqlParameter("@url_name", competition.UrlName));
            sqlParms.Add(new SqlParameter("@ext_url", competition.ExternalUrl));
            sqlParms.Add(new SqlParameter("@ext_mobiurl", competition.ExternalMobileUrl));
            sqlParms.Add(new SqlParameter("@date_start", competition.DateStart));
            sqlParms.Add(new SqlParameter("@date_end", competition.DateEnd));
            sqlParms.Add(new SqlParameter("@header", competition.HeaderImageWeb));
            sqlParms.Add(new SqlParameter("@footer", competition.FooterImageWeb));
            sqlParms.Add(new SqlParameter("@header_mobi", competition.HeaderImageMobi));
            sqlParms.Add(new SqlParameter("@footer_mobi", competition.FooterImageMobi));
            sqlParms.Add(new SqlParameter("@thumbnail", competition.ThumbNail));
            sqlParms.Add(new SqlParameter("@active", competition.Active));            
            sqlParms.Add(new SqlParameter("@modified", DateTime.Now));
            sqlParms.Add(new SqlParameter("@modifiedby", ""));            
            sqlParms.Add(new SqlParameter("@prizes", competition.Prizes));
            sqlParms.Add(new SqlParameter("@disclaimer", competition.Disclaimer));
            sqlParms.Add(new SqlParameter("@require_login", competition.RequireLogin));
            sqlParms.Add(new SqlParameter("@allow_multiple_entries_per_day", competition.AllowMultipleEntriesPerDay));
            sqlParms.Add(new SqlParameter("@entry_response", competition.EntryResponse));
            sqlParms.Add(new SqlParameter("@optin_message", competition.OptInMessage));
            sqlParms.Add(new SqlParameter("@feature", competition.Feature));
            sqlParms.Add(new SqlParameter("@marketing_message", competition.MarketingOptInMessage));
            sqlParms.Add(new SqlParameter("@marketing_optin", competition.HasMarketingOptIn));
            sqlParms.Add(new SqlParameter("@live_video_streaming_replacement", competition.LiveVideoStreamingReplacement));
            sqlParms.Add(new SqlParameter("@competition_sport", competition.Sport));
            sqlParms.Add(new SqlParameter("@video_category_url", competition.VideoCategoryUrl));
            sqlParms.Add(new SqlParameter("@multichoice_optin", competition.HasMultichoiceOptIn));
            sqlParms.Add(new SqlParameter("@rich_media", competition.HasRichMedia));
            sqlParms.Add(new SqlParameter("@mobile_thumbnail", competition.MobileThumbNail));
            sqlParms.Add(new SqlParameter("@ageverificationrequired", competition.AgeVerificationRequired));

            //try update first
            if (SqlHelper.ExecuteNonQuery(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_UPDATE_COMP, sqlParms.ToArray()) == 0)
            {
                sqlParms.Remove(new SqlParameter("@id", competition.Id));
                object compId = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_COMP, sqlParms.ToArray());
                if ((compId != null) && (compId != DBNull.Value))
                {
                    id = Convert.ToInt32(compId);
                }
            }
            else
            {
                id = competition.Id;
            }

            //save countries
            countryDAL.SaveCompetitionCountries(id, competition.OpenToCountries);       

            return id;
        }

        /// <summary>
        /// Reads a competition from database based on unique id
        /// </summary>
        /// <param name="id">unique competition id</param>
        /// <returns>Competition Entity</returns>
        public Competition GetCompetition(int id)
        {
            Competition comp = null;

            //Create a parameter
            SqlParameter parm = null;
            parm = new SqlParameter("@id", SqlDbType.Int);
            parm.Value = id;

            //Execute a query to read the competition details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COMP_BY_ID, parm))
            {
                while (rdr.Read())
                {
                    comp = new Competition();

                    comp.Id = Convert.ToInt32(rdr["Id"]);
                    comp.comp_id = Convert.ToInt32(rdr["Id"]);
                    comp.Name = Convert.ToString(rdr["comp"]);
                    comp.UrlName = Convert.ToString(rdr["urlname"]);
                    comp.ExternalUrl = Convert.ToString(rdr["exturl"]);
                    comp.ExternalMobileUrl = Convert.ToString(rdr["extmobiurl"]);
                    //comp.DateStart = Convert.ToDateTime(rdr["date_start"]);
                    //comp.DateEnd = Convert.ToDateTime(rdr["date_end"]);
                    comp.DateStartCheck = Convert.ToDateTime(rdr["date_start"]);
                    comp.DateStart = comp.DateStartCheck.ToShortDateString();
                    comp.DateEndCheck = Convert.ToDateTime(rdr["date_end"]);    
                    comp.DateEnd = comp.DateEndCheck.ToShortDateString();
                    comp.HeaderImageWeb = Convert.ToString(rdr["header"]);
                    comp.FooterImageWeb = Convert.ToString(rdr["footer"]);
                    comp.LiveVideoStreamingReplacement = Convert.ToString(rdr["live_video_streaming_replacement"]);
                    comp.HeaderImageMobi = Convert.ToString(rdr["header_mobi"]);
                    comp.FooterImageMobi = Convert.ToString(rdr["footer_mobi"]);
                    comp.ThumbNail = Convert.ToString(rdr["thumbnail"]);
                    comp.Prizes = Convert.ToString(rdr["prizes"]);
                    comp.Disclaimer = Convert.ToString(rdr["disclaimer"]);
                    comp.RequireLogin = Convert.ToBoolean(rdr["require_login"]);
                    comp.Active = Convert.ToBoolean(rdr["active"]);
                    comp.AllowMultipleEntriesPerDay = Convert.ToBoolean(rdr["allow_multiple_entries_per_day"]);
                    comp.EntryResponse = Convert.ToString(rdr["entry_response"]);
                    comp.OptInMessage = Convert.ToString(rdr["optin_message"]);
                    comp.Feature = Convert.ToBoolean(rdr["Feature"]);
                    comp.HasMarketingOptIn = Convert.ToBoolean(rdr["marketing_optin"]);
                    comp.MarketingOptInMessage = Convert.ToString(rdr["marketing_message"]);
                    comp.Sport = GetIntValue(rdr["competition_sport"]);
                    comp.VideoCategoryUrl = rdr["video_category_url"].ToString();
                    comp.HasMultichoiceOptIn = Convert.ToBoolean(rdr["multichoice_optin"]);
                    comp.HasRichMedia = Convert.ToBoolean(rdr["rich_media"]);
                    comp.MobileThumbNail = Convert.ToString(rdr["Mobile_thumbnail"]);
                    comp.AgeVerificationRequired = Convert.ToBoolean(rdr["ageverificationrequired"]);
                }
            }

            if (comp != null)
            {
                //get the questions for a competition
                comp.Questions = questDAL.GetQuestions(id);

                //get input fields
                comp.questionFields = questionFieldDAL.GetInputFields();

                //get countries assigned to competition
                comp.OpenToCountries = countryDAL.GetCompetitionCountries(comp.Id);
            }

            return comp;
        }

        /// <summary>
        /// Reads a competition from database based on unique friendly urlname
        /// </summary>
        /// <param name="urlname">unique competition urlname</param>
        /// <returns>Competition Entity</returns>
        public Competition GetCompetition(string urlname)
        {
            Competition comp = null;
            int compId = 0;

            //Create a parameter
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@urlname", urlname));

            //Execute a query to read the competition id
            object result = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COMP_ID_BY_URLNAME, parm.ToArray());

            if ((result != null) && (result != DBNull.Value))
            {
                compId = Convert.ToInt32(result);
            }
            comp = this.GetCompetition(compId);

            return comp;
        }

        /// <summary>
        /// Reads a list of competitions from database based on a user defined number
        /// </summary>
        /// <param name="maxNumber">Number of rows that needs to be returned</param>
        /// <returns></returns>
        public List<Competition> GetCompetitions(string searchVal, bool activeOnly)
        {
            List<Competition> comps = new List<Competition>();

            List<SqlParameter> parms = new List<SqlParameter>();

            parms.Add(new SqlParameter("@compName", "%" + searchVal + "%"));

            string query = SQL_SELECT_COMPLIST_BY_SEARCH;

            if (activeOnly)
            {
                query += " AND (active=1) ";
            }
            query += " ORDER BY created DESC";

            //Execute a query to read the competition details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, query, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Competition comp = new Competition();

                    comp.Id = Convert.ToInt32(rdr["Id"]);
                    comp.comp_id = Convert.ToInt32(rdr["comp_id"]);
                    comp.country_code = Convert.ToString(rdr["country_code"]);
                    comp.Name = Convert.ToString(rdr["comp"]);
                    comp.UrlName = Convert.ToString(rdr["urlname"]);
                    comp.ExternalUrl = Convert.ToString(rdr["exturl"]);
                    comp.ExternalMobileUrl = Convert.ToString(rdr["extmobiurl"]);
                    comp.DateStartCheck = Convert.ToDateTime(rdr["date_start"]);
                    comp.DateStart = comp.DateStartCheck.ToShortDateString();
                    comp.DateEndCheck = Convert.ToDateTime(rdr["date_end"]);
                    comp.DateEnd = comp.DateEndCheck.ToShortDateString();
                    comp.HeaderImageWeb = Convert.ToString(rdr["header"]);
                    comp.FooterImageWeb = Convert.ToString(rdr["footer"]);
                    comp.HeaderImageMobi = Convert.ToString(rdr["header_mobi"]);
                    comp.FooterImageMobi = Convert.ToString(rdr["footer_mobi"]);
                    comp.ThumbNail = Convert.ToString(rdr["thumbnail"]);
                    comp.Prizes = Convert.ToString(rdr["prizes"]);
                    comp.Disclaimer = Convert.ToString(rdr["disclaimer"]);
                    comp.RequireLogin = Convert.ToBoolean(rdr["require_login"]);
                    comp.Active = Convert.ToBoolean(rdr["active"]);
                    comp.AllowMultipleEntriesPerDay = Convert.ToBoolean(rdr["allow_multiple_entries_per_day"]);
                    comp.EntryResponse = Convert.ToString(rdr["entry_response"]);
                    comp.OptInMessage = Convert.ToString(rdr["optin_message"]);
                    comp.Feature = Convert.ToBoolean(rdr["Feature"]);
                    comp.HasMarketingOptIn = Convert.ToBoolean(rdr["marketing_optin"]);
                    comp.MarketingOptInMessage = Convert.ToString(rdr["marketing_message"]);
                    comp.Sport = GetIntValue(rdr["competition_sport"]);
                    comp.VideoCategoryUrl = rdr["video_category_url"].ToString();
                    comp.HasMultichoiceOptIn = Convert.ToBoolean(rdr["multichoice_optin"]);
                    comp.HasRichMedia = Convert.ToBoolean(rdr["rich_media"]);
                    comp.MobileThumbNail = Convert.ToString(rdr["Mobile_thumbnail"]);
                    comp.AgeVerificationRequired = Convert.ToBoolean(rdr["ageverificationrequired"]);
                    //Get questions for a compeition
                    comp.Questions = questDAL.GetQuestions(Convert.ToInt32(comp.comp_id));

                    //get countries assigned to competition
                    comp.OpenToCountries = countryDAL.GetCompetitionCountries(comp.comp_id);
                    comps.Add(comp);
                }
            }

            return comps;
        }

        public List<Competition> GetCompetitionsMobi(string searchVal, bool activeOnly)
        {
            List<Competition> comps = new List<Competition>();

            List<SqlParameter> parms = new List<SqlParameter>();

            parms.Add(new SqlParameter("@compName", "%" + searchVal + "%"));

            string query = SQL_SELECT_COMPLIST_BY_SEARCH;

            if (activeOnly)
            {
                query += " AND (active=1) ";
            }
            query += " ORDER BY created DESC";

            //Execute a query to read the competition details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, query, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Competition comp = new Competition();

                    comp.Id = Convert.ToInt32(rdr["Id"]);
                    comp.comp_id = Convert.ToInt32(rdr["comp_id"]);
                    comp.country_code = Convert.ToString(rdr["country_code"]);
                    comp.Name = Convert.ToString(rdr["comp"]);
                    comp.UrlName = Convert.ToString(rdr["urlname"]);
                    comp.ExternalUrl = Convert.ToString(rdr["exturl"]);
                    comp.ExternalMobileUrl = Convert.ToString(rdr["extmobiurl"]);
                    comp.DateStartCheck = Convert.ToDateTime(rdr["date_start"]);
                    comp.DateStart = comp.DateStartCheck.ToShortDateString();
                    comp.DateEndCheck = Convert.ToDateTime(rdr["date_end"]);
                    comp.DateEnd = comp.DateEndCheck.ToShortDateString();
                    comp.HeaderImageWeb = Convert.ToString(rdr["header"]);
                    comp.FooterImageWeb = Convert.ToString(rdr["footer"]);
                    comp.HeaderImageMobi = Convert.ToString(rdr["header_mobi"]);
                    comp.FooterImageMobi = Convert.ToString(rdr["footer_mobi"]);
                    comp.ThumbNail = Convert.ToString(rdr["thumbnail"]);
                    comp.Prizes = Convert.ToString(rdr["prizes"]);
                    comp.Disclaimer = Convert.ToString(rdr["disclaimer"]);
                    comp.RequireLogin = Convert.ToBoolean(rdr["require_login"]);
                    comp.Active = Convert.ToBoolean(rdr["active"]);
                    comp.AllowMultipleEntriesPerDay = Convert.ToBoolean(rdr["allow_multiple_entries_per_day"]);
                    comp.EntryResponse = Convert.ToString(rdr["entry_response"]);
                    comp.OptInMessage = Convert.ToString(rdr["optin_message"]);
                    comp.Feature = Convert.ToBoolean(rdr["Feature"]);
                    comp.HasMarketingOptIn = Convert.ToBoolean(rdr["marketing_optin"]);
                    comp.MarketingOptInMessage = Convert.ToString(rdr["marketing_message"]);
                    comp.Sport = GetIntValue(rdr["competition_sport"]);
                    comp.VideoCategoryUrl = rdr["video_category_url"].ToString();
                    comp.HasMultichoiceOptIn = Convert.ToBoolean(rdr["multichoice_optin"]);
                    comp.HasRichMedia = Convert.ToBoolean(rdr["rich_media"]);
                    comp.MobileThumbNail = Convert.ToString(rdr["Mobile_thumbnail"]);
                    comp.AgeVerificationRequired = Convert.ToBoolean(rdr["ageverificationrequired"]);
                    //Get questions for a compeition
                    comp.Questions = questDAL.GetQuestions(Convert.ToInt32(comp.comp_id));

                    //get countries assigned to competition
                    comp.OpenToCountries = countryDAL.GetCompetitionCountries(comp.comp_id);
                    comps.Add(comp);
                }
            }

            return comps;
        }

        /// <summary>
        /// Reads a list of competitions from database based on a user defined number
        /// </summary>
        /// <param name="maxNumber">Number of rows that needs to be returned</param>
        /// <returns></returns>
        public List<Competition> GetCompetitionsLight(string searchVal, bool activeOnly)
        {
            List<Competition> competitions = new List<Competition>();
            List<SqlParameter> sqlParameters = new List<SqlParameter>()
            {
                new SqlParameter("@compName", string.Concat("%", searchVal, "%"))
            };
            string str = "SELECT * FROM SuperSport.dbo.comp_competitions WHERE comp like @compName";
            if (activeOnly)
            {
                str = string.Concat(str, " AND (active=1) ");
            }
            str = string.Concat(str, " ORDER BY created DESC");
            using (SqlDataReader sqlDataReader = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, str, sqlParameters.ToArray()))
            {
                while (sqlDataReader.Read())
                {
                    Competition comp = new Competition();


                    comp.Id = Convert.ToInt32(sqlDataReader["Id"]);
                    //comp.comp_id = Convert.ToInt32(sqlDataReader["comp_id"]);
                    comp.Name = Convert.ToString(sqlDataReader["comp"]);
                    comp.UrlName = Convert.ToString(sqlDataReader["urlname"]);
                    comp.ExternalUrl = Convert.ToString(sqlDataReader["exturl"]);
                    comp.ExternalMobileUrl = Convert.ToString(sqlDataReader["extmobiurl"]);
                    //comp.DateStart = Convert.ToDateTime(sqlDataReader["date_start"]);
                    //comp.DateEnd = Convert.ToDateTime(sqlDataReader["date_end"]);
                    comp.DateStartCheck = Convert.ToDateTime(sqlDataReader["date_start"]);
                    comp.DateStart = comp.DateStartCheck.ToShortDateString();
                    comp.DateEndCheck = Convert.ToDateTime(sqlDataReader["date_end"]);
                    comp.DateEnd = comp.DateEndCheck.ToShortDateString();
                    comp.HeaderImageWeb = Convert.ToString(sqlDataReader["header"]);
                    comp.FooterImageWeb = Convert.ToString(sqlDataReader["footer"]);
                    comp.HeaderImageMobi = Convert.ToString(sqlDataReader["header_mobi"]);
                    comp.FooterImageMobi = Convert.ToString(sqlDataReader["footer_mobi"]);
                    comp.ThumbNail = Convert.ToString(sqlDataReader["thumbnail"]);
                    comp.Prizes = Convert.ToString(sqlDataReader["prizes"]);
                    comp.Disclaimer = Convert.ToString(sqlDataReader["disclaimer"]);
                    comp.RequireLogin = Convert.ToBoolean(sqlDataReader["require_login"]);
                    comp.Active = Convert.ToBoolean(sqlDataReader["active"]);
                    comp.AllowMultipleEntriesPerDay = Convert.ToBoolean(sqlDataReader["allow_multiple_entries_per_day"]);
                    comp.EntryResponse = Convert.ToString(sqlDataReader["entry_response"]);
                    comp.OptInMessage = Convert.ToString(sqlDataReader["optin_message"]);
                    comp.Feature = Convert.ToBoolean(sqlDataReader["Feature"]);
                    comp.HasMarketingOptIn = Convert.ToBoolean(sqlDataReader["marketing_optin"]);
                    comp.MarketingOptInMessage = Convert.ToString(sqlDataReader["marketing_message"]);
                    comp.Sport = CompetitionDAL.GetIntValue(sqlDataReader["competition_sport"]);
                    comp.VideoCategoryUrl = sqlDataReader["video_category_url"].ToString();
                    comp.OpenToCountries = CompetitionDAL.countryDAL.GetCompetitionCountries(comp.Id);
                   
                    competitions.Add(comp);
                }
            }
            return competitions;
        }

        private static int GetIntValue(object value)
        {
            if (value != null && value != DBNull.Value)
                return Convert.ToInt32(value);
            return -1;
        }



        public List<Competition> GetFeaturedCompetitions(string countryCode)
        {
            List<Competition> comps = null;

            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@country", countryCode));

            comps = new List<Competition>();

            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_FEATURES_COMPS, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    Competition comp = new Competition();

                    comp.Id = Convert.ToInt32(rdr["Id"]);
                    comp.comp_id = Convert.ToInt32(rdr["comp_id"]);
                    comp.country_code = Convert.ToString(rdr["country_code"]);
                    comp.Name = Convert.ToString(rdr["comp"]);
                    comp.UrlName = Convert.ToString(rdr["urlname"]);
                    comp.ExternalUrl = Convert.ToString(rdr["exturl"]);
                    comp.ExternalMobileUrl = Convert.ToString(rdr["extmobiurl"]);
                    comp.DateStartCheck = Convert.ToDateTime(rdr["date_start"]);
                    comp.DateStart = comp.DateStartCheck.ToShortDateString();
                    comp.DateEndCheck = Convert.ToDateTime(rdr["date_end"]);
                    comp.DateEnd = comp.DateEndCheck.ToShortDateString();
                    comp.HeaderImageWeb = Convert.ToString(rdr["header"]);
                    comp.FooterImageWeb = Convert.ToString(rdr["footer"]);
                    comp.HeaderImageMobi = Convert.ToString(rdr["header_mobi"]);
                    comp.FooterImageMobi = Convert.ToString(rdr["footer_mobi"]);
                    comp.ThumbNail = Convert.ToString(rdr["thumbnail"]);
                    comp.Prizes = Convert.ToString(rdr["prizes"]);
                    comp.Disclaimer = Convert.ToString(rdr["disclaimer"]);
                    comp.RequireLogin = Convert.ToBoolean(rdr["require_login"]);
                    comp.Active = Convert.ToBoolean(rdr["active"]);
                    comp.AllowMultipleEntriesPerDay = Convert.ToBoolean(rdr["allow_multiple_entries_per_day"]);
                    comp.EntryResponse = Convert.ToString(rdr["entry_response"]);
                    comp.OptInMessage = Convert.ToString(rdr["optin_message"]);
                    comp.Feature = Convert.ToBoolean(rdr["Feature"]);
                    comp.HasMarketingOptIn = Convert.ToBoolean(rdr["marketing_optin"]);
                    comp.MarketingOptInMessage = Convert.ToString(rdr["marketing_message"]);
                    comp.Sport = GetIntValue(rdr["competition_sport"]);
                    comp.VideoCategoryUrl = rdr["video_category_url"].ToString();

                    //Get questions for a compeition
                    comp.Questions = questDAL.GetQuestions(comp.comp_id);

                    //get countries assigned to competition
                    comp.OpenToCountries = countryDAL.GetCompetitionCountries(comp.Id);

                    comps.Add(comp);
                }
            }
            return comps;
        }

        public bool UrlNameExist(string urlname, int compId)
        {
            bool exist = false;

            //Create a parameter
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@urlname", urlname));
            parm.Add(new SqlParameter("@id", compId));
            
            //Execute a query to read the competition id
            object result = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_COUNT_COMP_URLNAME, parm.ToArray());

            int count = 0;

            if ((result != null) && (result != DBNull.Value))
            {
                count = Convert.ToInt32(result);
            }

            if (count > 0)
            {
                exist = true;
            }

            return exist;
        }

        public Dictionary<string,int> GetSports()
        {
            var sports = new Dictionary<string,int>();
            var rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_SPORTS, new List<SqlParameter>().ToArray());

            if(rdr != null)
            {
                while(rdr.Read())
                {
                    int sportId = Convert.ToInt32((rdr["ID"].ToString()));
                    string sportName = rdr["SportName"].ToString();
                    sports.Add(sportName, sportId);
                }
            }

            return sports;
        }
    }
}