﻿using System;
using System.Collections.Generic;
using System.Text;
using SuperSport.CompetitionLib.Models;
using SuperSport.DBUtility;
using System.Data.SqlClient;
using System.Data;

namespace SuperSport.CompetitionLib.DAL
{
    public class CompetitionEntryDAL
    {
        private const string SQL_INSERT_QUESTION = "INSERT INTO SuperSport.dbo.comp_entries (Comp_id, Question_Id, Name, Surname, Email, ContactNo, Answer, Receive_Marketing, Country_Code, Ip_Address, User_Agent, source, connectId, city, idnumber,multichoice_marketing) VALUES (@CompetitionId, @QuestionId, @Name, @Surname, @Email, @ContactNumber, @Answer, @ReceiveMarketing, @CountryCode, @IpAddress, @UserAgent, @source, @connectId, @city, @idnumber, @multichoicemarketing)";
        private const string SQL_SELECT_ENTRY = "SELECT * FROM SuperSport.dbo.comp_entries WHERE id=@id";

        private const string SQL_SEARCH_ENTRY = "SELECT * FROM SuperSport.dbo.comp_entries WHERE name LIKE @name AND surname LIKE @surname AND ContactNo like @contactNo AND email LIKE @email AND (date_entered BETWEEN @startdate AND @enddate) AND comp_id=@comp_id";

        private const string SQL_USER_DAILY_ENTRY_COUNT = "SELECT COUNT(*) FROM supersport.dbo.comp_entries WHERE comp_id=@compId AND contactno = @contactNo AND date_entered BETWEEN @startdate AND @enddate";
        private const string SQL_USER_QUESTION_DAILY_ENTRY_COUNT = "SELECT COUNT(*) FROM supersport.dbo.comp_entries WHERE (email = @email OR idnumber = @idnumber OR contactno = @contactNo) AND comp_id=@compId AND question_id = @question_id AND date_entered BETWEEN @startdate AND @enddate";
        private const string SQL_USER_QUESTION_ENTRY_COUNT = "SELECT COUNT(*) FROM supersport.dbo.comp_entries WHERE (email = @email OR idnumber = @idnumber OR contactno = @contactNo) AND comp_id=@compId AND question_id = @question_id";
        private const string SQL_INSERT_QUESTION_RICHMEDIA = "INSERT INTO SuperSport.dbo.comp_entries (Comp_id, Question_Id, Name, Surname, Email, ContactNo, Receive_Marketing, Country_Code, Ip_Address, User_Agent, source, connectId, city, idnumber,multichoice_marketing) VALUES (@CompetitionId, @QuestionId,@Name, @Surname, @Email, @ContactNumber, @ReceiveMarketing, @CountryCode, @IpAddress, @UserAgent, @source, @connectId, @city, @idnumber, @multichoicemarketing)";

        /// <summary>
        /// Counts the amount of entries a specific contact number has done on the current day for a competition
        /// </summary>
        /// <param name="competitionId">competition id as int</param>
        /// <param name="contactNo">contact number as string</param>
        /// <returns>entry count for the competition as an int</returns>
        public int CountUserEntriesPerDay(int competitionId, string contactNo)
        {          
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@compId", competitionId));
            parm.Add(new SqlParameter("@contactNo", contactNo));
            parm.Add(new SqlParameter("@startdate", DateTime.Now.ToString("yyyy/MM/dd 00:00:00")));
            parm.Add(new SqlParameter("@enddate", DateTime.Now.ToString("yyyy/MM/dd 23:59:59")));            


            //Execute a query to read the competition details
            object rslt = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_USER_DAILY_ENTRY_COUNT, parm.ToArray());
            int entryCount = 0;

            if ((rslt != null) && (rslt != DBNull.Value))
            {
                entryCount = Convert.ToInt32(rslt);
            }

            return entryCount;
        }

        public int CountQuestionUserEntriesPerDay(int competitionId, string contactNo, int questionId, string idNumber, string email)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@compId", competitionId));
            parm.Add(new SqlParameter("@contactNo", contactNo));
            parm.Add(new SqlParameter("@question_id", questionId));
            parm.Add(new SqlParameter("@startdate", DateTime.Now.ToString("yyyy/MM/dd 00:00:00")));
            parm.Add(new SqlParameter("@enddate", DateTime.Now.ToString("yyyy/MM/dd 23:59:59")));
            parm.Add(new SqlParameter("@email", email));
            parm.Add(new SqlParameter("@idnumber", idNumber));



            //Execute a query to read the competition details
            object rslt = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_USER_QUESTION_DAILY_ENTRY_COUNT, parm.ToArray());
            int entryCount = 0;

            if ((rslt != null) && (rslt != DBNull.Value))
            {
                entryCount = Convert.ToInt32(rslt);
            }

            return entryCount;
        }

        public int CountQuestionUserEntriesPerQuestion(int competitionId, string contactNo, int questionId, string idNumber, string email)
        {
            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@compId", competitionId));
            parm.Add(new SqlParameter("@contactNo", contactNo));
            parm.Add(new SqlParameter("@question_id", questionId));
            parm.Add(new SqlParameter("@email", email));
            parm.Add(new SqlParameter("@idnumber", idNumber));

            //Execute a query to read the competition details
            object rslt = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_USER_QUESTION_ENTRY_COUNT, parm.ToArray());
            int entryCount = 0;

            if ((rslt != null) && (rslt != DBNull.Value))
            {
                entryCount = Convert.ToInt32(rslt);
            }

            return entryCount;
        }

        public int CountCompetitionUserEntries(int competitionId, string contactNo, string idNumber, string email)
        {
            object obj = SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringRead, CommandType.Text,
                "SELECT COUNT(*) FROM supersport.dbo.comp_entries WHERE (email = @email OR idnumber = @idnumber OR contactno = @contactNo) AND comp_id=@compId",
                new List<SqlParameter>()
                {
                    new SqlParameter("@compId", (object)competitionId),
                    new SqlParameter("@contactNo", (object)contactNo),
                    new SqlParameter("@email", (object)email),
                    new SqlParameter("@idnumber", (object)idNumber)
                }.ToArray());
            int num = 0;
            if(obj != null && obj != DBNull.Value) num = Convert.ToInt32(obj);
            return num;
        }

        /// <summary>
        /// Enters a user into a competion by passing CompetitionEntry object.
        /// </summary>
        /// <param name="entry">CompetitionEntry parm</param>
        /// <returns>true if success, false if failed</returns>
        public bool Enter(CompetitionEntry entry)
        {
            bool success = true;

            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@CompetitionId", entry.CompetitionId));
            sqlParms.Add(new SqlParameter("@QuestionId", entry.QuestionId));
            sqlParms.Add(new SqlParameter("@Name", entry.Name));
            sqlParms.Add(new SqlParameter("@Surname", entry.Surname));
            sqlParms.Add(new SqlParameter("@city", entry.City));
            sqlParms.Add(new SqlParameter("@Email", entry.Email));
            sqlParms.Add(new SqlParameter("@ContactNumber", entry.ContactNumber));
            sqlParms.Add(new SqlParameter("@IdNumber", entry.IdNumber));
            sqlParms.Add(new SqlParameter("@Answer", entry.Answer));
            sqlParms.Add(new SqlParameter("@ReceiveMarketing", entry.ReceiveMarketing));
            sqlParms.Add(new SqlParameter("@CountryCode", entry.CountryCode));
            sqlParms.Add(new SqlParameter("@IpAddress", entry.IpAddress));
            sqlParms.Add(new SqlParameter("@UserAgent", entry.UserAgent));
            sqlParms.Add(new SqlParameter("@source", entry.Source));
            sqlParms.Add(new SqlParameter("@connectId", entry.ConnectId));

            SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_QUESTION, sqlParms.ToArray());

            return success;
        }

        /// <summary>
        /// Enters a user into a competion by passing CompetitionEntry object.
        /// </summary>
        /// <param name="entry">CompetitionEntry parm</param>
        /// <returns>true if success, false if failed</returns>
        public bool EnterNew(CompetitionEntryNew entry, int queId, string answer)
        {
            bool success = true;

            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@CompetitionId", entry.CompetitionId));
            //sqlParms.Add(new SqlParameter("@QuestionId", entry.QuestionId));
            sqlParms.Add(new SqlParameter("@QuestionId", queId));
            sqlParms.Add(new SqlParameter("@Name", entry.Name));
            sqlParms.Add(new SqlParameter("@Surname", entry.Surname));
            sqlParms.Add(new SqlParameter("@city", entry.City));
            sqlParms.Add(new SqlParameter("@Email", entry.Email));
            sqlParms.Add(new SqlParameter("@ContactNumber", entry.ContactNumber));
            sqlParms.Add(new SqlParameter("@IdNumber", entry.IdNumber));
            //sqlParms.Add(new SqlParameter("@Answer", entry.Answer));
            sqlParms.Add(new SqlParameter("@Answer", answer));
            sqlParms.Add(new SqlParameter("@ReceiveMarketing", entry.ReceiveMarketing));
            sqlParms.Add(new SqlParameter("@CountryCode", entry.CountryCode));
            sqlParms.Add(new SqlParameter("@IpAddress", entry.IpAddress));
            sqlParms.Add(new SqlParameter("@UserAgent", entry.UserAgent));
            sqlParms.Add(new SqlParameter("@source", entry.Source));
            sqlParms.Add(new SqlParameter("@connectId", entry.ConnectId));
            sqlParms.Add(new SqlParameter("@multichoicemarketing", entry.MultichoiceMarketing));

            SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_QUESTION, sqlParms.ToArray());

            return success;
        }

        public CompetitionEntry GetCompetitionEntry(int id)
        {
            CompetitionEntry entry = null;

            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@id", id));

            //Execute a query to read the competition details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_ENTRY, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    entry = new CompetitionEntry();
                    entry.CompetitionId = Convert.ToInt32(rdr["Comp_id"]);
                    entry.QuestionId = Convert.ToInt32(rdr["Question_Id"]);
                    entry.Name = Convert.ToString(rdr["name"]);
                    entry.Surname = Convert.ToString(rdr["Surname"]);
                    entry.Email = Convert.ToString(rdr["Email"]);
                    entry.City = Convert.ToString(rdr["city"]);
                    entry.ContactNumber = Convert.ToString(rdr["ContactNo"]);
                    entry.IdNumber = Convert.ToString(rdr["IdNumber"]);
                    entry.Answer = Convert.ToString(rdr["Answer"]);
                    entry.ReceiveMarketing = Convert.ToBoolean(rdr["Receive_Marketing"]);
                    entry.CountryCode = Convert.ToString(rdr["Country_Code"]);
                    entry.IpAddress = Convert.ToString(rdr["Ip_Address"]);
                    entry.UserAgent = Convert.ToString(rdr["User_Agent"]);
                    entry.Source = (CompetitionEntry.EntryLocation)Enum.Parse(typeof(CompetitionEntry.EntryLocation), Convert.ToString(rdr["source"]));
                    entry.ConnectId = Convert.ToString(rdr["connectId"]);
                    entry.DateEntered = Convert.ToDateTime(rdr["date_entered"]);
                }
            }

            return entry;
        }

        public List<CompetitionEntry> SearchCompetitionEntries(int competitionId, int amount, string name, string surname, string contactNumber, string email, DateTime startDate, DateTime enddDate)
        {
            List<CompetitionEntry> entries = new List<CompetitionEntry>();

            List<SqlParameter> parm = new List<SqlParameter>();
            parm.Add(new SqlParameter("@name", "%" + name + "%"));
            parm.Add(new SqlParameter("@surname", "%" + surname + "%"));
            parm.Add(new SqlParameter("@contactno", "%" + contactNumber + "%"));
            parm.Add(new SqlParameter("@email", "%" + email + "%"));
            parm.Add(new SqlParameter("@startDate", startDate));
            parm.Add(new SqlParameter("@endDate", enddDate));
            parm.Add(new SqlParameter("@comp_id", competitionId));


            //Execute a query to read the competition details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SEARCH_ENTRY, parm.ToArray()))
            {
                while (rdr.Read())
                {
                    CompetitionEntry entry = new CompetitionEntry();
                    entry.CompetitionId = Convert.ToInt32(rdr["Comp_id"]);
                    entry.QuestionId = Convert.ToInt32(rdr["Question_Id"]);
                    entry.Name = Convert.ToString(rdr["name"]);
                    entry.Surname = Convert.ToString(rdr["Surname"]);
                    entry.Email = Convert.ToString(rdr["Email"]);
                    entry.ContactNumber = Convert.ToString(rdr["ContactNo"]);
                    entry.IdNumber = Convert.ToString(rdr["IdNumber"]);
                    entry.Answer = Convert.ToString(rdr["Answer"]);
                    entry.ReceiveMarketing = Convert.ToBoolean(rdr["Receive_Marketing"]);
                    entry.CountryCode = Convert.ToString(rdr["Country_Code"]);
                    entry.City = Convert.ToString(rdr["city"]);
                    entry.IpAddress = Convert.ToString(rdr["Ip_Address"]);
                    entry.UserAgent = Convert.ToString(rdr["User_Agent"]);
                    entry.Source = (CompetitionEntry.EntryLocation)Enum.Parse(typeof(CompetitionEntry.EntryLocation), Convert.ToString(rdr["source"]));
                    entry.ConnectId = Convert.ToString(rdr["connectId"]);
                    entry.DateEntered = Convert.ToDateTime(rdr["date_entered"]);

                    entries.Add(entry);
                }
            }

            return entries;
        }
        public bool EnterRichMedia(CompetitionEntryRichMedia entry, int queId)
        {
            bool success = true;

            List<SqlParameter> sqlParms = new List<SqlParameter>();
            sqlParms.Add(new SqlParameter("@CompetitionId", entry.CompetitionId));
  
            sqlParms.Add(new SqlParameter("@QuestionId", queId));
            sqlParms.Add(new SqlParameter("@Name", entry.Name));
            sqlParms.Add(new SqlParameter("@Surname", entry.Surname));
            sqlParms.Add(new SqlParameter("@city", entry.City));
            sqlParms.Add(new SqlParameter("@Email", entry.Email));
            sqlParms.Add(new SqlParameter("@ContactNumber", entry.ContactNumber));
            sqlParms.Add(new SqlParameter("@IdNumber", entry.IdNumber));
            sqlParms.Add(new SqlParameter("@ReceiveMarketing", entry.ReceiveMarketing));
            sqlParms.Add(new SqlParameter("@CountryCode", entry.CountryCode));
            sqlParms.Add(new SqlParameter("@IpAddress", entry.IpAddress));
            sqlParms.Add(new SqlParameter("@UserAgent", entry.UserAgent));
            sqlParms.Add(new SqlParameter("@source", entry.Source));
            sqlParms.Add(new SqlParameter("@connectId", entry.ConnectId));
            sqlParms.Add(new SqlParameter("@multichoicemarketing", entry.MultichoiceMarketing));

            SqlHelper.ExecuteScalar(SqlHelper.ConnectionStringWrite, CommandType.Text, SQL_INSERT_QUESTION_RICHMEDIA, sqlParms.ToArray());

            return success;
        }
    }
}
