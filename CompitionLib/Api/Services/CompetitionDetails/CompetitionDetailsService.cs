﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Dtos;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.CompetitionDetails
{
    public class CompetitionDetailsService : IGetService<CompetitionDetailsRequest>
    {
        public object Get(CompetitionDetailsRequest request)
        {
            return new CompetitionDto(Competition.GetCompetition(request.CompetitionId));
        }
    }
}