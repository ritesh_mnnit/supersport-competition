﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperSport.CompetitionLib.Exceptions;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.EnterCompetition
{
    public class EnterCompetitionService : IPostService<CompetitionEntry>
    {
        public object Post(CompetitionEntry request)
        {
            try
            {
                Competition competition = Competition.GetCompetition(request.CompetitionId);

                request.Source = CompetitionEntry.EntryLocation.Api;
                request.DateEntered = DateTime.Now;

                return competition.Enter(request, CompetitionEntryRestriction.OncePerDayPerQuestion);
            }
            catch (ValidationException exception)
            {
                throw new EntryValidationException(exception.ValidationList);
            }

        }
    }
}