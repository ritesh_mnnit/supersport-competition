﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.EnterCompetition
{
    public class EnterCompetitionRequest
    {
        public int CompetitionId { get; set; }
        public CompetitionEntry Entry { get; set; }
    }
}