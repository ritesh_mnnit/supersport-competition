﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Dtos;
using SuperSport.CompetitionLib.Models;


namespace Api.Services.CompetitionWinner
{
    public class CompetitionWinnerService : IGetService<CompetitionWinnerRequest>
    {

        public object Get(CompetitionWinnerRequest request)
        {
            return  (Competition.GetPreviousWinners(request.year));
        }
        
    }
}