﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Dtos;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.AllCompetitions
{
    public class AllCompetitionsService : IGetService<AllCompetitionsRequest>
    {
        public object Get(AllCompetitionsRequest request)
        {
            return Competition.GetCompetitions("", true)
                .Select(x => new CompetitionSummaryDto(x))
                .ToArray()
                ;
        }
    }
}