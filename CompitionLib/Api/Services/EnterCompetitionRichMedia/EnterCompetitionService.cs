﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperSport.CompetitionLib.Exceptions;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.EnterCompetitionRichMedia
{
    public class EnterCompetitionService : IPostService<CompetitionEntryRichMedia>
    {
        public object Post(CompetitionEntryRichMedia request)
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();
            Competition competition=null;
            try
            {

                if (request.CompetitionId == 0)
                {
                     validationList.Add(new ValidationMessage("CompetitionId is required", "CE17"));
                   
                }
                else
                {
                    competition = Competition.GetCompetition(request.CompetitionId);
                    if (competition == null)
                    {
                        validationList.Add(new ValidationMessage("Please enter valid CompetitionId", "CE18"));
                      
                    }
                  
                }

                if (validationList.Count > 0)
                {
                    ValidationException exception = new ValidationException();
                    exception.ValidationList = validationList;
                    throw exception;

                }
                else
                {

                    request.Source = CompetitionEntryRichMedia.EntryLocation.Api;
                    request.DateEntered = DateTime.Now;
                    return competition.EnterRichMedia(request, CompetitionEntryRestriction.OncePerDayPerQuestion);
                }
            }
            catch (ValidationException exception)
            {
                throw new EntryValidationException(exception.ValidationList);
            }
        }
    }
}