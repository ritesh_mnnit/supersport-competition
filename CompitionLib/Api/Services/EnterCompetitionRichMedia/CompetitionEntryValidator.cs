﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.FluentValidation;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.EnterCompetitionRichMedia
{
    public class CompetitionEntryValidator: AbstractValidator<CompetitionEntry>
    {
        public CompetitionEntryValidator()
        {
            RuleFor(r => r.IdNumber)
                .NotEmpty()
                .NotNull()
                ;
        }
    }
}