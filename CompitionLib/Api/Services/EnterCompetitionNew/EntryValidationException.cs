﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface.ServiceModel;
using SuperSport.CompetitionLib.Exceptions;

namespace Api.Services.EnterCompetitionNew
{
    public class EntryValidationException : ValidationException, IResponseStatusConvertible
    {
        public EntryValidationException(List<ValidationMessage> validationList)
            : base(validationList)
        { }

        public ResponseStatus ToResponseStatus()
        {
            return new ResponseStatus()
            {
                Message = Message,
                StackTrace = StackTrace,
                ErrorCode = "ValidationException",
                Errors = ValidationList
                .Select(x => new ResponseError()
                {
                    ErrorCode = x.Code,
                    Message = x.ErrorMessage,
                })
                .ToList<ResponseError>(),
            };
        }
    }
}