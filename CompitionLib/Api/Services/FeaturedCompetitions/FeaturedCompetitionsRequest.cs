﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services.FeaturedCompetitions
{
    public class FeaturedCompetitionsRequest
    {
        public int? NumberOfCompetitions { get; set; }
        public string CountryCode { get; set; }
    }
}