﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Dtos;
using SuperSport.CompetitionLib.Models;

namespace Api.Services.FeaturedCompetitions
{
    public class FeaturedCompetitionsService : IGetService<FeaturedCompetitionsRequest>
    {
        private readonly Func<string, List<Competition>> getFeaturedCompetitions;

        public FeaturedCompetitionsService(Func<string, List<Competition>> featuredCompetitionsFunc)
        {
            getFeaturedCompetitions = featuredCompetitionsFunc;
        }

        public object Get(FeaturedCompetitionsRequest request)
       {
            if (string.IsNullOrEmpty(request.CountryCode))
                throw new ArgumentNullException("Country Code");

            IEnumerable<Competition> competitions = getFeaturedCompetitions(request.CountryCode);

            if (request.NumberOfCompetitions.HasValue)
            {
                competitions = competitions.Take(request.NumberOfCompetitions.Value);
            }

            return competitions
                .Select(x => new CompetitionSummaryDto(x))
                .ToArray()
                ;
        }

    }
}