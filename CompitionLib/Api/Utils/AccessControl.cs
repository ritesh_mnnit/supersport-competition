﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperSport.FeedACLLib.Models;

namespace SuperSport.Utils
{
    public class AccessControl
    {
        public static bool HasAccess(string url, string sport, string tournament, METHODS method, Consumer consumer)
        {
            bool allow = false;

            if (consumer != null)
            {
                //if user has access to everything, allow
                if (consumer.AllowAll)
                {
                    return true;
                }


                //check methods
                if (method == METHODS.COMPETITIONS)
                {
                    if (consumer.MethodAccess.Contains(method.ToString()))
                    {
                        allow = true;
                    }
                }
      
            }

            return allow;
        }

 
    }
}