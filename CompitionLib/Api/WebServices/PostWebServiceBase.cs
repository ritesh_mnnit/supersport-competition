﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace Api.WebServices
{
    public class PostWebServiceBase<TRequest> : Service
    {
        private readonly IPostService<TRequest> postService;

        public PostWebServiceBase(IPostService<TRequest> postService)
        {
            this.postService = postService;
        }

        public virtual object Post(TRequest request)
        {
            return postService.Post(request);
        }
    }
}