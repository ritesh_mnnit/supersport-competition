﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace Api.WebServices
{
    public class GetWebServiceBase<TRequest> : Service
    {
        private readonly IGetService<TRequest> getService;

        public GetWebServiceBase(IGetService<TRequest> getService)
        {
            this.getService = getService;
        }

        public virtual object Get(TRequest request)
        {
            return getService.Get(request);
        }
    }
}