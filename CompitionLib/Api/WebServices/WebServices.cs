﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Dtos;
using Api.Services.AllCompetitions;
using Api.Services.CompetitionDetails;
using Api.Services.EnterCompetition;
using Api.Services.FeaturedCompetitions;
using Api.Services.CompetitionWinner;

using SuperSport.CompetitionLib.Models;

namespace Api.WebServices
{
    public class FeaturedCompetitionWebService :
        GetWebServiceBase<FeaturedCompetitionsRequest>
    {
        public FeaturedCompetitionWebService(IGetService<FeaturedCompetitionsRequest> getService)
            : base(getService)
        { }
    }

    public class AllCompetitionsWebService :
        GetWebServiceBase<AllCompetitionsRequest>
    {
        public AllCompetitionsWebService(IGetService<AllCompetitionsRequest> getService)
            : base(getService)
        { }
    }

    public class CompetitionDetailsWebService :
        GetWebServiceBase<CompetitionDetailsRequest>
    {
        public CompetitionDetailsWebService(IGetService<CompetitionDetailsRequest> getService)
            : base(getService)
        { }
    }
    public class CompetitionWinnerWebService :
 GetWebServiceBase<CompetitionWinnerRequest>
    {
        public CompetitionWinnerWebService(IGetService<CompetitionWinnerRequest> getService)
            : base(getService)
        { }
    }

    public class CompetitionEntryWebService :
        PostWebServiceBase<CompetitionEntry>
    {
        public CompetitionEntryWebService(IPostService<CompetitionEntry> postService)
            : base(postService)
        { }
    }

    public class CompetitionEntryNewWebService :
       PostWebServiceBase<CompetitionEntryNew>
    {
        public CompetitionEntryNewWebService(IPostService<CompetitionEntryNew> postService)
            : base(postService)
        { }
    }
    public class CompetitionEntryRichMediaNewWebService :
       PostWebServiceBase<CompetitionEntryRichMedia>
    {
        public CompetitionEntryRichMediaNewWebService(IPostService<CompetitionEntryRichMedia> postService)
            : base(postService)
        { }
    }

}