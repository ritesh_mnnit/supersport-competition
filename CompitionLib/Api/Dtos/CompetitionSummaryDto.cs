﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using SuperSport.CompetitionLib.Models;

namespace Api.Dtos
{
    [DataContract]
    public class CompetitionSummaryDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int comp_id { get; set; }
        [DataMember]
        public string country_code { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DateStart { get; set; }
        [DataMember]
        public string DateEnd { get; set; }
        [DataMember]
        public string ThumbNail { get; set; }
        [DataMember]
        public string MobileThumbNail { get; set; }
        [DataMember]
        public string UrlName { get; set; }
        [DataMember]
        public string ExternalUrl { get; set; }
        [DataMember]
        public string ExternalMobileUrl { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool Feature { get; set; }

        public CompetitionSummaryDto(Competition competition)
        {
            Id = competition.Id;
            comp_id = competition.comp_id;
            country_code = competition.country_code;
            Name = competition.Name;
            DateStart = competition.DateStart;
            DateEnd = competition.DateEnd;
            ThumbNail = competition.ThumbNail;
            UrlName = competition.UrlName;
            ExternalUrl = competition.ExternalUrl;
            ExternalMobileUrl = competition.ExternalMobileUrl;
            Description = competition.OptInMessage;
            Feature = competition.Feature;
            MobileThumbNail = competition.MobileThumbNail;

        }
    }
}