﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using SuperSport.CompetitionLib.Models;

namespace Api.Dtos
{
    public class CompetitionDto : Competition
    {

        public string Description
        {
            get { return OptInMessage; }
        }

        public List<Question> CompetitionQuestions
        {
            get
            {
                return Questions
                    .Where(q => q.DateStart <= DateTime.Now)
                    .Where(q => q.DateEnd >= DateTime.Now)
                    .Where(q => q.Active)
                    .ToList()
                    ;
            }
        }

        [IgnoreDataMember]
        public override string OptInMessage { get; set; }

        public CompetitionDto(Competition competition)
        {
            Questions = competition.Questions;
            Id = competition.Id;
            comp_id = competition.Id;
            Name = competition.Name;
            DateStart = competition.DateStart;
            DateEnd = competition.DateEnd;
            HeaderImageWeb = competition.HeaderImageWeb;
            FooterImageWeb = competition.FooterImageWeb;
            HeaderImageMobi = competition.HeaderImageMobi;
            FooterImageMobi = competition.FooterImageMobi;
            ThumbNail = competition.ThumbNail;
            Active = competition.Active;
            UrlName = competition.UrlName;
            ExternalUrl = competition.ExternalUrl;
            ExternalMobileUrl = competition.ExternalMobileUrl;
            Disclaimer = competition.Disclaimer;
            Prizes = competition.Prizes;
            RequireLogin = competition.RequireLogin;
            EntryResponse = competition.EntryResponse;
            OptInMessage = competition.OptInMessage;
            Feature = competition.Feature;
            AllowMultipleEntriesPerDay = competition.AllowMultipleEntriesPerDay;
            OpenToCountries = competition.OpenToCountries;
            questionFields = competition.questionFields;
            HasRichMedia = competition.HasRichMedia;
            HasMultichoiceOptIn = competition.HasMultichoiceOptIn;
            HasMarketingOptIn = competition.HasMarketingOptIn;
            MobileThumbNail = competition.MobileThumbNail;
            AgeVerificationRequired = competition.AgeVerificationRequired;
        }

    }
}