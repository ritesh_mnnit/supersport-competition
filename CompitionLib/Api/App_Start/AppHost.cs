using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using Api.DI;
using Api.Services.AllCompetitions;
using Api.Services.CompetitionDetails;
using Api.Services.EnterCompetition;
using Api.Services.FeaturedCompetitions;
using Api.Services.CompetitionWinner;
using Api.WebServices;
using ServiceStack.Configuration;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;
using ServiceStack.Mvc;
using ServiceStack.OrmLite;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.ServiceInterface.Validation;
using ServiceStack.WebHost.Endpoints;
using SuperSport.CompetitionLib.Models;

namespace Api.App_Start
{
    public class AppHost : AppHostBase
    {
        public AppHost() //Tell ServiceStack the name and where to find your web services
            : base("Supersport Competitions ASP.NET Host", typeof(GetWebServiceBase<>).Assembly) { }

        public override void Configure(Funq.Container container)
        {
            container.Adapter = new StructureMapContainerAdapter();

            //Set JSON web services to return idiomatic JSON camelCase properties
            ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;

            //Configure User Defined REST Paths
            Routes
                .Add<FeaturedCompetitionsRequest>("/competitions/featured/{countrycode}/{numberofcompetitions*}/", "GET")
                .Add<AllCompetitionsRequest>("/competitions/", "GET")
                .Add<CompetitionDetailsRequest>("/competition/{competitionid}/", "GET")
                .Add<CompetitionEntry>("/competitions/", "POST")
                .Add<CompetitionEntryNew>("/competitions/new", "POST")
                .Add<CompetitionWinnerRequest>("/previouscompetitions/{year}/", "GET")
                .Add<CompetitionEntryRichMedia>("/competitions/richmedia", "POST")
              ;

            Plugins.Add(new ValidationFeature());
        }

        public static void Start()
        {
            new AppHost().Init();
        }
    }
}