﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Api.App_Start;
using Api.DI;
using log4net;
using log4net.Config;
using System.Text;
using SuperSport.FeedACLLib.Models;
using SuperSport.FeedACLLib;
using SuperSport.FeedACLLib.DAL;
using System.Configuration;

namespace Api
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static List<Consumer> _consumers = new List<Consumer>();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MvcApplication));
        private static readonly IAccessControlService AccessControl = new AccessControlDAL();
        private static readonly DateTime CacheTime = DateTime.Now;
        protected void Application_Start()
        {
            StructureMapContainer.Initialise();
            AppHost.Start();
        }

        void Application_beginRequest(object sender, EventArgs e)
        {
            _consumers = AccessControl.Get();
            var tournament = string.Empty;
            var sport = string.Empty;
            var accessControlled = Convert.ToBoolean(ConfigurationManager.AppSettings["AccessControlled"]);

            if (accessControlled)
            {
                var authKey = string.Empty;

                if ((Request.Headers["auth"] != null))
                {
                    authKey = Request.Headers["auth"];
                }
                else
                {
                    //request fallback
                    if ((Request.QueryString["auth"] != null))
                    {
                        authKey = Request.QueryString["auth"];
                    }
                }
                var consumer = _consumers.Find(f => (f.AuthKey == authKey));

                try
                {
                    var currentContext = new HttpContextWrapper(HttpContext.Current);
                    var routeData = RouteTable.Routes.GetRouteData(currentContext);
                    var method = METHODS.NONE;
                    if (consumer != null)
                    {
                        if (consumer.MethodAccess.Contains("COMPETITIONS"))
                        {
                            method = METHODS.COMPETITIONS;
                        }

                        if (SuperSport.Utils.AccessControl.HasAccess(Request.Url.ToString(), sport, tournament, method, consumer))
                        {
                            //allow
                            Log(consumer.Name, authKey, Request.Url.ToString(), RemoteIP());

                        }
                        else
                        {
                            //do some logging
                            Log("Unauthorized", authKey, Request.Url.ToString(), RemoteIP());
                            ReturnResponse(401, "Unauthorized");
                        }
                    }
                    else
                    {
                        Log("Unauthorized", authKey, Request.Url.ToString(), RemoteIP());
                        ReturnResponse(401, "Unauthorized");
                    }
                }
                catch
                {
                    // ignored
                    Log("Unauthorized", authKey, Request.Url.ToString(), RemoteIP());
                    ReturnResponse(401, "Unauthorized");
                }
            }
        }


        void Application_EndRequest(object sender, EventArgs e)
        {
        }
        private void Log(string consumer, string key, string url, string consumerIp)
        {
            ThreadContext.Properties["consumer"] = consumer;

            var sb = new StringBuilder();
            sb.Append("consumer=");
            sb.Append(consumer);
            sb.Append(",");
            sb.Append("url=");
            sb.Append(url);
            sb.Append(",");
            sb.Append("consumerIP=");
            sb.Append(consumerIp);

            Logger.Info(sb.ToString());
        }

        private string RemoteIP()
        {
            var ipAddress = string.Empty;

            if (HttpContext.Current.Request.ServerVariables["Remote_Addr"] != null)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["Remote_Addr"];
            }

            return ipAddress;
        }

        private void ReturnResponse(int statusCode, string message)
        {
            //deny
            try
            {
                Response.StatusCode = statusCode;
                Response.StatusDescription = message;
                Response.Write(message);
                //Response.End();
            }
            catch (Exception ex)
            {
                // ignored
            }
        }
        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            return custom.Equals("RawUrl", StringComparison.OrdinalIgnoreCase) ? context.Request.RawUrl : string.Empty;
        }
    }
}