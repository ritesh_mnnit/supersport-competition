﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.Configuration;
using StructureMap;

namespace Api.DI
{
    /// <summary>
    /// Used to replace the built-in Funq container bundled with ServiceStack
    /// </summary>
    public class StructureMapContainerAdapter : IContainerAdapter
    {
        public T TryResolve<T>()
        {
            return ObjectFactory.TryGetInstance<T>();
        }

        public T Resolve<T>()
        {
            return ObjectFactory.TryGetInstance<T>();
        }
    }
}