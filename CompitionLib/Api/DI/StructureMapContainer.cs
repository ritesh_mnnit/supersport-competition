﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.WebServices;
using ServiceStack.FluentValidation;
using StructureMap;
using SuperSport.CompetitionLib.Models;

namespace Api.DI
{
    public class StructureMapContainer
    {
        public static void Initialise()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.LookForRegistries();
                    scan.WithDefaultConventions();
                    scan.TheCallingAssembly();
                    scan.ConnectImplementationsToTypesClosing(typeof(IGetService<>));
                    scan.ConnectImplementationsToTypesClosing(typeof (GetWebServiceBase<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(IPostService<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(PostWebServiceBase<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(IValidator<>));
                });

                x.For<Func<string, List<Competition>>>()
                    .AlwaysUnique()
                    .Use(y => Competition.GetFeaturedCompetitions(y))
                    ;

            });

            ObjectFactory.AssertConfigurationIsValid();
            string wdih = ObjectFactory.WhatDoIHave();
        }
    }
}