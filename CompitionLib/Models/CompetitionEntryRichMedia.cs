﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperSport.CompetitionLib.Models
{
    public class CompetitionEntryRichMedia
    {
        public enum EntryLocation { Web, Mobile, Api }

        public int CompetitionId { get; set; }
        public int[] QuestionId { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string IdNumber { get; set; }
        public string Answer { get; set; }
        public bool ReceiveMarketing { get; set; }
        public string ConnectId { get; set; }
        public string CountryCode { get; set; }
        public string City { get; set; }
        public string IpAddress { get; set; }
        public string UserAgent { get; set; }
        public DateTime DateEntered { get; set; }
        public EntryLocation Source { get; set; }
        public bool MultichoiceMarketing { get; set; }
    }
}
