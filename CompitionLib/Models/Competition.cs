﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;
using SuperSport.CompetitionLib.Exceptions;
using System.Configuration;
using SuperSport.CompetitionLib.Utils;
using System.Text.RegularExpressions;
using System.Data;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class Competition
    {
        public List<Question> Questions;

        // Properties
        public int Id { get; set; }
        public int comp_id { get; set; }
        public string country_code { get; set; }
        public int Sport { get; set; }
        public string VideoCategoryUrl { get; set; }
        public string Name { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public DateTime DateStartCheck { get; set; }
        public DateTime DateEndCheck { get; set; }
        public string HeaderImageWeb { get; set; }
        public string FooterImageWeb { get; set; }
        public string LiveVideoStreamingReplacement { get; set; }
        public string HeaderImageMobi { get; set; }
        public string FooterImageMobi { get; set; }
        public string ThumbNail { get; set; }
        public string MobileThumbNail { get; set; }
        public bool Active { get; set; }

        public bool HasMarketingOptIn { get; set; }
        public string MarketingOptInMessage { get; set; }
        public bool HasMultichoiceOptIn { get; set; }
        public bool HasRichMedia { get; set; }
        public bool AgeVerificationRequired { get; set; }
        public string ExternalUrl { get; set; }
        public string ExternalMobileUrl { get; set; }
        public string UrlName { get; set; }
        public string Disclaimer { get; set; }
        public string Prizes { get; set; }
        public bool RequireLogin { get; set; }
        public string EntryResponse { get; set; }
        public virtual string OptInMessage { get; set; }
        public bool Feature { get; set; }
        public bool AllowMultipleEntriesPerDay { get; set; }
        public virtual string ImageUrl { get { return ConfigurationManager.AppSettings["CompetitionImageUrl"]; } }

        public List<Models.Country> OpenToCountries { get; set; }
        public List<InputField> questionFields { get; set; }

        public virtual List<CompetitionWinner> CompetitionWinners { get { return this.getCompetitionWinners(); } }

        //private vars
        private static readonly CompetitionDAL compDal = new CompetitionDAL();
        private static readonly CompetitionEntryDAL compEntryDal = new CompetitionEntryDAL();
        private static readonly CompetitionWinnerDAL compWinnerDal = new CompetitionWinnerDAL();
        private static readonly CompetitionStatsDAL statsDal = new CompetitionStatsDAL();

        private static readonly int DAILY_ENTRY_MAX_PER_USER = 1;

        /// <summary>
        /// Saves a competition Entry by passing a competitionEntry object
        /// </summary>
        /// <param name="entry">CompetitionEntry Entity</param>
        /// <param name="competitionEntryRestriction"></    param>
        /// <returns>true if success, false if failure</returns>
        public bool Enter(CompetitionEntry entry, CompetitionEntryRestriction competitionEntryRestriction)
        {
           
           //check if entry must be allowed
            ValidateEntry(entry);
            entry.ContactNumber = Utilities.NumberCleanup(entry.ContactNumber);
            int dailyEntryCount = 1;

            //check if question daily limit has been reached
            if (competitionEntryRestriction == CompetitionEntryRestriction.OncePerDayPerQuestion)
            {
                dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerDay(entry.CompetitionId, entry.ContactNumber, entry.QuestionId, entry.IdNumber, entry.Email);
            }
            else if(competitionEntryRestriction == CompetitionEntryRestriction.OncePerQuestion)
            {
                dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerQuestion(entry.CompetitionId, entry.ContactNumber, entry.QuestionId, entry.IdNumber, entry.Email);
            }

            if (dailyEntryCount < DAILY_ENTRY_MAX_PER_USER)
            {
                compEntryDal.Enter(entry);
            }
            else
            {
                throw new ValidationException(new List<ValidationMessage>()
                {
                    new ValidationMessage(
                        "We have already received your entry for this question today, which is limited to one entry per day. You may still enter our other listed competitions.",
                        "CE14")
                });
            }

            return true;
        }

        public bool EnterNew(CompetitionEntryNew entry, CompetitionEntryRestriction competitionEntryRestriction)
        {

            //check if entry must be allowed
            ValidateEntryNew(entry);
            entry.ContactNumber = Utilities.NumberCleanup(entry.ContactNumber);
            int dailyEntryCount = 1;


            string[] tmpAns = entry.Answer.Split(',');
            //foreach (int queId in entry.QuestionId)
            for (int i = 0; i < entry.QuestionId.Length; i++)
            {
                //check if question daily limit has been reached
                if (competitionEntryRestriction == CompetitionEntryRestriction.OncePerDayPerQuestion)
                {
                    //dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerDay(entry.CompetitionId, entry.ContactNumber, queId, entry.IdNumber, entry.Email);
                    dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerDay(entry.CompetitionId, entry.ContactNumber, entry.QuestionId[i], entry.IdNumber, entry.Email);
                }
                else if (competitionEntryRestriction == CompetitionEntryRestriction.OncePerQuestion)
                {
                    dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerQuestion(entry.CompetitionId, entry.ContactNumber, entry.QuestionId[i], entry.IdNumber, entry.Email);
                }

                if (dailyEntryCount < DAILY_ENTRY_MAX_PER_USER)
                {
                    //compEntryDal.EnterNew(entry, queId, tmpAns[i]);
                    compEntryDal.EnterNew(entry, entry.QuestionId[i], tmpAns[i]);
                }
                else
                {
                    throw new ValidationException(new List<ValidationMessage>()
                {
                    new ValidationMessage(
                        "We have already received your entry for this question today, which is limited to one entry per day. You may still enter our other listed competitions.",
                        "CE14")
                });
                }
            }

            return true;
        }


        public int GetNumberOfEntries(CompetitionEntry entry)
        {
            return compEntryDal.CountCompetitionUserEntries(entry.CompetitionId, entry.ContactNumber, entry.IdNumber, entry.Email);
        }

        public void ValidateEntry(CompetitionEntry entry)
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            if (entry == null)
            {
                validationList.Add(new ValidationMessage("Competition Entry Object cannot be null", "CE1"));
            }
           
            if (string.IsNullOrEmpty(entry.Answer))
            {
                validationList.Add(new ValidationMessage("Answer is required", "CE2"));
            }

            if (entry.CompetitionId == 0)
            {
                validationList.Add(new ValidationMessage("CompetitionId is required", "CE3"));
            }
            if (string.IsNullOrEmpty(entry.Name))
            {
                validationList.Add(new ValidationMessage("Name is required", "CE5"));
            }
            if (string.IsNullOrEmpty(entry.Surname))
            {
                validationList.Add(new ValidationMessage("Surname is required", "CE6"));
            }
            if (string.IsNullOrEmpty(entry.City))
            {
                validationList.Add(new ValidationMessage("City is required", "CE13"));
            }
            if (string.IsNullOrEmpty(entry.CountryCode))
            {
                validationList.Add(new ValidationMessage("CountryCode is required", "CE13"));
            }
            if (string.IsNullOrEmpty(entry.ContactNumber))
            {
                validationList.Add(new ValidationMessage("Contact Number is required", "CE4"));
            }
            if (string.IsNullOrEmpty(entry.Email))
            {
                validationList.Add(new ValidationMessage("Email is required", "CE4"));
            }
            
            if (string.IsNullOrEmpty(entry.IpAddress))
            {
                validationList.Add(new ValidationMessage("Ip Adress is required", "CE7"));
            }

            Question q = new Question();
            List<Question> tmpQuest = Question.GetQuestions(this.Id);
            if(tmpQuest != null)
            {
                q = tmpQuest.Find(listQuestion => { return listQuestion.Id == entry.QuestionId; });
            }

            if (!q.Active)
            {
                validationList.Add(new ValidationMessage("Competition is closed", "CE8"));
            }

            if ((q.QuestionFormat == Question.QuestionType.MultiSelection) && (!string.IsNullOrEmpty(entry.Answer)))
            {
                string[] tmpAns = entry.Answer.Split('|');
                if ((tmpAns != null) && (tmpAns.Length != q.RequiredAnswersTotal))
                {
                    validationList.Add(new ValidationMessage("Please select at least " + q.RequiredAnswersTotal, "CE12"));
                }
            }

            if ((DateTime.Now < q.DateStart) || (DateTime.Now > q.DateEnd))
            {
                validationList.Add(new ValidationMessage("Competition is closed", "CE9"));
            }

            //if ((this.RequireLogin) && (string.IsNullOrEmpty(entry.ConnectId)))
            if (this.RequireLogin)
            {
                validationList.Add(new ValidationMessage("You must be logged in to enter this competition", "CE10"));
            }

            if (!this.AllowMultipleEntriesPerDay)
            {
                //To do:
                //Do check if user entered already for this competition on this day
            }

            if (!this.Active)
            {
                validationList.Add(new ValidationMessage("Competition is closed", "CE11"));
            }

            if (validationList.Count > 0)
            {
                throw new ValidationException(validationList);
            }
        }

        public void ValidateEntryNew(CompetitionEntryNew entry)
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            if (entry == null)
            {
                validationList.Add(new ValidationMessage("Competition Entry Object cannot be null", "CE1"));
            }

            if (string.IsNullOrEmpty(entry.Answer.Trim()))
            {
                validationList.Add(new ValidationMessage("Answer is required", "CE2"));
            }

            if (entry.CompetitionId == 0)
            {
                validationList.Add(new ValidationMessage("CompetitionId is required", "CE3"));
            }

            if ((entry.QuestionId == null))
            {
                validationList.Add(new ValidationMessage("QuestionId is required", "CE17"));
            }

            //////////if (string.IsNullOrEmpty(entry.Name.Trim()))
            //////////{
            //////////    validationList.Add(new ValidationMessage("Name is required", "CE5"));
            //////////}
            string name;
            name = entry.Name;
            if (string.IsNullOrEmpty(name))
            {
                validationList.Add(new ValidationMessage("Name is required", "CE5"));
            }
            //else
            //{

            //    Regex regex = new Regex(@"^[A-Za-z\s]*$");
            //    Match match = regex.Match(entry.Name);
            //    if (!match.Success)
            //        validationList.Add(new ValidationMessage(entry.Name + " is invalid Name", "CE5"));
            //}



            string surName;
            surName = entry.Surname;
            if (string.IsNullOrEmpty(surName))
            {
                validationList.Add(new ValidationMessage("Surname is required", "CE6"));
            }
            //else
            //{
            //    Regex regex = new Regex(@"^[A-Za-z\s]*$");
            //    Match match = regex.Match(entry.Surname);
            //    if (!match.Success)
            //        validationList.Add(new ValidationMessage(entry.Surname + " is invalid Surname", "CE6"));
            //}


            string Email;
            Email = entry.Email;
            if (string.IsNullOrEmpty(Email))
            {

                validationList.Add(new ValidationMessage("Email is required", "CE4"));
            }
            //else
            //{
            //    Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            //    Match match = regex.Match(entry.Email);
            //    if (!match.Success)
            //        validationList.Add(new ValidationMessage(entry.Email + " is Invalid Email Address", "CE4"));
            //}

            string ContactNumber;
            ContactNumber = entry.ContactNumber;
            if (string.IsNullOrEmpty(entry.ContactNumber))
            {
                validationList.Add(new ValidationMessage("Contact Number is required", "CE4"));
            }
            //else
            //{
            //    //Regex regex = new Regex("^[0-9]*$");
            //    //Regex regex = new Regex(@"(?<!\d)\d{10}(?!\d)");


            //    Regex regex = new Regex("^[0-9]{8,16}$");
            //    Match match = regex.Match(entry.ContactNumber);
            //    if (!match.Success)
            //        validationList.Add(new ValidationMessage(entry.ContactNumber + " is invalid Contact number", "CE4"));
            //}


            string IdNumber;
            IdNumber = entry.IdNumber;
            if (string.IsNullOrEmpty(IdNumber))
            {

                validationList.Add(new ValidationMessage("IdNumber is required", "CE16"));
            }

            string City;
            City = entry.City;
            if (string.IsNullOrEmpty(entry.City))
            {
                validationList.Add(new ValidationMessage("City is required", "CE13"));
            }
            //else
            //{
            //    Regex regex = new Regex(@"^[a-zA-Z]+$");
            //    Match match = regex.Match(entry.City);
            //    if (!match.Success)
            //        validationList.Add(new ValidationMessage(entry.City + " is invalid City", "CE13"));
            //}


            string countryCode;
            countryCode = entry.CountryCode;
            if (string.IsNullOrEmpty(countryCode))
            {
                validationList.Add(new ValidationMessage("CountryCode is required", "CE14"));

            }

            if (string.IsNullOrEmpty(entry.IpAddress))
            {
                validationList.Add(new ValidationMessage("Ip Adress is required", "CE7"));
            }

            if (entry.QuestionId != null && entry.Answer != null)
            {
                int noOfQues = entry.QuestionId.Length;
                string[] tmpAnswers = entry.Answer.Split(',');

                int noOfAns = tmpAnswers.Length;
                if (noOfQues != noOfAns)
                {
                    validationList.Add(new ValidationMessage("Please enter no of questionId is equal to no of Answers", "CE15"));
                }
                else
                {

                    Question q = new Question();
                    List<Question> tmpQuest = Question.GetQuestions(this.Id);

                    foreach (int i in entry.QuestionId)
                    {
                        if (tmpQuest != null)
                        {
                            q = tmpQuest.Find(listQuestion =>
                            {
                                if (listQuestion.Id == i) return true;
                                else
                                    return false;
                            });
                        }

                        //if (string.IsNullOrEmpty(q))
                        if (q == null)
                        {
                            validationList.Add(new ValidationMessage("Enter valid question id- " + i, "CE8"));
                        }
                        else
                        {

                            if (!q.Active)
                            {
                                validationList.Add(new ValidationMessage("Competition is closed for question- " + i, "CE8"));
                            }

                            if ((q.QuestionFormat == Question.QuestionType.MultiSelection) && (!string.IsNullOrEmpty(entry.Answer)))
                            {
                                string[] tmpAns = entry.Answer.Split('|');
                                if ((tmpAns != null) && (tmpAns.Length != q.RequiredAnswersTotal))
                                {
                                    validationList.Add(new ValidationMessage("Please select at least " + q.RequiredAnswersTotal + " answers for question " + i, "CE12"));
                                }
                            }

                            if ((DateTime.Now < q.DateStart) || (DateTime.Now > q.DateEnd))
                            {
                                validationList.Add(new ValidationMessage("Competition is closed", "CE9"));
                            }
                        }
                    }

                }
            }

            //if ((this.RequireLogin) && (string.IsNullOrEmpty(entry.ConnectId)))
            if (this.RequireLogin)
            {
                validationList.Add(new ValidationMessage("You must be logged in to enter this competition", "CE10"));
            }

            if (!this.AllowMultipleEntriesPerDay)
            {
                //To do:
                //Do check if user entered already for this competition on this day
            }

            if (!this.Active)
            {
                validationList.Add(new ValidationMessage("Competition is closed", "CE11"));
            }

            if (validationList.Count > 0)
            {
                throw new ValidationException(validationList);
            }
        }
    

        /// <summary>
        /// Saves/Updates the Current Comeptition instance to database
        /// </summary>
        /// <returns>true if success, false if failed</returns>
        public bool Save()
        {
            bool success = true;

            Validate();

            this.Id = compDal.Save(this);

            if (Questions != null)
            {
                Questions.ForEach(delegate(Question itm)
                {
                    itm.CompetitionId = this.Id;
                    itm.Save();
                });
            }

            return success;
        }

        /// <summary>
        /// Draw and saves Winners for a competition during a specified period, answer and if optIn is required
        /// </summary>
        /// <param name="startDate">Date from when the winner should be drawn</param>
        /// <param name="endDate">Date to when a winner should be drawn</param>
        /// <param name="answer">The required answer</param>
        /// <param name="optInRequired">Should it only be opted in users or all</param>
        /// <returns>List of CompetitionWinner Entities</returns>
        public List<CompetitionWinner> DrawWinners(int numberOfWinners, DateTime startDate, DateTime endDate, string answer, bool optInRequired)
        {
            List<CompetitionWinner> winners = new List<CompetitionWinner>();
            //To do Logic to draw winners

            winners = compWinnerDal.DrawWinner(this.Id, numberOfWinners, startDate, endDate, answer, optInRequired);

            return winners;
        }

        /// <summary>
        /// Gets all the drawn winners for this competition
        /// </summary>
        /// <returns>List of competition winners</returns>
        protected List<CompetitionWinner> getCompetitionWinners()
        {
            List<CompetitionWinner> winners = new List<CompetitionWinner>();

            winners = compWinnerDal.GetWinners(this.Id);

            return winners;
        }

        /// <summary>
        /// Gets all the entries for the competition based on search criterea
        /// </summary>
        /// /// <param name="amount">Amount of records to return</param>
        /// <param name="name">Name of entry</param>
        /// <param name="surname">Surname of entry</param>
        /// <param name="contactNumber">contact number of entry</param>
        /// <param name="email">email of entry</param>
        /// <returns></returns>
        public List<CompetitionEntry> SearchEntries(int amount, string name, string surname, string contactNumber, string email, DateTime startDate, DateTime enddDate)
        {
            List<CompetitionEntry> entries = new List<CompetitionEntry>();

            entries = compEntryDal.SearchCompetitionEntries(this.Id, amount, name, surname, contactNumber, email, startDate, enddDate);

            return entries;
        }

        /// <summary>
        /// Returns all the statistics for this competition for a given period
        /// </summary>
        /// <param name="fromDate">Date from which stats must calculated</param>
        /// <param name="toDate">Date to which stats must be calculated</param>
        /// <returns>CompetitionStats Entity</returns>
        ///                      
        public CompetitionStats GetStats(DateTime fromDate, DateTime toDate, string countryCode)
        {
            CompetitionStats stats = new CompetitionStats();
            stats = statsDal.GetStats(this.Id, fromDate, toDate, countryCode);

            return stats;
        }

        /// <summary>
        /// Validates the current instance of the competition and throws a ValidationException
        /// </summary>
        public void Validate()
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            //Name may not be null
            if (string.IsNullOrEmpty(this.Name))
            {
                validationList.Add(new ValidationMessage("Name is required", "C1"));
            }

            //url name
            if (string.IsNullOrEmpty(this.UrlName))
            {
                validationList.Add(new ValidationMessage("Url name is required", "C2"));
            }

            //To do: check if unique
            if (compDal.UrlNameExist(this.UrlName, this.Id))
            {
                validationList.Add(new ValidationMessage("Url name is not unique", "C7"));
            }

            //Dates
            if (this.DateStart == null)
            {
                validationList.Add(new ValidationMessage("Start date is required", "C3"));
            }

            if (this.DateEnd == null)
            {
                validationList.Add(new ValidationMessage("End date is required", "C4"));
            }

            if (this.DateEndCheck < this.DateStartCheck)
            {
                validationList.Add(new ValidationMessage("End date can not be before start date", "C5"));
            }

            //Countries
            if (this.OpenToCountries == null)
            {
                validationList.Add(new ValidationMessage("Please specify countries", "C6"));
            }

            if (validationList.Count > 0)
            {
                ValidationException exception = new ValidationException();
                exception.ValidationList = validationList;
                throw exception;
            }            
        }

        /// <summary>
        /// Gets a list of competitions from the database based on a searchval. Can also specify if only active compeititions should be returned
        /// </summary>
        /// <param name="searchVal">searchval to find competitions by</param>
        /// <param name="activeOnly">false to return active and inactive competitions, true to return only active competitions</param>
        /// <returns></returns>
        public static List<Models.Competition> GetCompetitions(string searchVal, bool activeOnly)
        {
            List<Models.Competition> comps = compDal.GetCompetitions(searchVal, activeOnly);

            return comps;
        }

        /// <summary>
        /// Gets a list of competitions from the database based on a searchval. Can also specify if only active compeititions should be returned
        /// </summary>
        /// <param name="searchVal">searchval to find competitions by</param>
        /// <param name="activeOnly">false to return active and inactive competitions, true to return only active competitions</param>
        /// <returns></returns>
        public static List<Models.Competition> GetCompetitionsLight(string searchVal, bool activeOnly)
        {
            List<Models.Competition> comps = compDal.GetCompetitionsLight(searchVal, activeOnly);

            return comps;
        }

        /// <summary>
        /// Gets a competition based on its unique identifier.
        /// </summary>
        /// <param name="id">Unique id</param>
        /// <returns>a Competition Entity</returns>
        public static Models.Competition GetCompetition(int id)
        {
            Models.Competition comp = compDal.GetCompetition(id);

            return comp;
        }

        /// <summary>
        /// Gets a competition based on its unique friendly urlname.
        /// </summary>
        /// <param name="urlname">Unique urlname as a string</param>
        /// <returns>a Competition Entity</returns>
        public static Models.Competition GetCompetition(string urlname)
        {
            Models.Competition comp = compDal.GetCompetition(urlname);

            return comp;
        }

        /// <summary>
        /// Gets a list of featured competitions from the database based
        /// </summary>
        /// <param name="countryCode">For a more country specific list add the country or leave blank</param>
        /// <returns></returns>
        public static List<Models.Competition> GetFeaturedCompetitions(string countryCode)
        {
            List<Models.Competition> comps = compDal.GetFeaturedCompetitions(countryCode);

            return comps;
        }

        /// <summary>
        /// Checks if a competitions urlname already exists in the database.
        /// </summary>
        /// <param name="urlname">urlname of a competition as a string</param>
        /// <param name="competitionId">competition Id that you are updating/ 0 if it is a new competition</param>
        /// <returns>if exists return true if not exists return false</returns>

        public static bool UrlNameExist(string urlname, int competitionId)
        {
            return compDal.UrlNameExist(urlname, competitionId);
        }

        /// <summary>
        /// Returns all sports in the SuperSort domain
        /// </summary>
        /// <returns>A dictionary of the sports in the SuperSport Domain</returns>

        public static Dictionary<string,int> GetSports()
        {
            return compDal.GetSports();
        }

        public static DataTable GetOptInWinners(int competitionId, DateTime startDate, DateTime endDate)
        {
            DataTable dt = compWinnerDal.GetoptWinners(competitionId, startDate, endDate);

            return dt;
        }

        public static DataTable GetMultichoiceOptInWinners(int competitionId, DateTime startDate, DateTime endDate)
        {
            DataTable dt = compWinnerDal.MultichoiceOpt(competitionId, startDate, endDate);

            return dt;
        }


        public static List<Models.PreviousCompetitionWinner> GetPreviousWinners(int year)
        {
            List<PreviousCompetitionWinner> previouswinnerList = compWinnerDal.GetPreviousWinners(year);

            return previouswinnerList;
        }


        public bool EnterRichMedia(CompetitionEntryRichMedia entry, CompetitionEntryRestriction competitionEntryRestriction)
        {

            //check if entry must be allowed
            ValidateEntryRichMedia(entry);
            entry.ContactNumber = Utilities.NumberCleanup(entry.ContactNumber);
            int dailyEntryCount = 1;


            //////string[] tmpAns = entry.Answer.Split(',');
         
            for (int i = 0; i < entry.QuestionId.Length; i++)
            {
                //check if question daily limit has been reached
                if (competitionEntryRestriction == CompetitionEntryRestriction.OncePerDayPerQuestion)
                {
                   
                    dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerDay(entry.CompetitionId, entry.ContactNumber, entry.QuestionId[i], entry.IdNumber, entry.Email);
                }
                else if (competitionEntryRestriction == CompetitionEntryRestriction.OncePerQuestion)
                {
                    dailyEntryCount = compEntryDal.CountQuestionUserEntriesPerQuestion(entry.CompetitionId, entry.ContactNumber, entry.QuestionId[i], entry.IdNumber, entry.Email);
                }

                if (dailyEntryCount < DAILY_ENTRY_MAX_PER_USER)
                {
               
                    compEntryDal.EnterRichMedia(entry, entry.QuestionId[i]);
                }
                else
                {
                    throw new ValidationException(new List<ValidationMessage>()
                {
                    new ValidationMessage(
                        "We have already received your entry for this question today, which is limited to one entry per day. You may still enter our other listed competitions.",
                        "CE14")
                });
                }
            }

            return true;
        }



        public void ValidateEntryRichMedia(CompetitionEntryRichMedia entry)
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            if (entry == null)
            {
                validationList.Add(new ValidationMessage("Competition Entry Object cannot be null", "CE1"));
            }

            //if (string.IsNullOrEmpty(entry.Answer.Trim()))
            //{
            //    validationList.Add(new ValidationMessage("Answer is required", "CE2"));
            //}

            if (entry.CompetitionId == 0)
            {
                validationList.Add(new ValidationMessage("CompetitionId is required", "CE3"));
            }

            if ((entry.QuestionId == null))
            {
                validationList.Add(new ValidationMessage("QuestionId is required", "CE17"));
            }

          
            string name;
            name = entry.Name;
            if (string.IsNullOrEmpty(name))
            {
                validationList.Add(new ValidationMessage("Name is required", "CE5"));
            }
          

            string surName;
            surName = entry.Surname;
            if (string.IsNullOrEmpty(surName))
            {
                validationList.Add(new ValidationMessage("Surname is required", "CE6"));
            }
          


            string Email;
            Email = entry.Email;
            if (string.IsNullOrEmpty(Email))
            {

                validationList.Add(new ValidationMessage("Email is required", "CE4"));
            }
            

            string ContactNumber;
            ContactNumber = entry.ContactNumber;
            if (string.IsNullOrEmpty(entry.ContactNumber))
            {
                validationList.Add(new ValidationMessage("Contact Number is required", "CE4"));
            }
         


            string IdNumber;
            IdNumber = entry.IdNumber;
            if (string.IsNullOrEmpty(IdNumber))
            {

                validationList.Add(new ValidationMessage("IdNumber is required", "CE16"));
            }

            string City;
            City = entry.City;
            if (string.IsNullOrEmpty(entry.City))
            {
                validationList.Add(new ValidationMessage("City is required", "CE13"));
            }
           


            string countryCode;
            countryCode = entry.CountryCode;
            if (string.IsNullOrEmpty(countryCode))
            {
                validationList.Add(new ValidationMessage("CountryCode is required", "CE14"));

            }

            if (string.IsNullOrEmpty(entry.IpAddress))
            {
                validationList.Add(new ValidationMessage("Ip Adress is required", "CE7"));
            }

            if (entry.QuestionId != null)
            {
            

                    Question q = new Question();
                    List<Question> tmpQuest = Question.GetQuestions(this.Id);

                    foreach (int i in entry.QuestionId)
                    {
                        if (tmpQuest != null)
                        {
                            q = tmpQuest.Find(listQuestion =>
                            {
                                if (listQuestion.Id == i) return true;
                                else
                                    return false;
                            });
                        }

                     
                        if (q == null)
                        {
                            validationList.Add(new ValidationMessage("Enter valid question id- " + i, "CE8"));
                        }
                        else
                        {

                            if (!q.Active)
                            {
                                validationList.Add(new ValidationMessage("Competition is closed for question- " + i, "CE8"));
                            }

                   

                            if ((DateTime.Now < q.DateStart) || (DateTime.Now > q.DateEnd))
                            {
                                validationList.Add(new ValidationMessage("Competition is closed", "CE9"));
                            }
                        }
                    }

               
            }

            //if ((this.RequireLogin) && (string.IsNullOrEmpty(entry.ConnectId)))
            if (this.RequireLogin)
            {
                validationList.Add(new ValidationMessage("You must be logged in to enter this competition", "CE10"));
            }

            if (!this.AllowMultipleEntriesPerDay)
            {
                //To do:
                //Do check if user entered already for this competition on this day
            }

            if (!this.Active)
            {
                validationList.Add(new ValidationMessage("Competition is closed", "CE11"));
            }

            if (validationList.Count > 0)
            {
                throw new ValidationException(validationList);
            }
        }
    }
}
