﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class InputField
    {
        public enum FieldType { Textbox, Checkbox, Dropdown }

        //Properties
        public int Id { get; set; }
        public string FieldText { get; set; }
        public FieldType FieldInputType { get; set; }
        public bool Required { get; set; }
    }
}
