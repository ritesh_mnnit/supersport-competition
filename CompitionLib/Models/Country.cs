﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class Country
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public bool IsSubSaharaCountry { get; set; }
        public bool IsSouthernAfrica { get; set; }

        private static readonly CountryDAL countryDal = new CountryDAL();

        public static List<Country> GetCountryList()
        {
            List<Country> countries = countryDal.GetCountries();

            return countries;
        }
    }
}
