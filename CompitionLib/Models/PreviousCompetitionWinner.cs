﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;
using SuperSport.CompetitionLib.Exceptions;
using System.Configuration;
using SuperSport.CompetitionLib.Utils;
using System.Text.RegularExpressions;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class PreviousCompetitionWinner
    {
        public string CompetitionName { get; set; }
        public string PreviousWinners { get; set; }
        
    }
}
