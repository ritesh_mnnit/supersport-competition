﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class CompetitionStats
    {
        public int TotalEntries { get; set; }
        public int UniqueEntries { get; set; }
        public int OptInEntries { get; set; }
        public int UniqueOptInEntries { get; set; }
        public int WebEntries { get; set; }
        public int MobileEntries { get; set; }
        public int UniqueWebEntries { get; set; }
        public int UniqueMobileEntries { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
        public List<Country> mainCountries { get; set; }
    }
}
