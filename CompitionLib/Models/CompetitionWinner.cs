﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class CompetitionWinner
    {
        public int Id { get; set; }
        public int EntryId { get; set; }
        public int CompetitionId { get; set; }
        public int QuestionId { get; set; }
        public bool ClaimedWinner { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CompetitionName { get; set; }
        public string PreviousWinners { get; set; }


        public CompetitionEntry EntryDetails { get { return compEntryDal.GetCompetitionEntry(this.EntryId); } }

        private static readonly CompetitionEntryDAL compEntryDal = new CompetitionEntryDAL();
        private static readonly CompetitionWinnerDAL compWinnerDal = new CompetitionWinnerDAL();

        public bool UpdateWinner(int winnerId, bool claimedWinner, string comment)
        {
            bool result = compWinnerDal.UpdateWinner(winnerId, claimedWinner, comment);
            return result;
        }

        public static List<CompetitionWinner> GetWinners(int competitionId)
        {
            List<CompetitionWinner> winnerList = compWinnerDal.GetWinners(competitionId);

            return winnerList;
        }


        public static List<PreviousCompetitionWinner> GetPreviousWinners(int year)
        {
            List<PreviousCompetitionWinner> previouswinnerList = compWinnerDal.GetPreviousWinners(year);

            return previouswinnerList;
        }
    }
}
