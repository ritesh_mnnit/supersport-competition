﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;
using SuperSport.CompetitionLib.Exceptions;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class Answer
    {
        // Properties
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string AnswerText { get; set; }
        public string ExtendedText { get; set; }
        public bool isCorrect { get; set; }
        public bool Active { get; set; }

        private static readonly AnswerDAL ansDAL = new AnswerDAL();

        public bool Save()
        {
            bool success = true;

            Validate();

            this.Id = ansDAL.Save(this);

            return success;
        }

        public void Validate()
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            //Question text may not be null
            if (string.IsNullOrEmpty(this.AnswerText))
            {
                validationList.Add(new ValidationMessage("Answer is required", "A1"));
            }

            if (validationList.Count > 0)
            {
                ValidationException exception = new ValidationException();
                exception.ValidationList = validationList;
                throw exception;
            }
        }

        public static List<Answer> GetAnswers(int questionId)
        {
            List<Answer> answerList = ansDAL.GetAnswers(questionId);

            return answerList;
        }

        public int GetVotes()
        {
            int total = ansDAL.GetVotes(this);

            return total;
        }
    }
}
