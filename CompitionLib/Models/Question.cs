﻿using System;
using System.Collections.Generic;

using System.Text;
using SuperSport.CompetitionLib.DAL;
using SuperSport.CompetitionLib.Exceptions;

namespace SuperSport.CompetitionLib.Models
{
    [Serializable]
    public class Question
    {
        public enum QuestionType { TextEntry, SingleSelection, MultiSelection }

        // Properties
        public int Id { get; set; }
        public int CompetitionId { get; set; }
        public string QuestionText { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public QuestionType QuestionFormat { get; set; }
        public int RequiredAnswersTotal { get; set; }

        public bool Active { get; set; }
        public List<Answer> Answers { get; set; }       

        //private vars
        private static readonly QuestionDAL questDal = new QuestionDAL();

        public bool Save()
        {
            bool success = true;

            Validate();

            this.Id = questDal.Save(this);

            //check if answers not null or empty
            if (Answers != null)
            {
                Answers.ForEach(delegate(Answer itm)
                {
                    itm.QuestionId = this.Id;
                    itm.Save();
                });
            }

            return success;
        }

        public void Validate()
        {
            List<ValidationMessage> validationList = new List<ValidationMessage>();

            //Question text may not be null
            if (string.IsNullOrEmpty(this.QuestionText))
            {
                validationList.Add(new ValidationMessage("Question is required", "Q1"));
            }

            //Question format not null
            if (this.QuestionFormat == null)
            {
                validationList.Add(new ValidationMessage("Question format is required", "Q2"));
            }

            //Dates
            if (this.DateStart == null)
            {
                validationList.Add(new ValidationMessage("Start date is required", "Q3"));
            }

            if (this.DateEnd == null)
            {
                validationList.Add(new ValidationMessage("End date is required", "Q4"));
            }

            if (this.DateEnd < this.DateStart)
            {
                validationList.Add(new ValidationMessage("End date can not be before start date", "Q5"));
            }

            if (validationList.Count > 0)
            {
                ValidationException exception = new ValidationException();
                exception.ValidationList = validationList;
                throw exception;
            }
        }

        public static List<Question> GetQuestions(int competitionId)
        {
            List<Question> questionList = questDal.GetQuestions(competitionId);

            return questionList;
        }
    }
}
