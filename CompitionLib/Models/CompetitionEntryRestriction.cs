﻿namespace SuperSport.CompetitionLib.Models
{
  public enum CompetitionEntryRestriction
  {
    OncePerDayPerQuestion,
    OncePerQuestion,

  }
}
