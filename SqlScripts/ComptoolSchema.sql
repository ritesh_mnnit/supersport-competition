USE [SuperSport]
GO

DROP TABLE [dbo].[comp_answers]
GO
DROP TABLE [dbo].[comp_users]
GO
DROP TABLE [dbo].[comp_shortcodes]
GO
DROP TABLE [dbo].[comp_entries]
GO
DROP TABLE [dbo].[comp_questions]
GO

/******DROP TABLE [dbo].[comp_countrycodes]
GO
******/
DROP TABLE [dbo].[comp_countries]
GO
DROP TABLE [dbo].[comp_competitions]
GO
DROP TABLE [dbo].[comp_questionFields]
GO
DROP TABLE [dbo].[comp_CompetitionWinner]
GO

/****** Object:  Table [dbo].[comp_users]    Script Date: 04/13/2011 11:11:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[comp_users](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [fullname] [varchar](100) NULL,
                [username] [varchar](50) NULL,
                [password] [varchar](50) NULL,
                [admin] [int] NULL,
                [active] [int] NULL,
                [deleted] [int] NULL,
                [lastsession] [datetime] NOT NULL,
                [session_count] [numeric](18, 0) NULL,
                [modified] [datetime] NOT NULL,
                [modifiedby] [varchar](255) NULL,
                [created] [datetime] NOT NULL,
                [guid] [varchar](50) NULL,
CONSTRAINT [PK_cm_users] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_admin]  DEFAULT ((0)) FOR [admin]
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_deleted]  DEFAULT ((0)) FOR [deleted]
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_lastsession]  DEFAULT ('2000-01-01 00:00') FOR [lastsession]
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_session_count]  DEFAULT ((0)) FOR [session_count]
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_modified]  DEFAULT ('2000-01-01 00:00') FOR [modified]
GO

ALTER TABLE [dbo].[comp_users] ADD  CONSTRAINT [DF_cm_users_sv_created]  DEFAULT ('2000-01-01 00:00') FOR [created]
GO












/****** Object:  Table [dbo].[comp_shortcodes]    Script Date: 04/13/2011 11:11:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_shortcodes](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [shortcode] [varchar](50) NULL,
CONSTRAINT [PK_comp_shortcodes] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO









/****** Object:  Table [dbo].[comp_questions]    Script Date: 04/13/2011 11:11:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_questions](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [comp_id] [int] NULL,
                [question] [text] NULL,
                [questionFormat] [varchar](255) NOT NULL,
                [date_start] [datetime] NULL,
                [date_end] [datetime] NULL,
                [active] [bit] NULL,
				[deleted] [bit] NULL,
                [modified] [datetime] NULL,
                [modifiedby] [varchar](255) NULL,
                [created] [datetime] default GetDate(),
CONSTRAINT [PK_comp_questions] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_questions] ADD  CONSTRAINT [DF_comp_questions_active]  DEFAULT ((0)) FOR [active]
GO

ALTER TABLE [dbo].[comp_questions] ADD  CONSTRAINT [DF_comp_questions_deleted]  DEFAULT ((0)) FOR [deleted]
GO











/****** Object:  Table [dbo].[comp_entries]    Script Date: 04/13/2011 11:11:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_entries](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [comp_id] [int] NULL,
                [question_id] [int] NULL,
                [name] [varchar](255) NULL,
                [surname] [varchar](255) NULL,
                [email] [varchar](255) NULL,
                [contactno] [varchar](20) NULL,
                [answer] [varchar](255) NULL,
                [source] [varchar](50) NULL,
                [ip_address] [varchar](50) NULL,
                [country_code] [varchar](50) NULL,
                [ip_country_code] [varchar](50) NULL,
				[receive_marketing] [bit] NULL,
				[connectId] [varchar] (350) NULL,
				[user_agent] [text] null,
                [date_entered] [datetime] default GetDate(),				
CONSTRAINT [PK_sc_entries] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_entries] ADD  CONSTRAINT [DF_comp_entries_comp_id]  DEFAULT ((0)) FOR [comp_id]
GO







/****** Object:  Table [dbo].[comp_countrycodes]    Script Date: 04/13/2011 11:10:59
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_countrycodes](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [code] [varchar](50) NULL,
                [name] [varchar](255) NULL,
                [ssa] [bit] NULL,
                [saf] [bit] NULL,
CONSTRAINT [PK_comp_countrycodes] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
******/








/****** Object:  Table [dbo].[comp_countries]    Script Date: 04/13/2011 11:10:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_countries](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [comp_id] [int] NULL,
                [country_code] [varchar](50) NULL,
CONSTRAINT [PK_comp_countries_1] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO






/****** Object:  Table [dbo].[comp_competitions]    Script Date: 04/13/2011 11:10:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_competitions](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [comp] [varchar](255) NULL,
				[urlname] [varchar](255) NULL,
                [date_start] [datetime] NULL,
                [date_end] [datetime] NULL,
                [header] [varchar](255) NULL,
                [footer] [varchar](255) NULL,
                [header_mobi] [varchar](255) NULL,
                [footer_mobi] [varchar](255) NULL,
				[thumbnail] [varchar] (255) NULL,
                [active] [bit] NULL,
				[prizes] [varchar] (1000) NULL,
				[disclaimer] [varchar] (1000) NULL,
				[require_login] [bit] NULL,
				[allow_multiple_entries_per_day] [bit] NULL,
				[entry_response] [varchar] (1000) NULL,
				[optin_message] [varchar] (1000) NULL,
                [modified] [datetime] NULL,
                [modifiedby] [varchar](255) NULL,
                [created] [datetime] default GetDate(),
CONSTRAINT [PK_comp_competitions] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_competitions] ADD  CONSTRAINT [DF_comp_competitions_active]  DEFAULT ((0)) FOR [active]
GO





/****** Object:  Table [dbo].[comp_answers]    Script Date: 04/13/2011 11:10:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_answers](
                [id] [int] IDENTITY(1,1) NOT NULL,
                [question_id] [int] NULL,
                [answer] [varchar](4000) NULL,
                [deleted] [bit] NULL,
                [modified] [datetime] NULL,
                [modifiedby] [varchar](255) NULL,
                [created] [datetime] default GetDate(),
CONSTRAINT [PK_comp_answers] PRIMARY KEY CLUSTERED 
(
                [id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_answers] ADD  CONSTRAINT [DF_comp_answers_deleted]  DEFAULT ((0)) FOR [deleted]
GO



/****** Object:  Table [dbo].[comp_questionFields]    Script Date: 05/03/2011 08:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_questionFields](
	[id] [int] NOT NULL,
	[fieldText] [varchar](255) NOT NULL,
	[fieldType] [varchar](100) NOT NULL,
	[systemField] [bit] NOT NULL,
 CONSTRAINT [PK_comp_questionFields] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

INSERT INTO [dbo].[comp_questionFields] (id, fieldText,fieldType, systemField) VALUES (1,'Name','0',1)
INSERT INTO [dbo].[comp_questionFields] (id, fieldText,fieldType, systemField) VALUES (2,'Surname','0',1)
INSERT INTO [dbo].[comp_questionFields] (id, fieldText,fieldType, systemField) VALUES (3,'Contact Number','0',1)
INSERT INTO [dbo].[comp_questionFields] (id, fieldText,fieldType, systemField) VALUES (4,'Email','0',1)
INSERT INTO [dbo].[comp_questionFields] (id, fieldText,fieldType, systemField) VALUES (5,'ID Number','0',1)






GO

/****** Object:  Table [dbo].[comp_CompetitionWinner]    Script Date: 06/07/2011 15:58:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[comp_CompetitionWinner](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[entryId] [int] NOT NULL,
	[competitionId] [int] NOT NULL,
	[questionId] [int] NOT NULL,
	[claimedWinner] [bit] NOT NULL,
	[comment] [varchar](255) NULL,
	[createdDate] [datetime] NOT NULL,
	[updatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CompetitionWinner] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[comp_CompetitionWinner] ADD  CONSTRAINT [DF_comp_CompetitionWinner_createdDate]  DEFAULT (getdate()) FOR [createdDate]
GO

ALTER TABLE [dbo].[comp_CompetitionWinner] ADD  CONSTRAINT [DF_comp_CompetitionWinner_updatedDate]  DEFAULT (getdate()) FOR [updatedDate]
GO




