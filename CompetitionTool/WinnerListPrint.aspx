﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WinnerListPrint.aspx.cs" Inherits="CompetitionTool.WinnersListPrint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="External/Scripts/general.js" type="text/javascript" language="javascript"></script>
    <link href="EXTERNAL/Styles/layout.css" rel="stylesheet" type="text/css" /><link href="External/Styles/text.css" rel="stylesheet" type="text/css" /><link href="External/Styles/controls.css" rel="stylesheet" type="text/css" /><link href="External/Styles/menu.css" rel="stylesheet" type="text/css" />
    <link href="External/Styles/print.css" rel="stylesheet" type="text/css" />
</head>
<body style="text-align: center;">
    <form id="form1" runat="server">
    <div class="print">
        <div class="header"><asp:Literal id="ltl1" runat="server" EnableViewState="false" /></div>
        <asp:Repeater ID="rpCompetitionWinners" runat="server" OnItemDataBound="rpCompetitionWinners_OnItemDataBound">
        <HeaderTemplate>
            <div class="txt_black_12" style="text-align:left; padding:3px; background-color:#FFFFFF; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>PREVIOUS WINNERS</strong></div>
                 <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	                <tr>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">NAME</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">SURNAME</td>
		                <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">CONTACT NUMBER</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">ID NUMBER</td>
		                <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">EMAIL</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">COUNTRY CODE</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">TOWN</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">ANSWER</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">CONFIRMED WINNER</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">DATE DRAWN</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">DATE ENTERED</td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">COMMENT</td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
	                <tr>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbName" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbSurname" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbContactNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbIdNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbEmail" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbCountry" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbTown" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbAnswer" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:CheckBox ID="chkConfirmedWinner" runat="server" Enabled="false" /></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbDateDrawn" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbDateEntered" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbComment" runat="server"></asp:Label></td>
	                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>                
            </FooterTemplate>
            </asp:Repeater>
    </div>
    <br /><br />
    </form>
</body>
</html>