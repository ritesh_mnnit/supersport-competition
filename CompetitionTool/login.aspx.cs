﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using CompetitionTool.Models;

namespace CompetitionTool
{
    public partial class Login : System.Web.UI.Page
    {
        private static readonly LoginDAL loginDal = new LoginDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Request.Cookies[ConfigurationManager.AppSettings["PrimaryCookie"]] != null)
            //{
            //    Response.Redirect("competitions.aspx");
            //}
        }

        protected void btLogin_Click(object sender, EventArgs e)
        {
            User user = loginDal.Login(tbUsername.Text, tbPassword.Text);

            if (user != null)
            {
                HttpCookie cookie = new HttpCookie(ConfigurationManager.AppSettings["PrimaryCookie"]);

                cookie.Values.Add("id", user.Id.ToString());
                cookie.Values.Add("name", user.Firstname);
                cookie.Values.Add("surname", user.Surname);               

                cookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(cookie);

                Response.Redirect("competitions.aspx");
            }
            else
            {
                lbErrorMessage.Visible = true;
            }
        }
    }
}