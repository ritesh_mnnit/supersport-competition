﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuestionPrint.aspx.cs" Inherits="CompetitionTool.QuestionPrint" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="External/Scripts/general.js" type="text/javascript" language="javascript"></script>
    <link href="EXTERNAL/Styles/layout.css" rel="stylesheet" type="text/css" /><link href="External/Styles/text.css" rel="stylesheet" type="text/css" /><link href="External/Styles/controls.css" rel="stylesheet" type="text/css" /><link href="External/Styles/menu.css" rel="stylesheet" type="text/css" />
    <link href="External/Styles/print.css" rel="stylesheet" type="text/css" />
</head>
<body style="text-align: center;">
    <form id="form1" runat="server">
    <div class="print">
        <div class="txt_black_12" style="text-align:left; padding:3px; background-color:#FFFFFF; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>QUESTIONS</strong></div>

     <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	    <tr>
            <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">QUESTION</td>
            <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">FORMAT</td>
		    <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">START</td>
		    <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_blue_10">END</td>
		    <%--<td align="left" valign="top" bgcolor="#F2F2F2">LAST MODIFIED</td>--%>
	    </tr>

        <asp:Repeater ID="repQuestionList" runat="server" OnItemDataBound="repQuestionList_OnItemDataBound"></asp:Repeater>
      
    </table>
    </div>
    <br /><br />
    </form>
</body>
</html>
