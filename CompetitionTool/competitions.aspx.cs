﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using SuperSport.CompetitionLib.Models;
using System.Globalization;

namespace CompetitionTool
{
    public partial class Competitions : System.Web.UI.Page
    {
        public int pcurr = 1;

        public int act = 0;

        public string search = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["pcurr"], "int"))
            {
                pcurr = Convert.ToInt32(Request.QueryString["pcurr"]);
            }

            if (ext.isNumericType(Request.QueryString["act"], "int"))
            {
                act = Convert.ToInt32(Request.QueryString["act"]);
            }

            search = Request.QueryString["search"];
            act = Convert.ToInt32(Request.QueryString["act"]);

            if (!Page.IsPostBack)
            {
                f_search.Text = search;
                f_activeonly.Checked = Convert.ToBoolean(act);

                //List<Competition> compList = Competition.GetCompetitions(search, Convert.ToBoolean(act));
                List<Competition> DupComps = Competition.GetCompetitions(search, Convert.ToBoolean(act));
                List<Competition> compList = new List<Competition>(); ;
                compList = DupComps.GroupBy(x => x.Id).Select(y => y.First()).ToList();

                if (compList != null)
                {
                    PagedDataSource tmpSource = new PagedDataSource();
                    tmpSource.DataSource = compList;
                    tmpSource.AllowPaging = true;
                    tmpSource.PageSize = 20;
                    if (pcurr < 1) { pcurr = 1; }
                    if (pcurr > tmpSource.PageCount) { pcurr = tmpSource.PageCount; }
                    f_page.Text = pcurr.ToString();
                    tmpSource.CurrentPageIndex = pcurr - 1;
                    if (tmpSource.IsFirstPage) { btn_prev.Enabled = false; }
                    if (tmpSource.IsLastPage) { btn_next.Enabled = false; }
                    lit_totalpages.Text = tmpSource.PageCount.ToString();
                    rep_list.DataSource = tmpSource;
                    rep_list.DataBind();
                }
            }
        }

        protected void rep_list_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                string[] allowedFormats = { "MM/dd/yyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy" };
                DateTime startDate;
                DateTime endDate;
                SuperSport.CompetitionLib.Models.Competition item = (SuperSport.CompetitionLib.Models.Competition)e.Item.DataItem;
                
                HtmlGenericControl wrapList = new HtmlGenericControl("div");

                StringBuilder outStr = new StringBuilder();

                string rowCol = "txt_black_10";

                if (item.Active == false)
                {
                    rowCol = "txt_red_10";
                }

                DateTime.TryParseExact(item.DateStart, allowedFormats, CultureInfo.CurrentUICulture, DateTimeStyles.None, out startDate);
                DateTime.TryParseExact(item.DateEnd, allowedFormats, CultureInfo.CurrentUICulture, DateTimeStyles.None, out endDate);

                outStr.Append("<tr style=\"background-color:#FFFFFF;\" id=\"row_"+ item.Id +"\" onmouseover=\"ChangeRowColour(this.id,'#F2F2F2')\" onmouseout=\"ChangeRowColour(this.id,'#FFFFFF')\">");

                outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\">");
                outStr.Append("        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                outStr.Append("	            <tr>");
                outStr.Append("		            <td class=\"txt_black_10\" style=\"padding:0 3px 0 0; white-space:nowrap;\"><a href=\"CompetitionDetails.aspx?edit=1&id=" + item.Id + "\">OPEN / EDIT</a></td>");
                //outStr.Append("		        <td class=\"txt_red_10\" style=\"padding:0 0 0 3px\"><a href=\"xxx\">DELETE</a></td>");
                outStr.Append("	            </tr>");
                outStr.Append("        </table>");
                outStr.Append("    </td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.Name + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.UrlName + "</td>");
                //outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateStart.ToString("dd MMMM yyyy") + "</td>");
                //outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateEnd.ToString("dd MMMM yyyy") + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + startDate.ToString("dd MMMM yyyy") + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + endDate.ToString("dd MMMM yyyy") + "</td>");
                //outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\""+ rowCol +"\">Modified XXXX</td>");
                outStr.Append("</tr>");

                wrapList.InnerHtml = outStr.ToString();

                e.Item.Controls.Add(wrapList);
            }
        }

        protected void btn_ok_Click(object sender, EventArgs e)
        {
            Response.Redirect("Competitions.aspx?pcurr=" + f_page.Text + "&search=" + f_search.Text + "&act=" + Convert.ToInt32(f_activeonly.Checked));
        }

        protected void btn_prev_Click(object sender, EventArgs e)
        {
            Response.Redirect("Competitions.aspx?pcurr=" + (pcurr - 1).ToString() + "&search=" + f_search.Text + "&act=" + Convert.ToInt32(f_activeonly.Checked));
        }

        protected void btn_next_Click(object sender, EventArgs e)
        {
            Response.Redirect("Competitions.aspx?pcurr=" + (pcurr + 1).ToString() + "&search=" + f_search.Text + "&act=" + Convert.ToInt32(f_activeonly.Checked));
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            Response.Redirect("Competitions.aspx?pcurr=" + f_page.Text + "&search=" + f_search.Text + "&act=" + Convert.ToInt32(f_activeonly.Checked));
        }
    }
}