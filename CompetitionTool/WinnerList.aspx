﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="WinnerList.aspx.cs" Inherits="CompetitionTool.WinnerList" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" runat="server">

    <%= ext.buildPageHeading("WINNER LIST - " + competitionName)%>

</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">
    
    <asp:ScriptManager ID="sm_winnerlist" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">
               
        <%=ext.buildBackCancel("BACK TO COMPETITION","CompetitionDetails.aspx?edit=1&id=" + compId, true) %>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <div>
        <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>DETAILS</strong></div>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATES&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF">

                    <table>
                        <tr>
                            <td width="200">
                                <asp:UpdatePanel ID="up_datestart" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datestart" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateStartHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>START DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateStart" runat="server" ControlToValidate="txtDateStartHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                            <asp:Calendar ID="tbStartDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbStartDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpStartTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                            <td width="1"></td>
                            <td width="200">
                                <asp:UpdatePanel ID="up_dateend" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datesend" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateEndHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>END DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateEnd" runat="server" ControlToValidate="txtDateEndHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                            <asp:Calendar ID="tbEndDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbEndDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpEndTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbAnswer" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>
            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">OPT IN REQUIRED&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:CheckBox ID="chkOptInRequired" runat="server" /></td>
            </tr>
            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NUMBER OF WINNERS&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:DropDownList id="drpAmount" runat="server"></asp:DropDownList></td>
            </tr>
                                    
            <%--<tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DISCLAIMER</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="f_disclaimer" runat="server" TextMode="MultiLine" Rows="4" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>--%>

        </table>
        <%=ext.makeSpace(10,"",true) %>
        <asp:button runat="server" ID="btDrawWinners" CssClass="btn_grey" 
            Text="DRAW WINNERS" onclick="btDrawWinners_Click"  />
    </div>

    <%=ext.makeSpace(20,"",true) %>


        <asp:Repeater ID="rpCompetitionWinnersDraw" runat="server" OnItemDataBound="rpCompetitionWinners_OnItemDataBound">        
            <HeaderTemplate>
            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>WINNERS</strong></div>
                 <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	                <tr>
                        <td width="65" align="center" valign="top" bgcolor="#F2F2F2" class="txt_darkgrey_10" style="white-space:nowrap;"></td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NAME</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SURNAME</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONTACT NUMBER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ID NUMBER</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EMAIL</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COUNTRY CODE</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CITY</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONNECT ID</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COMMENT</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONFIRMED WINNER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATE DRAWN</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATE ENTERED</td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
	                <tr style="background-color:#FFFFFF;" id="<%#DataBinder.Eval(Container,"dataitem.id")%>" onmouseover="ChangeRowColour(this.id,'#F2F2F2')" onmouseout="ChangeRowColour(this.id,'#FFFFFF')">
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbLink" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbName" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbSurname" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbContactNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbIdNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbEmail" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbCountry" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lblCity" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbAnswer" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbConnectId" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbComment" runat="server"></asp:Label></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:CheckBox ID="chkConfirmedWinner" runat="server" Enabled="false" /></td>
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbDateDrawn" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" bgcolor="#FFFFFF" class="txt_black_10"><asp:Label ID="lbDateEntered" runat="server"></asp:Label></td>                        
	                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>                
            </FooterTemplate>
        </asp:Repeater>
        <asp:Label ID="lbDrawResponse" runat="server" Text="No winners could be found for the selected criteria" Visible="false"></asp:Label>

    <%=ext.makeSpace(20,"",true) %>


        <asp:Repeater ID="rpCompetitionWinners" runat="server" OnItemDataBound="rpCompetitionWinners_OnItemDataBound">        
            <HeaderTemplate>
            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>PREVIOUS WINNERS</strong></div>
                 <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	                <tr>
                        <td width="65" align="center" valign="top" bgcolor="#F2F2F2" class="txt_darkgrey_10" style="white-space:nowrap;"></td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NAME</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SURNAME</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONTACT NUMBER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ID NUMBER</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EMAIL</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COUNTRY CODE</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CITY</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONNECT ID</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COMMENT</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONFIRMED WINNER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATE DRAWN</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATE ENTERED</td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
	                <tr style="background-color:#FFFFFF;" id="<%#DataBinder.Eval(Container,"dataitem.id")%>" onmouseover="ChangeRowColour(this.id,'#F2F2F2')" onmouseout="ChangeRowColour(this.id,'#FFFFFF')">
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_black_10"><asp:Label ID="lbLink" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbName" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbSurname" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbContactNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbIdNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbEmail" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbCountry" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lblCity" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbAnswer" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbConnectId" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbComment" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:CheckBox ID="chkConfirmedWinner" runat="server" Enabled="false" /></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbDateDrawn" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbDateEntered" runat="server"></asp:Label></td>
	                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>                
            </FooterTemplate>
        </asp:Repeater>
        <div class="txt_blue_10 printlink">
            <asp:HyperLink ID="lnkprint" runat="server" Text="Print version" />
        </div>
</asp:Content>
