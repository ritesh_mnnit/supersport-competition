﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool.CompetitionEntry
{
    public partial class ucCompetition : System.Web.UI.UserControl
    {
        private SuperSport.CompetitionLib.Models.Competition comp;
        private int id = 1;
        private const string AnswerControlPrefix = "compAnswer_";
        private const string ImagePath = "http://images.supersport.co.za/";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            comp = SuperSport.CompetitionLib.Models.Competition.GetCompetition(id);

            //render comp details
            renderCompetition();
        }

        private void renderCompetition()
        {
            if (comp != null)
            {
                lblHeader.Text = comp.Name;

                Question quest = comp.Questions.Find(q => { return (q.DateStart <= DateTime.Now) && (q.DateEnd >= DateTime.Now); });

                if (quest != null)
                {
                    lblQuestion.Text = quest.QuestionText;
                    lbClosingDate.Text = quest.DateEnd.ToString("dd MMM yyyy");
                    renderAnswers(quest);
                }

                if (!string.IsNullOrEmpty(comp.Prizes))
                {
                    ltrlPrizes.Text = comp.Prizes;
                }
                else
                {
                    ltrlPrizes.Visible = false;
                }

                if (!string.IsNullOrEmpty(comp.HeaderImageWeb))
                {
                    imgHeaderImage.ImageUrl = ImagePath + comp.HeaderImageWeb;
                }
                else
                {
                    imgHeaderImage.Visible = false;
                }

                if (!string.IsNullOrEmpty(comp.FooterImageWeb))
                {
                    imgFooterImage.ImageUrl = ImagePath + comp.FooterImageWeb;
                }
                else
                {
                    imgFooterImage.Visible = false;
                }

                if (!string.IsNullOrEmpty(comp.Disclaimer))
                {
                    ltrlDisclaimer.Text = comp.Disclaimer;
                }
                else
                {
                    ltrlDisclaimer.Visible = false;
                }
            }
        }

        private void renderAnswers(Question quest)
        {
            RequiredFieldValidator rqAnswer = new RequiredFieldValidator();
            rqAnswer.ID = "rqAnswer";
            rqAnswer.ErrorMessage = "* Please supply an answer";
            rqAnswer.ValidationGroup = "compForm";

            if (quest.QuestionFormat == Question.QuestionType.SingleSelection)
            {
                RadioButtonList rdAnswerList = new RadioButtonList();
                rdAnswerList.ID = AnswerControlPrefix + quest.CompetitionId + "_" + quest.Id.ToString() + "_";

                foreach (Answer a in quest.Answers)
                {
                    ListItem itm = new ListItem(a.AnswerText);
                    rdAnswerList.Items.Add(itm);
                }
                pnlAnswers.Controls.Add(rdAnswerList);
                rqAnswer.ControlToValidate = rdAnswerList.ID;
                pnlAnswers.Controls.Add(rqAnswer);
            }
            else if (quest.QuestionFormat == Question.QuestionType.MultiSelection)
            {
                CheckBoxList chckAnswerList = new CheckBoxList();
                chckAnswerList.ID = AnswerControlPrefix + quest.CompetitionId + "_" + quest.Id.ToString() + "_";

                foreach (Answer a in quest.Answers)
                {
                    ListItem itm = new ListItem(a.AnswerText);
                    chckAnswerList.Items.Add(itm);
                }
                pnlAnswers.Controls.Add(chckAnswerList);
            }
            else if (quest.QuestionFormat == Question.QuestionType.TextEntry)
            {
                TextBox answer = new TextBox();
                answer.ID = AnswerControlPrefix + quest.CompetitionId + "_" + quest.Id.ToString() + "_";
                pnlAnswers.Controls.Add(answer);
                rqAnswer.ControlToValidate = answer.ID;
                pnlAnswers.Controls.Add(rqAnswer);
            }                                  
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int competitionId = 0;
            int questionId = 0;
            string answer = "";

            foreach (string s in this.Request.Form.Keys)
            {
                if (s.Contains(AnswerControlPrefix))
                {
                    if (!string.IsNullOrEmpty(answer))
                    {
                        answer += ",";
                    }
                    answer += Request.Form[s];
                    string[] tmpstring = s.Split('_');
                    competitionId = Convert.ToInt32(tmpstring[2]);
                    questionId = Convert.ToInt32(tmpstring[3]);
                }
            }
            
            Question quest = null;

            if ((comp != null) && (comp.Questions != null))
            {
                quest = this.comp.Questions.Find(q => { return (q.Id == Convert.ToInt32(questionId)); });
            }

            if (quest != null)
            {
                SuperSport.CompetitionLib.Models.CompetitionEntry compEntry = new SuperSport.CompetitionLib.Models.CompetitionEntry();
                compEntry.Name = txtName.Text;
                compEntry.Surname = txtSurname.Text;
                compEntry.IpAddress = "127.0.0.1";
                compEntry.Answer = answer;
                compEntry.CompetitionId = comp.Id;
                compEntry.QuestionId = quest.Id;
                compEntry.ContactNumber = txtContactNumber.Text;
                compEntry.CountryCode = "ZA";
                compEntry.Email = txtEmail.Text;
                compEntry.ReceiveMarketing = chckFeedback.Checked;
                compEntry.UserAgent = Request.UserAgent.ToString();
                compEntry.Source = SuperSport.CompetitionLib.Models.CompetitionEntry.EntryLocation.Web;

                comp.Enter(compEntry,CompetitionEntryRestriction.OncePerDayPerQuestion);
            }
            
        }
    }
}