﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCompetition.ascx.cs" Inherits="CompetitionTool.CompetitionEntry.ucCompetition" %>

<div class="block640">
        <div style="background: url(http://www.supersport.com/images/fxitures_results_header.jpg) repeat-x; padding:10px; background-color:#000000; margin: 0 0 10px 0;"><h2 style="color:#ffffff;">Competitions</h2></div>


    <div id="article">
            <div class="articleImage">
			    <table id="ContentPlaceHolder1_tbl_image" cellspacing="0" cellpadding="5" align="left" style="width:180px;border-collapse:collapse;">
	                <tbody>
                        <tr>
		                    <td><asp:Image ID="imgHeaderImage" runat="server" /></td>
	                    </tr>
                    </tbody>
                </table>
			</div>
			<div id="articlecontent">
			    <h1><asp:Label ID="lblHeader" runat="server"></asp:Label></h1>
                <hr class="dotted">
                <div style="padding-top:10px;">
                    <span style="width: 100%;" class="metadata">Closes <asp:Label ID="lbClosingDate" runat="server" CssClass="author"></asp:Label></span><br>

		        </div>
             </div>
		    <p>
                <asp:Literal ID="ltrlPrizes" runat="server">
                </asp:Literal>
            </p>
    </div>
               
<div style="clear:both;"></div>
<asp:Panel id="pnlCompetition" runat="server">
    <div>
        <h2>Question</h2>
        <asp:Label ID="lblQuestion" runat="server" Text="">
        </asp:Label>
    </div>

    <asp:Panel ID="pnlAnswers" runat="server">
    </asp:Panel>

    <div style="height:1px; border-bottom:#000 dotted 1px; margin-top:10px; margin-bottom:10px;"></div>

    <asp:Panel ID="pnlUserDetails" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="3" style="margin-bottom:10px;">
	        <tbody>
                <tr>
		            <td width="150" align="left" valign="top">Name</td>
		            <td align="left" valign="top"><asp:TextBox ID="txtName" runat="server" CssClass="inputType"></asp:TextBox><span class="warningMessage">*</span></td>
                    <td>
                        <asp:RequiredFieldValidator ID="rqName" runat="server" ControlToValidate="txtName" ValidationGroup="compForm" ErrorMessage="* Your name is required"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="rgName" runat="server" ControlToValidate="txtName" ValidationGroup="compForm" ErrorMessage="! Please enter a valid name" ValidationExpression="^[a-zA-Z-\\s]{1,40}$"></asp:RegularExpressionValidator>
                    </td>
	            </tr>
	            <tr>
		            <td align="left" valign="top">Surname</td>
		            <td align="left" valign="top"><asp:TextBox ID="txtSurname" runat="server" CssClass="inputType"></asp:TextBox><span class="warningMessage">*</span></td>
                    <td>
                        <asp:RequiredFieldValidator ID="rqSurname" runat="server" ControlToValidate="txtSurname" ValidationGroup="compForm" ErrorMessage="* Your contact number is required"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="rgSurname" runat="server" ControlToValidate="txtSurname" ValidationGroup="compForm" ErrorMessage="! Please enter a valid surname" ValidationExpression="^[a-zA-Z-\\s]{1,40}$"></asp:RegularExpressionValidator>
                    </td>
	            </tr>
	            <tr>
		            <td align="left" valign="top">Contact Number</td>
		            <td align="left" valign="top"><asp:TextBox ID="txtContactNumber" runat="server" CssClass="inputType"></asp:TextBox><span class="warningMessage">*</span></td>
                    <td>
                        <asp:RequiredFieldValidator ID="rqContactNumber" runat="server" ControlToValidate="txtContactNumber" ValidationGroup="compForm" ErrorMessage="* Your contact number is required"></asp:RequiredFieldValidator><br />
                        <asp:RegularExpressionValidator ID="rgContactNumber" runat="server" ControlToValidate="txtContactNumber" ValidationGroup="compForm" ErrorMessage="! Please enter a valid contact number" ValidationExpression="^[0-9\\s\\(\\)\\+\\-]{8,16}$"></asp:RegularExpressionValidator>
                    </td>
	            </tr>
	            <tr>
		            <td align="left" valign="top">Email</td>
		            <td align="left" valign="top"><asp:TextBox ID="txtEmail" runat="server" CssClass="inputType"></asp:TextBox><span class="warningMessage">*</span></td>
                    <td>
                        <asp:RegularExpressionValidator ID="rgEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="compForm" ErrorMessage="! Please enter a valid email address" ValidationExpression="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$"></asp:RegularExpressionValidator>
                    </td>
	            </tr>
            </tbody>
        </table>

        <div>
            <asp:CheckBox ID="chckFeedback" runat="server" Checked="true" Text="Receive marketing feedback" />
        </div>
    </asp:Panel>

    <div style="height:1px; border-bottom:#000 dotted 1px; margin-top:10px; margin-bottom:10px;"></div>

    <asp:Literal ID="ltrlDisclaimer" runat="server"></asp:Literal>

    <div style="text-align: center;">
        <asp:Button ID="btnSubmit" runat="server" CssClass="btn_red" 
            onclick="btnSubmit_Click" Text="Enter" ValidationGroup="compForm" />
    </div>
</asp:Panel>

<asp:Panel ID="pnlResponse" runat="server">

</asp:Panel>

<asp:Image ID="imgFooterImage" runat="server" />
</div>