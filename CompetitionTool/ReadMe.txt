﻿(Version 1.0, 9592)
ReadMe
------
This is a Read Me file to install the SuperSport Competition Tool.
Contents of this file are:
1.Prerequisites
2.Installation Details
3.Release Notes

1. Prerequisites
----------------
1.1 IIS 7
1.2 Access to SuperSport MS SQL database schema

2. Installation Details
-----------------------
2.1 Setup folder on IIS as a .Net 4 Application. (App pool can use integrated mode)
2.2 Open web.config and configure the following settings according to the environment
	2.2.1 connectionStrings (za-sql.supersport.com for local lan testing and za-sql.dstvo.local for production)
	2.2.2 when deploying to production make sure customErrors mode="On"
	2.2.3 when deploying to production make sure compilation debug="false"
	2.2.4 <add key="CompetitionImageUrl" value="http://theserver/compGraphics/" /> must point the url where the images will be read from
2.3 Delete/Replace all existing files/folders on deployment server that is contained in the release
2.4 Setup a map path for directory compGraphics to upload images (Provide security access)
2.5 Run SqlScripts/ComptoolSchema.sql if the competition db schema does not exist

3. Release Notes
-----------------
Version 1.0 2011/06/23
-Updated previous Winners to be only eligble every 6 months
-Implemented Check on the urlname so that there can't be duplicates
-implemented the active checkbox on the answers for questions. Answers can now be activated/de-activated

Version 1.0 2011/06/13
- Updated image path when displaying logo images

Version 1.0 2011/06/06
 - Added login

Version 1.0 2011/06/02
	Fixes
 - If a competition is not saved the admin user must not be able to click on the other tabs e.g.: Winners, Entries and Stats.
 - Error message are not displayed when the admin user click on save without entering the required fields on Creating Competition, Question and Answer.
 - Create an Answer page click on ‘Back to Question” tab does not redirects the user to the Question page.
 - If no question are assigned to a competition, the text “No question Created” is not displayed.


Version 1.0 2011/05/31
 - beta release of the competition tool