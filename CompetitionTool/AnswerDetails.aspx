﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AnswerDetails.aspx.cs" Inherits="CompetitionTool.AnswerDetails"  ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" Runat="Server">

    <%= ext.buildPageHeading("ANSWER DETAILS - " + question)%>

</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">

<asp:ScriptManager ID="sm_competitiondetails" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">
        
        <%--<asp:button runat="server" ID="btn_save" CssClass="btn_grey" Text="SAVE DETAILS" onclick="btn_save_Click" />--%>
        <asp:button runat="server" ID="btn_saveback" CssClass="btn_grey" Text="SAVE AND GO BACK TO QUESTION" onclick="btn_saveback_Click" />
        <%=ext.buildBackCancel("BACK TO QUESTION","QuestionDetails.aspx?edit=1&id=" + quesId + "&compId="+ compId, true) %>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <asp:Panel ID="panException" runat="server" CssClass="txt_red_10" Visible="false">
        <asp:Literal ID="litException" runat="server"></asp:Literal>
        <%=ext.makeSpace(20,"",true) %>
    </asp:Panel>

    <asp:Panel ID="panCompetition" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
                <td align="left" valign="top">

                    <div class="txt_black_10">
                        <asp:CheckBox ID="chk_active" runat="server" CssClass="txt_red_10" Text="Activate / De-Activate this answer" />
                    </div>

                    <%=ext.makeSpace(10,"",true) %>

                    <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>DETAILS</strong></div>

                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER TEXT&nbsp;<asp:RequiredFieldValidator ID="reqAnswerText" runat="server" ControlToValidate="f_answertext" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="f_answertext" runat="server" TextMode="MultiLine" Rows="5" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EXTENDED TEXT</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="txExtendedText" runat="server" TextMode="MultiLine" Rows="5" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                    </table>

                    <%=ext.makeSpace(10,"",true) %>

                    <div class="txt_black_10">
                        <asp:CheckBox ID="chk_correct" runat="server" CssClass="txt_red_10" Text="&nbsp;Is Answer" />
                    </div>

                </td>
	        </tr>
        </table>
    </asp:Panel>

</asp:Content>


