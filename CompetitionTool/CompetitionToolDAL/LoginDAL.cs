﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SuperSport.DBUtility;
using CompetitionTool.Models;
using System.Data;

namespace CompetitionTool
{
    public class LoginDAL
    {
        private const string SQL_SELECT_USER = "SELECT Id, Firstname, Surname, Username, Password  FROM SuperSportZone.dbo.zoneusers WHERE username=@username AND password=@password";

        /// <summary>
        /// Reads the defined answers for a question based on the unique question Id
        /// </summary>
        /// <param name="quesId">unique Answer Id</param>
        /// <returns>List of Answer Entities</returns>
        public User Login(string username, string password)
        {
            User user = null;

            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@username", username));
            parms.Add(new SqlParameter("@password", password));

            //Execute a query to read the answer details
            using (SqlDataReader rdr = SqlHelper.ExecuteReader(SqlHelper.ConnectionStringRead, CommandType.Text, SQL_SELECT_USER, parms.ToArray()))
            {
                while (rdr.Read())
                {
                    user = new User();
                    //code to test using reflection to assign values from sql to same name class attributes
                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        string t = (rdr.GetName(i)).ToString();
                        //do check if field exist
                        if (user.GetType().GetProperty(rdr.GetName(i)) != null)
                        {
                            user.GetType().GetProperty(rdr.GetName(i)).SetValue(user, rdr[i], null);
                        }
                    }
                }
            }

            return user;
        }
    }
}