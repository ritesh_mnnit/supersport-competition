﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="WinnerUpdate.aspx.cs" Inherits="CompetitionTool.WinnerUpdate" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" Runat="Server">

    <%= ext.buildPageHeading("ANSWER DETAILS")%>

</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">

<asp:ScriptManager ID="sm_competitiondetails" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">
        
        <%--<asp:button runat="server" ID="btn_save" CssClass="btn_grey" Text="SAVE DETAILS" onclick="btn_save_Click" />--%>
        <asp:button runat="server" ID="btn_saveback" CssClass="btn_grey" Text="SAVE AND GO BACK TO WINNERS LIST" onclick="btn_saveback_Click" />
        <%--<%=ext.buildBackCancel("BACK TO WINNERS LIST","WinnerList.aspx?edit=1&id=" + compId, true) %>--%>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <asp:Panel ID="panException" runat="server" CssClass="txt_red_10" Visible="false">
        <asp:Literal ID="litException" runat="server"></asp:Literal>
        <%=ext.makeSpace(20,"",true) %>
    </asp:Panel>

    <asp:Panel ID="panCompetition" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
                <td align="left" valign="top">

                    <div class="txt_black_10">
                        <asp:CheckBox ID="chk_confirm" runat="server" CssClass="txt_red_10" Text="Confirmed winner" />
                    </div>

                    <%=ext.makeSpace(10,"",true) %>

                    <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>COMMENTS</strong></div>

                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
                        <tr>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="f_comment" runat="server" TextMode="MultiLine" Rows="5" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                    </table>

                </td>
	        </tr>
        </table>
    </asp:Panel>

</asp:Content>


