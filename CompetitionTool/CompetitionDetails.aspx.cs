﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using SuperSport.CompetitionLib.Models;
using SuperSport.CompetitionLib.Exceptions;
using System.IO;
using System.Globalization;
using System.Data;
using OfficeOpenXml;

namespace CompetitionTool
{
    public partial class CompetitionDetails : System.Web.UI.Page
    {
        public int id;
        public int edit;
        public string urlname = string.Empty;

        private Competition selectedComp;

        protected void Page_Load(object sender, EventArgs e)
        {
            PopulateDropDownList();

            if (ext.isNumericType(Request.QueryString["id"], "int"))
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (ext.isNumericType(Request.QueryString["edit"], "int"))
            {
                edit = Convert.ToInt32(Request.QueryString["edit"]);
            }

            if (edit == 1)
            {
                selectedComp = Competition.GetCompetition(id);
                pnlStats.Visible = true;
            }

            if (!Page.IsPostBack)
            {
                buildCountryList();

                if (edit == 1)
                {
                    Competition thisComp = selectedComp;//Competition.GetCompetition(id);

                    if (thisComp != null)
                    {                        
                        urlname = thisComp.UrlName;

                        //DateTime.ParseExact("YouDateString", "dd-MM-yyyy", CultureInfo.InvariantCulture)
                        //    yyyy-MM-dd

                        string[] allowedFormats = { "MM/dd/yyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy" };
                        DateTime startDate;
                        DateTime endDate;

                        DateTime.TryParseExact(thisComp.DateStart, allowedFormats, CultureInfo.CurrentUICulture, DateTimeStyles.None, out startDate);
                        DateTime.TryParseExact(thisComp.DateEnd, allowedFormats, CultureInfo.CurrentUICulture, DateTimeStyles.None, out endDate);

                        cal_datestart.SelectedDate = Convert.ToDateTime(startDate.ToString("yyyy-MM-dd"));
                        cal_datestart.VisibleDate = Convert.ToDateTime(startDate.ToString("yyyy-MM-dd"));
                        f_datestartHIDDEN.Text = startDate.ToString("yyyy-MM-dd"); 
                        cal_dateend.SelectedDate = Convert.ToDateTime(endDate.ToString("yyyy-MM-dd"));
                        cal_dateend.VisibleDate = Convert.ToDateTime(endDate.ToString("yyyy-MM-dd"));
                        f_dateendHIDDEN.Text = endDate.ToString("yyyy-MM-dd");


                        //cal_datestart.SelectedDate = Convert.ToDateTime(thisComp.DateStart.ToString("yyyy-MM-dd"));
                        //cal_datestart.VisibleDate = Convert.ToDateTime(thisComp.DateStart.ToString("yyyy-MM-dd"));
                        //f_datestartHIDDEN.Text = thisComp.DateStart.ToString("yyyy-MM-dd");
                        //cal_dateend.SelectedDate = Convert.ToDateTime(thisComp.DateEnd.ToString("yyyy-MM-dd"));
                        //cal_dateend.VisibleDate = Convert.ToDateTime(thisComp.DateEnd.ToString("yyyy-MM-dd"));
                        //f_dateendHIDDEN.Text = thisComp.DateEnd.ToString("yyyy-MM-dd");

                        f_compname.Text = thisComp.Name;
                        f_urlname.Text = thisComp.UrlName;
                        f_exturl.Text = thisComp.ExternalUrl;
                        f_mobiexturl.Text = thisComp.ExternalMobileUrl;
                        f_prizes.Text = thisComp.Prizes;
                        f_disclaimer.Text = thisComp.Disclaimer;
                        f_response.Text = thisComp.EntryResponse;
                        f_optinmessage.Text = thisComp.OptInMessage;
                        f_marketingmessage.Text = String.IsNullOrEmpty(thisComp.MarketingOptInMessage) ? "I agree that SuperSport and/or the competition sponsor can send me marketing messaging." : thisComp.MarketingOptInMessage;
                        f_videocategoryurl.Text = thisComp.VideoCategoryUrl;
                        PopulateDropDownList();
                        if (thisComp.Sport > 0)
                            ddl_competitionsport.SelectedValue = thisComp.Sport.ToString();

                        lit_webheader.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.HeaderImageWeb +"\" target=\"_blank\">" + thisComp.HeaderImageWeb + "</a>";
                        lit_webfooter.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.FooterImageWeb + "\" target=\"_blank\">" + thisComp.FooterImageWeb + "</a>";
                        lit_LiveVideoStreamingReplacement.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.LiveVideoStreamingReplacement + "\" target=\"_blank\">" + thisComp.LiveVideoStreamingReplacement + "</a>";
                        lit_mobiheader.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.HeaderImageMobi + "\" target=\"_blank\">" + thisComp.HeaderImageMobi + "</a>";
                        lit_mobifooter.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.FooterImageMobi + "\" target=\"_blank\">" + thisComp.FooterImageMobi + "</a>";
                        lit_thumbnail.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.ThumbNail + "\" target=\"_blank\">" + thisComp.ThumbNail + "</a>";
                        lit_mobilethumbnail.Text = "<a href=\"" + thisComp.ImageUrl + thisComp.MobileThumbNail + "\" target=\"_blank\">" + thisComp.MobileThumbNail + "</a>";

                        chk_multipleentries.Checked = thisComp.AllowMultipleEntriesPerDay;
                        chk_requirelogon.Checked = thisComp.RequireLogin;
                        chk_feature.Checked = thisComp.Feature;
                        chk_active.Checked = thisComp.Active;
                        chk_optincheckbox.Checked = thisComp.HasMarketingOptIn;

                        chk_multichoiceoptincheckbox.Checked = thisComp.HasMultichoiceOptIn;
                        chk_richmedia.Checked = thisComp.HasRichMedia;
                        chk_ageverificationrequired.Checked = thisComp.AgeVerificationRequired;


                        litCreateNew.Text = "<a href=\"QuestionDetails.aspx?edit=0&compid="+ thisComp.Id +"\">CREATE</a>";

                        btnDelWebHeader.Visible = true;
                        btnDelWebFooter.Visible = true;
                        btnDelMobiHeader.Visible = true;
                        btnDelMobiFooter.Visible = true;
                        btnDelThumbNail.Visible = true;
                        btnDelMobileThumbnail.Visible = true;

                        lnk_Del_LiveVideoStreamingReplacement.Visible = true;

                        if (thisComp.Questions.Count > 0)
                        {
                            pnlNoQuestions.Visible = false;
                            repQuestionList.DataSource = thisComp.Questions;
                            repQuestionList.DataBind();
                            lnkprint.NavigateUrl = "QuestionPrint.aspx?compid=" + thisComp.Id.ToString();
                        }
                        else
                        {
                            pnlNoQuestions.Visible = true;
                            lnkprint.Visible = false;
                        }
                    }
                }
                else
                {
                    if (String.IsNullOrEmpty(Request.QueryString["id"]) && edit == 0)
                        f_marketingmessage.Text = "I agree that SuperSport and/or the competition sponsor can send me marketing messaging.";
                }
            }
        }

        private void PopulateDropDownList()
        {
            ddl_competitionsport.DataSource = Competition.GetSports();
            ddl_competitionsport.DataTextField = "Key";
            ddl_competitionsport.DataValueField = "Value";
            ddl_competitionsport.DataBind();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            saveRecord(false);
        }

        protected void btn_saveback_Click(object sender, EventArgs e)
        {
            saveRecord(true);
        }

        protected void saveRecord(bool goBackToList)
        {
            Competition newComp = new Competition();

            if (edit == 1)
            {
                newComp = selectedComp;
            }

            //newComp.DateStart = Convert.ToDateTime(f_datestartHIDDEN.Text);
            //newComp.DateEnd = Convert.ToDateTime(f_dateendHIDDEN.Text);

            newComp.DateStart = Convert.ToDateTime(f_datestartHIDDEN.Text).ToString();
            newComp.DateEnd = Convert.ToDateTime(f_dateendHIDDEN.Text).ToString();

            newComp.Name = f_compname.Text;
            newComp.UrlName = f_urlname.Text;
            newComp.ExternalUrl = String.IsNullOrEmpty(f_exturl.Text) ? string.Empty : f_exturl.Text;
            newComp.ExternalMobileUrl = String.IsNullOrEmpty(f_mobiexturl.Text) ? string.Empty : f_mobiexturl.Text;
            newComp.Active = chk_active.Checked;
            newComp.HasMarketingOptIn = chk_optincheckbox.Checked;
            newComp.Prizes = f_prizes.Text;
            newComp.Disclaimer = f_disclaimer.Text;
            newComp.EntryResponse = f_response.Text;
            newComp.OptInMessage = f_optinmessage.Text;
            newComp.MarketingOptInMessage = chk_optincheckbox.Checked ? f_marketingmessage.Text : string.Empty;
            newComp.VideoCategoryUrl = String.IsNullOrEmpty(f_videocategoryurl.Text) ? string.Empty : f_videocategoryurl.Text;
            newComp.Sport = Convert.ToInt32(ddl_competitionsport.SelectedValue);

            newComp.HasMultichoiceOptIn = chk_multichoiceoptincheckbox.Checked;
            newComp.HasRichMedia = chk_richmedia.Checked;
            newComp.AgeVerificationRequired = chk_ageverificationrequired.Checked;

            if (upl_webheader.HasFile)
            {
                newComp.HeaderImageWeb = "SSCOMP_" + newComp.Id.ToString() + "_HEADER_WEB_" + upl_webheader.FileName;
                uploadGraphicFiles(upl_webheader, newComp.HeaderImageWeb);
            }

            if (upl_webfooter.HasFile)
            {
                newComp.FooterImageWeb = "SSCOMP_" + newComp.Id.ToString() + "_FOOTER_WEB_" + upl_webfooter.FileName;
                uploadGraphicFiles(upl_webfooter, newComp.FooterImageWeb);
            }

            if (FileUpload_lnk_LiveVideoStreamingReplacement.HasFile)
            {
                newComp.LiveVideoStreamingReplacement = "SSCOMP_" + newComp.Id.ToString() + "_LIVE_STREAMING_REPLACEMENT_WEB_" + FileUpload_lnk_LiveVideoStreamingReplacement.FileName;
                uploadGraphicFiles(FileUpload_lnk_LiveVideoStreamingReplacement, newComp.LiveVideoStreamingReplacement);
            }

            if (upl_mobiheader.HasFile)
            {
                newComp.HeaderImageMobi = "SSCOMP_" + newComp.Id.ToString() + "_HEADER_MOBI_" + upl_mobiheader.FileName;
                uploadGraphicFiles(upl_mobiheader, newComp.HeaderImageMobi);
            }

            if (upl_mobifooter.HasFile)
            {
                newComp.FooterImageMobi = "SSCOMP_" + newComp.Id.ToString() + "_FOOTER_MOBI_" + upl_mobifooter.FileName;
                uploadGraphicFiles(upl_mobifooter, newComp.FooterImageMobi);
            }

            if (upl_thumbnail.HasFile)
            {
                newComp.ThumbNail = "SSCOMP_" + newComp.Id.ToString() + "_THUMBNAIL_" + upl_thumbnail.FileName;
                uploadGraphicFiles(upl_thumbnail, newComp.ThumbNail);
            }
            if (upl_mobilethumbnail.HasFile)
            {
                newComp.MobileThumbNail = "SSCOMP_" + newComp.Id.ToString() + "_MOBILETHUMBNAIL_" + upl_mobilethumbnail.FileName;
                uploadGraphicFiles(upl_mobilethumbnail, newComp.MobileThumbNail);
            }
            newComp.RequireLogin = chk_requirelogon.Checked;
            newComp.AllowMultipleEntriesPerDay = chk_multipleentries.Checked;
            newComp.Feature = chk_feature.Checked;

            if (newComp.OpenToCountries != null)
            {
                newComp.OpenToCountries.Clear();
            }
            else
            {
                newComp.OpenToCountries = new List<Country>();
            }

            foreach (ListItem li in chk_countries.Items)
            {
                if (li.Selected)
                {
                    Country newCountry = new Country();

                    newCountry.CountryCode = li.Value;

                    newComp.OpenToCountries.Add(newCountry);
                }
            }

            try
            {
                newComp.Save();

                if (goBackToList == true)
                {
                    Response.Redirect("Competitions.aspx");
                }
                else
                {
                    Response.Redirect("CompetitionDetails.aspx?edit=1&id=" + newComp.Id);
                }
            }
            catch (ValidationException ex)
            {
                litException.Text = "";

                foreach (ValidationMessage item in ex.ValidationList)
                {
                    litException.Text += item.ErrorMessage + "<br />";
                }
                panException.Visible = true;
            }
        }

        protected void uploadGraphicFiles(FileUpload uplControl, string fName)
        {
            String ulDir = "";
            if (uplControl.ID == "FileUpload_lnk_LiveVideoStreamingReplacement")
                ulDir = MapPath("~/external/Graphics");
            else
                ulDir = MapPath("compGraphics");

            ulDir = ulDir.Replace("\\", "\\") + "\\";

            String selectedfile;

            String ext = "";

            bool err;
            err = false;

            if (uplControl.HasFile)
            {
                selectedfile = fName;// uplControl.FileName;

                try
                {
                    uplControl.PostedFile.SaveAs(ulDir + selectedfile);
                }
                catch (Exception ex)
                {
                    err = true;
                }
            }
        }

        protected void f_datestart_SelectionChanged(object sender, EventArgs e)
        {
            f_datestartHIDDEN.Text = cal_datestart.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void f_dateend_SelectionChanged(object sender, EventArgs e)
        {
            f_dateendHIDDEN.Text = cal_dateend.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void buildCountryList()
        {
            List<Country> countryList = Country.GetCountryList();

            if (countryList != null)
            {
                chk_countries.DataSource = countryList;
                chk_countries.DataValueField = "CountryCode";
                chk_countries.DataTextField = "CountryName";
                chk_countries.DataBind();
            }

            if ((edit == 1) && (selectedComp.OpenToCountries != null))
            {
                foreach (Country item in selectedComp.OpenToCountries)
                {
                    if (chk_countries.Items.FindByValue(item.CountryCode) != null)
                    {
                        chk_countries.Items.FindByValue(item.CountryCode).Selected = true;
                    }
                }
            }
        }

        protected void btn_deselect_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in chk_countries.Items)
            {
                li.Selected = false;
            }
        }

        protected void btn_subsaharan_Click(object sender, EventArgs e)
        {
            List<Country> countryList = Country.GetCountryList();

            foreach (ListItem li in chk_countries.Items)
            {
                li.Selected = false;
                Country c = countryList.Find(tmpCountry => { return tmpCountry.CountryCode == li.Value; });
                if (c.IsSubSaharaCountry)
                {
                    li.Selected = true;
                }
            }
        }

        protected void btn_wholeworld_Click(object sender, EventArgs e)
        {
            foreach (ListItem li in chk_countries.Items)
            {
                li.Selected = true;
            }
        }

        protected void repQuestionList_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                SuperSport.CompetitionLib.Models.Question item = (SuperSport.CompetitionLib.Models.Question)e.Item.DataItem;

                HtmlGenericControl wrapList = new HtmlGenericControl("div");

                StringBuilder outStr = new StringBuilder();

                string rowCol = "txt_black_10";

                if (item.Active == false)
                {
                    rowCol = "txt_red_10";
                }

                outStr.Append("<tr style=\"background-color:#FFFFFF;\" id=\"row_" + item.Id + "\" onmouseover=\"ChangeRowColour(this.id,'#F2F2F2')\" onmouseout=\"ChangeRowColour(this.id,'#FFFFFF')\">");

                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\">");
                outStr.Append("        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                outStr.Append("	            <tr>");
                outStr.Append("		            <td class=\"txt_black_10\" style=\"padding:0 3px 0 0\"><a href=\"QuestionDetails.aspx?edit=1&id=" + item.Id + "&compid="+ id +"\">OPEN / EDIT</a></td>");
                //outStr.Append("		        <td class=\"txt_red_10\" style=\"padding:0 0 0 3px\"><a href=\"xxx\">DELETE</a></td>");
                outStr.Append("	            </tr>");
                outStr.Append("        </table>");
                outStr.Append("    </td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.QuestionText + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.QuestionFormat + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateStart.ToString("dd MMMM yyyy") + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateEnd.ToString("dd MMMM yyyy") + "</td>");
                //outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\""+ rowCol +"\">Modified XXXX</td>");
                outStr.Append("</tr>");

                wrapList.InnerHtml = outStr.ToString();

                e.Item.Controls.Add(wrapList);
            }
        }

        protected void btnDelWebHeader_Click(object sender, EventArgs e)
        {
            selectedComp.HeaderImageWeb = string.Empty;
            selectedComp.Save();
            lit_webheader.Text = string.Empty;
        }

        protected void btnDelWebFooter_Click(object sender, EventArgs e)
        {
            selectedComp.FooterImageWeb = string.Empty;
            selectedComp.Save();
            lit_webfooter.Text = string.Empty;
        }

        protected void btnDelMobiHeader_Click(object sender, EventArgs e)
        {
            selectedComp.HeaderImageMobi = string.Empty;
            selectedComp.Save();
            lit_mobiheader.Text = string.Empty;
        }

        protected void btnDelMobiFooter_Click(object sender, EventArgs e)
        {
            selectedComp.FooterImageMobi = string.Empty;
            selectedComp.Save();
            lit_mobifooter.Text = string.Empty;
        }

        protected void btnThumbNail_Click(object sender, EventArgs e)
        {
            selectedComp.ThumbNail = string.Empty;
            selectedComp.Save();
            lit_thumbnail.Text = string.Empty;
        }

        protected void lnk_LiveVideoStreamingReplacement_Click(object sender, EventArgs e)
        {
            selectedComp.LiveVideoStreamingReplacement = string.Empty;
            selectedComp.Save();
            lit_LiveVideoStreamingReplacement.Text = string.Empty;
        }


        protected void btWinners_Click(object sender, EventArgs e)
        {
            
            Response.Clear();

            string name = "Competition_" + f_datestartHIDDEN.Text + "_To_" + f_dateendHIDDEN.Text + ".xlsx";
            DataTable dt = Competition.GetOptInWinners(id, Convert.ToDateTime(f_datestartHIDDEN.Text + " " + "0:0"), Convert.ToDateTime(f_dateendHIDDEN.Text + " " + "0:0"));

            MemoryStream ms = DataTableToExcelXlsx(dt, name);
            ms.WriteTo(Response.OutputStream);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + name);
            Response.StatusCode = 200;
            Response.Flush();
            Response.End();

            

        }


        public static MemoryStream DataTableToExcelXlsx(DataTable table, string sheetName)
        {
            MemoryStream Result = new MemoryStream();
            ExcelPackage pack = new ExcelPackage();
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(sheetName);

            ws.Cells[1, 1].Value = "Name";
            ws.Cells[1, 2].Value = "Surname";
            ws.Cells[1, 3].Value = "ContactNumber";
            ws.Cells[1, 4].Value = "IdNumber";
            ws.Cells[1, 5].Value = "Email";
            ws.Cells[1, 6].Value = "CountryCode";
            ws.Cells[1, 7].Value = "City";
            ws.Cells[1, 8].Value = "Answer";
            ws.Cells[1, 9].Value = "ConnectId";
            ws.Cells[1, 10].Value = "DateEntered";

            int col = 1;
            int row = 2;
            foreach (DataRow rw in table.Rows)
            {
                foreach (DataColumn cl in table.Columns)
                {  
                    if (rw[cl.ColumnName] != DBNull.Value)
                        ws.Cells[row, col].Value = rw[cl.ColumnName].ToString();
                    col++;
                }
                row++;
                col = 1;
            }

            pack.SaveAs(Result);
            return Result;
        }

        protected void btmultichoiceWinners_Click(object sender, EventArgs e)
        {
            
            Response.Clear();

            string name = "Multichoice_" + f_datestartHIDDEN.Text + "_To_" + f_dateendHIDDEN.Text + ".xlsx";
                        
            DataTable dt = Competition.GetMultichoiceOptInWinners(id, Convert.ToDateTime(f_datestartHIDDEN.Text + " " + "0:0"), Convert.ToDateTime(f_dateendHIDDEN.Text + " " + "0:0"));

            MemoryStream ms = DataTableToExcelXlsx(dt, name);
            ms.WriteTo(Response.OutputStream);
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + name);
            Response.StatusCode = 200;
            Response.Flush();
            Response.End();

        }

        protected void btnDelMobileThumbnail_Click(object sender, EventArgs e)
        {
            selectedComp.MobileThumbNail = string.Empty;
            selectedComp.Save();
            lit_mobilethumbnail.Text = string.Empty;
        }

     
    }
}