﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class _master : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        if (Request.Cookies.Get(ConfigurationManager.AppSettings["PrimaryCookie"]) == null)
        {
            Response.Redirect("default.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lit_fullname.Text = Request.Cookies[ConfigurationManager.AppSettings["PrimaryCookie"]]["name"] + " " + Request.Cookies[ConfigurationManager.AppSettings["PrimaryCookie"]]["surname"];
        lit_date.Text = DateTime.Now.ToString("MMMM dd, yyyy");

        buildMainMenu();
    }

    protected void buildMainMenu()
    {
        StringBuilder outStr = new StringBuilder();

        outStr.Append("     <ul class=\"PCM PCMm0\">");

        outStr.Append("         <li class=\"PCMi0\"><a class=\"PCMi0\" href=\"competitions.aspx\">COMPETITIONS</a></li>");

        outStr.Append("         <li class=\"PCMi0\"><a class=\"PCMi0\" href=\"#\">SYSTEM ADMIN</a>");
        outStr.Append("             <ul class=\"PCMm\">");
        outStr.Append("                 <li class=\"PCMi\"><a href=\"users.aspx\">NAMES AND PERMISSIONS</a></li>");
        //outStr.Append("                 <li class=\"PCMi\"><a href=\"areas.aspx\">AREAS / SUITES</a></li>");
        outStr.Append("             </ul>");
        outStr.Append("         </li>");

        outStr.Append("     </ul>");

        lit_menu.Text = outStr.ToString();
    }
}
