﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Collections.Generic;

public class ext
{
    public class time
    {
        public String timeval { get; set; }

        public time()
        {
        }
    }

    public class time_list
    {
        public time_list()
        {
        }

        public List<time> getTimeList(int interval)
        {
            List<time> timeList = new List<time>();

            for (DateTime tval = Convert.ToDateTime("00:00"); tval <= Convert.ToDateTime("23:59"); tval = tval.AddMinutes(interval))
            {
                time item = new time();

                item.timeval = tval.ToString("HH:mm");

                timeList.Add(item);
            }

            return timeList;
        }
    }

    public static void stop(String testString)
    {
        HttpContext.Current.Response.Write(testString);
        HttpContext.Current.Response.End();
    }

    public static void write(String testString)
    {
        HttpContext.Current.Response.Write(testString);
    }

    public static void forceFileDownload(String filePath, String fileName)
    {
        FileStream fStream = new FileStream(@filePath + fileName, FileMode.Open, FileAccess.Read);
        byte[] byteBuffer = new byte[(int)fStream.Length];
        fStream.Read(byteBuffer, 0, (int)fStream.Length);
        fStream.Close();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = "application/octet-stream";
        HttpContext.Current.Response.AddHeader("Content-Length", byteBuffer.Length.ToString());
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
        HttpContext.Current.Response.BinaryWrite(byteBuffer);
    }

    public static void OpenPopUp(System.Web.UI.WebControls.WebControl opener, String pageUrl)
    {
        String clientScript = "";
        clientScript = "window.open('" + pageUrl + "')";

        opener.Attributes.Add("onClick", clientScript);
    }

    public static String buildFrame(int part, int width, int frameSize, string color)
    {
        String retStr = "";

        switch (part)
        {
            case 0:
                retStr += "<table width=\"" + width + "\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
                retStr += "    <tr>";
                retStr += "	    <td width=\"" + frameSize + "\" height=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_top_left_" + frameSize + ".png) top left no-repeat;\"></td>";
                retStr += "	    <td style=\"background:url(External/Graphics/fade_top_" + frameSize + ".png) top left repeat-x;\"></td>";
                retStr += "	    <td width=\"" + frameSize + "\" height=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_top_right_" + frameSize + ".png) top left no-repeat;\"></td>";
                retStr += "    </tr>";
                retStr += "    <tr>";
                retStr += "	    <td width=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_left_" + frameSize + ".png) top left repeat-y;\"></td>";
                retStr += "	    <td style=\"padding:5px;\" bgcolor=\"" + color + "\">";
                break;

            case 1:
                retStr += "	    </td>";
                retStr += "	    <td width=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_right_" + frameSize + ".png) top left repeat-y;\"></td>";
                retStr += "    </tr>";
                retStr += "    <tr>";
                retStr += "	    <td width=\"" + frameSize + "\" height=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_bottom_left_" + frameSize + ".png) top left no-repeat;\"></td>";
                retStr += "	    <td style=\"background:url(External/Graphics/fade_bottom_" + frameSize + ".png) top left repeat-x;\"></td>";
                retStr += "	    <td width=\"" + frameSize + "\" height=\"" + frameSize + "\" style=\"background:url(External/Graphics/fade_bottom_right_" + frameSize + ".png) top left no-repeat;\"></td>";
                retStr += "    </tr>";
                retStr += "</table>";
                break;
        }

        return retStr;
    }

    public static void buildYears(DropDownList dropField)
    {
        int startYear = DateTime.Now.AddMonths(-1).Year;

        for (int i = 0; i < 3; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + (startYear + i).ToString() + "", "" + (startYear + i).ToString() + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static void buildMonths(DropDownList dropField)
    {
        for (int i = 0; i < 12; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + Convert.ToDateTime(("1900-" + (i + 1).ToString() + "-01")).ToString("MMMM") + "", "" + (i + 1).ToString().PadLeft(2, '0') + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static void buildDays(DropDownList dropField)
    {
        for (int i = 0; i < 31; i++)
        {
            dropField.Items.Insert(i, new ListItem("" + (i + 1).ToString().PadLeft(2, '0') + "", "" + (i + 1).ToString().PadLeft(2, '0') + ""));
        }

        dropField.Items.Insert(0, new ListItem("", ""));
    }

    public static String buildPageHeading(String heading)
    {
        String retStr = "";

        retStr += heading;

        return retStr;
    }

    public static String buildBackCancel(String text, String url, bool isButton)
    {
        String backMsg = "";

        if (isButton)
        {
            if (url == "")
            {
                backMsg = "<input name=\"btn_cancel\" type=\"button\" class=\"btn_grey\" id=\"btn_back\" value=\"" + text + "\" onclick=\"history.back()\" />";
            }
            else
            {
                backMsg = "<input name=\"btn_cancel\" type=\"button\" class=\"btn_grey\" id=\"btn_back\" value=\"" + text + "\" onclick=\"gotoURL('" + url + "')\" />";
            }
        }
        else
        {
            if (url == "")
            {
                backMsg += "<div class=\"txt_red_10\" style=\"margin-bottom:10px;\"><a href=\"javascript:void(0);\" onclick=\"history.back()\";>" + text + "</a></div>";
            }
            else
            {
                backMsg += "<div class=\"txt_red_10\" style=\"margin-bottom:10px;\"><a href=\"" + url + "\">" + text + "</a></div>";
            }
        }

        return backMsg;
    }

    public static String getGuid()
    {
        string guidResult = System.Guid.NewGuid().ToString();
        return guidResult;
    }

    public static bool isNumericType(String testValue, String testType)
    {
        bool returnVal = false;

        switch (testType)
        {
            case "int":
                int out_INT;

                if (int.TryParse(testValue, out out_INT))
                {
                    returnVal = true;
                }
                break;

            case "double":
                double out_DOUBLE;

                if (double.TryParse(testValue, out out_DOUBLE))
                {
                    returnVal = true;
                }
                break;
        }

        return returnVal;
    }

    public static bool isDate(String testValue)
    {
        bool returnVal = false;

        DateTime out_DATE;

        if (DateTime.TryParse(testValue, out out_DATE))
        {
            returnVal = true;
        }

        return returnVal;
    }

    public static string GetCurrentPageName(String qString, bool useCurrentQuerystring)
    {
        String sRet;
        String sPath = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
        System.IO.FileInfo oInfo = new System.IO.FileInfo(sPath);
        sRet = oInfo.Name;

        if (useCurrentQuerystring)
        {
            sRet += qString;
        }

        return sRet;
    }

    public static String makeSpace(int height, String bgCol, bool clear)
    {
        String spaceDiv = "";
        String clearVal = "none";
        if (clear == true)
        {
            clearVal = "both";
        }
        spaceDiv += "<div style=\"height:" + height + "px; background-color:" + bgCol + "; clear:" + clearVal + ";\"></div>";
        return spaceDiv;
    }

    public static String clearBoth()
    {
        String clearDiv = "";
        clearDiv += "<div style=\"clear:both;\"></div>";
        return clearDiv;
    }

    public static String buildAlphabetIndex(String curPage, String align)
    {
        String indexTable = "";
        String asciiChar;
        indexTable += " <table align=\"" + align + "\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" style=\"margin:0 auto;\">" + Environment.NewLine;
        indexTable += "     <tr class=\"txt_white_10\">" + Environment.NewLine;

        for (int i = 0; i < 26; i++)
        {
            asciiChar = Convert.ToChar(i + 65).ToString();

            indexTable += "         <td class=\"ALPHABETITEM\" onclick=\"gotoURL('" + curPage + "?sel=" + asciiChar + "')\"><strong>" + asciiChar + "</strong></td>" + Environment.NewLine;
        }

        indexTable += "         <td class=\"ALPHABETITEM\" onclick=\"gotoURL('" + curPage + "?sel=0-9')\"><strong>Other</strong></td>" + Environment.NewLine;

        indexTable += "     </tr>" + Environment.NewLine;
        indexTable += " </table>" + Environment.NewLine;

        return indexTable;
    }
}