﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool.test
{
    public partial class createCompTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //List<Competition> cList = Competition.GetCompetitions("", false);
            //createComp();
            Competition c = Competition.GetCompetition(3);
            int x = 0;
        }        

        public void createComp()
        {
            Competition comp = new Competition();
            comp.Name = "test";
            comp.DateStart = Convert.ToDateTime("2011/04/20").ToString();
            comp.DateEnd = Convert.ToDateTime("2011/04/20").ToString();
            comp.UrlName = "testurl";
            //comp.HeaderImageWeb = "";
            comp.FooterImageWeb = "";
            comp.FooterImageMobi = "";
            comp.HeaderImageMobi = "";

            List<Country> countries = new List<Country>();
            Country c = new Country();
            c.CountryCode = "ZA";
            c.CountryName = "South Africa";
            c.Id = 1;
            countries.Add(c);
            comp.OpenToCountries = countries;

            //question
            List<Question> qList = new List<Question>();
            Question q = new Question();
            q.DateStart = Convert.ToDateTime("2011/04/20");
            q.DateEnd = Convert.ToDateTime("2011/04/20");
            q.QuestionText = "My question is...";
            q.QuestionFormat = Question.QuestionType.SingleSelection;
            q.CompetitionId = comp.Id;
            //q.Save();

            //answer
            List<Answer> alist = new List<Answer>();
            Answer a = new Answer();
            a.AnswerText = "answer1";
            a.QuestionId = q.Id;
            alist.Add(a);
            q.Answers = alist;

            qList.Add(q);
            comp.Questions = qList;

            //a.Save();

            comp.Save();
            Response.Write(comp.Id);
        }
    }
}