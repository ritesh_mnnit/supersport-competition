﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool
{
    public partial class WinnerList : System.Web.UI.Page
    {
        public int compId;
        public string competitionName = string.Empty;

        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["compId"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compId"]);
            }

            comp = Competition.GetCompetition(compId);

            if (!Page.IsPostBack)
            {
                buildTimeDrop(drpStartTime,15);
                buildTimeDrop(drpEndTime, 15);
                renderPreviousWinners();
                loadWinnerTotal();
            }
        }

        private void renderPreviousWinners()
        {
            if ((comp != null) && (comp.CompetitionWinners != null))
            {
                competitionName = comp.Name;
                rpCompetitionWinners.DataSource = comp.CompetitionWinners;
                rpCompetitionWinners.DataBind();

                if (comp.CompetitionWinners.Count <= 0)
                {
                    lnkprint.Visible = false;
                }
                else
                {
                    lnkprint.NavigateUrl = "WinnerListPrint.aspx?compid=" + comp.Id.ToString();
                }
            }
        }

        private void loadWinnerTotal()
        {
            int i = 1;
            do
            {
                drpAmount.Items.Add(i.ToString());
                i++;
            }
            while (i < 11);
        }
        
        protected void rpCompetitionWinners_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                CompetitionWinner item = (CompetitionWinner)e.Item.DataItem;

                ((Label)e.Item.FindControl("lbLink")).Text = "<a href=\"WinnerUpdate.aspx?id="+ item.Id +"&compid="+ item.CompetitionId +"\">OPEN / EDIT</a>";
                ((Label)e.Item.FindControl("lbName")).Text = item.EntryDetails.Name;
                ((Label)e.Item.FindControl("lbSurname")).Text = item.EntryDetails.Surname;
                ((Label)e.Item.FindControl("lbContactNumber")).Text = item.EntryDetails.ContactNumber;
                ((Label)e.Item.FindControl("lbIdNumber")).Text = item.EntryDetails.IdNumber;
                ((Label)e.Item.FindControl("lbEmail")).Text = item.EntryDetails.Email;
                ((Label)e.Item.FindControl("lbConnectId")).Text = item.EntryDetails.ConnectId;
                ((Label)e.Item.FindControl("lbCountry")).Text = item.EntryDetails.CountryCode;
                ((Label)e.Item.FindControl("lblCity")).Text = item.EntryDetails.City;
                ((Label)e.Item.FindControl("lbAnswer")).Text = item.EntryDetails.Answer;
                ((Label)e.Item.FindControl("lbDateDrawn")).Text = item.CreatedDate.ToString("dd MMM yyyy HH:mm");
                ((Label)e.Item.FindControl("lbDateEntered")).Text = item.EntryDetails.DateEntered.ToString("dd MMM yyyy HH:mm");
                ((Label)e.Item.FindControl("lbComment")).Text = item.Comment;
                ((CheckBox)e.Item.FindControl("chkConfirmedWinner")).Checked = item.ClaimedWinner;
            }
        }

        protected void btDrawWinners_Click(object sender, EventArgs e)
        {
            lbDrawResponse.Visible = false;
            renderPreviousWinners();
            List<CompetitionWinner> compWinnersDraw = comp.DrawWinners(Convert.ToInt32(drpAmount.SelectedValue), Convert.ToDateTime(txtDateStartHIDDEN.Text + " " + drpStartTime.SelectedValue), Convert.ToDateTime(txtDateEndHIDDEN.Text + " " + drpEndTime.SelectedValue), tbAnswer.Text, chkOptInRequired.Checked);
            if (compWinnersDraw != null)
            {
                rpCompetitionWinnersDraw.DataSource = compWinnersDraw;
                rpCompetitionWinnersDraw.DataBind();
            }
            if ((compWinnersDraw == null) || ((compWinnersDraw != null) && (compWinnersDraw.Count <= 0)))
            {
                lbDrawResponse.Visible = true;
            }
        }

        protected void tbStartDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateStartHIDDEN, tbStartDate);
        }

        protected void tbEndDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateEndHIDDEN, tbEndDate);
        }

        protected void changeDateVal(TextBox hiddenDate, Calendar calDate)
        {
            hiddenDate.Text = calDate.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void buildTimeDrop(DropDownList dropList, int interval)
        {
            ext.time_list list = new ext.time_list();
            List<ext.time> timeList = new List<ext.time>();

            timeList = list.getTimeList(interval);

            dropList.DataSource = timeList;
            dropList.DataValueField = "timeval";
            dropList.DataTextField = "timeval";

            dropList.DataBind();
        }
    }
}