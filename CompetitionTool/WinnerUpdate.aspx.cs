﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using SuperSport.CompetitionLib.Models;
using SuperSport.CompetitionLib.Exceptions;

namespace CompetitionTool
{
    public partial class WinnerUpdate : System.Web.UI.Page
    {
        public int id;
        public int compId;

        private CompetitionWinner competitionWinner;
        private Competition competition;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["id"], "int"))
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (ext.isNumericType(Request.QueryString["compid"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compid"]);
            }

            competition = Competition.GetCompetition(compId);

            List<CompetitionWinner> compWinners = CompetitionWinner.GetWinners(compId);
            competitionWinner = compWinners.Find(a => { return a.Id == id; });

            if (!Page.IsPostBack)
            {
                if (competitionWinner != null)
                {
                    chk_confirm.Checked = competitionWinner.ClaimedWinner;
                    f_comment.Text = competitionWinner.Comment;
                }
            }
        }

        //protected void btn_save_Click(object sender, EventArgs e)
        //{
        //    saveRecord(false);
        //}

        protected void btn_saveback_Click(object sender, EventArgs e)
        {
            saveRecord(true);
        }

        protected void saveRecord(bool goBackToList)
        {
            try 
            {
                competitionWinner.UpdateWinner(id, chk_confirm.Checked, f_comment.Text);

                if (goBackToList == true)
                {
                    Response.Redirect("WinnerList.aspx?compid=" + compId);
                }
                else
                {
                    Response.Redirect("WinnerUpdate.aspx?id=" + competitionWinner.Id + "&compid=" + compId);
                }
            }
            catch(ValidationException ex)
            {
                litException.Text = "";

                foreach(ValidationMessage item in ex.ValidationList)
                {
                    litException.Text += item.ErrorMessage + "<br />";
                }

                panException.Visible = true;
            }
        }
    }
}