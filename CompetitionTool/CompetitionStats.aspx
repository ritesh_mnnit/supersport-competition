﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CompetitionStats.aspx.cs" Inherits="CompetitionTool.CompetitionStats" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp_pageheading" runat="server">

    <%= ext.buildPageHeading("COMPETITION ENTRIES - " + competitionName)%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cp_pagedata" runat="server">

<asp:ScriptManager ID="sm_entrylist" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">

        <%=ext.buildBackCancel("BACK TO COMPETITION","CompetitionDetails.aspx?edit=1&id=" + compId, true) %>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <div>   
        <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>STATS DETAILS (Leave blank to calculated from start of competition till now)</strong></div>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATES&nbsp;</td>
                <td colspan="3" align="left" valign="top" bgcolor="#FFFFFF">

                    <table>
                        <tr>
                            <td width="200">
                                <asp:UpdatePanel ID="up_datestart" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datestart" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateStartHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>FROM DATE</strong></div>
                                            <asp:Calendar ID="tbStartDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbStartDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpStartTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                            <td width="1"></td>
                            <td width="200">
                                <asp:UpdatePanel ID="up_dateend" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datesend" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateEndHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>TO DATE</strong></div>
                                            <asp:Calendar ID="tbEndDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbEndDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpEndTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="txt_black_12" style="padding: 15px 0 5px 0;">Optionally select the countries you want stats for from the list</div>
                            <div id="div_countries" style="border: #CCCCCC solid 1px; left:5; top:7px; width:199; height:145px; overflow-x:hidden; overflow-y: scroll;">
                                <asp:CheckBoxList ID="chk_countries" runat="server" CssClass="txt_black_10"></asp:CheckBoxList></td>
                            </div>
                        </tr>
                    </table>
                
                </td>
            </tr>
        </table>
        <%=ext.makeSpace(10,"",true) %>
        <asp:button runat="server" ID="btnGetStats" CssClass="btn_grey" Text="GET STATS" onclick="btnGetStats_Click"  />
    </div>

    <%=ext.makeSpace(20,"",true) %>

        <asp:Panel ID="pnlStatsResults" runat="server" Visible="false">
            <asp:PlaceHolder ID="plcStats" runat="server" EnableViewState="false" />
                <asp:Repeater ID="rtp1_stats" runat="server" OnItemDataBound="rtp1_stats_dataBound">
                    <ItemTemplate></ItemTemplate>
                </asp:Repeater>
            </asp:Panel>
</asp:Content>
