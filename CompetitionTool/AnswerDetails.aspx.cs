﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using SuperSport.CompetitionLib.Models;
using SuperSport.CompetitionLib.Exceptions;

namespace CompetitionTool
{
    public partial class AnswerDetails : System.Web.UI.Page
    {
        public int id;
        public int quesId;
        public int compId;
        public int edit;
        public string question = string.Empty;

        private Answer selectedAnswer;
        private Question compQuestion;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["id"], "int"))
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (ext.isNumericType(Request.QueryString["quesid"], "int"))
            {
                quesId = Convert.ToInt32(Request.QueryString["quesid"]);                
            }

            if (ext.isNumericType(Request.QueryString["compid"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compid"]);
            }

            if (ext.isNumericType(Request.QueryString["edit"], "int"))
            {
                edit = Convert.ToInt32(Request.QueryString["edit"]);
            }

            compQuestion = Question.GetQuestions(compId).Find(q => { return q.Id == quesId; });
            if (compQuestion != null)
            {
                question = compQuestion.QuestionText;
            }

            if (edit == 1)
            {
                //List<Question> compQuestions = Question.GetQuestions(compId);
                //selectedQuestion = compQuestions.Find(q => { return q.Id == id; });

                List<Answer> quesAnswers = Answer.GetAnswers(quesId);
                selectedAnswer = quesAnswers.Find(a => { return a.Id == id; });
            }

            if (!Page.IsPostBack)
            {
                if (edit == 1)
                {
                    //List<Question> compQuestions = Question.GetQuestions(compId);
                    //Question thisQuestion = compQuestions.Find(q => { return q.Id == id; });

                    List<Answer> quesAnswers = Answer.GetAnswers(quesId);
                    Answer thisAnswer = quesAnswers.Find(q => { return q.Id == id; });

                    if (thisAnswer != null)
                    {
                        txExtendedText.Text = thisAnswer.ExtendedText;
                        f_answertext.Text = thisAnswer.AnswerText;
                        chk_active.Checked = thisAnswer.Active;
                        chk_correct.Checked = thisAnswer.isCorrect;
                    }
                }
            }
        }

        //protected void btn_save_Click(object sender, EventArgs e)
        //{
        //    saveRecord(false);
        //}

        protected void btn_saveback_Click(object sender, EventArgs e)
        {
            saveRecord(true);
        }

        protected void saveRecord(bool goBackToList)
        {
            Answer newAnswer = new Answer();

            if (edit == 1)
            {
                newAnswer = selectedAnswer;
            }

            newAnswer.QuestionId = quesId;

            newAnswer.ExtendedText = txExtendedText.Text;
            newAnswer.AnswerText = f_answertext.Text;
            newAnswer.Active = chk_active.Checked;
            newAnswer.isCorrect = chk_correct.Checked;

            try 
            {
                newAnswer.Save();

                if (goBackToList == true)
                {
                    Response.Redirect("QuestionDetails.aspx?edit=1&id=" + quesId + "&compid=" + compId);
                }
                else
                {
                    Response.Redirect("AnswerDetails.aspx?edit=1&id=" + newAnswer.Id + "&quesid=" + quesId + "&compid=" + compId);
                }
            }
            catch(ValidationException ex)
            {
                litException.Text = "";

                foreach(ValidationMessage item in ex.ValidationList)
                {
                    litException.Text += item.ErrorMessage + "<br />";
                }
                panException.Visible = true;
            }
        }
    }
}