﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;
using System.Web.UI.HtmlControls;
using System.Text;

namespace CompetitionTool
{
    public partial class CompetitionStats : System.Web.UI.Page
    {
        public int compId;
        public string competitionName = string.Empty;
        public HtmlGenericControl curDiv;
        public bool countrySpecific = true;
        public int statsCount = 0;

        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["compId"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compId"]);
            }
           
            comp = Competition.GetCompetition(compId);
            if (comp != null)
            {
                competitionName = comp.Name;
            }

            if (!Page.IsPostBack)
            {
                if (comp != null)
                {
                    var BreakDownOfAllCountries = new Country();
                    BreakDownOfAllCountries.CountryCode = "00";
                    BreakDownOfAllCountries.CountryName = "Breakdown of ALL countries";
                    comp.OpenToCountries.Insert(0, BreakDownOfAllCountries);
                    chk_countries.DataSource = comp.OpenToCountries;
                    chk_countries.DataValueField = "CountryCode";
                    chk_countries.DataTextField = "CountryName";
                    chk_countries.DataBind();
                }
                buildTimeDrop(drpStartTime, 15);
                buildTimeDrop(drpEndTime, 15);
            }
        }

        protected void btnGetStats_Click(object sender, EventArgs e)
        {
            pnlStatsResults.Visible = true;

            DateTime fromDate;
            DateTime toDate;

            if (!string.IsNullOrEmpty(txtDateStartHIDDEN.Text))
            {
                fromDate = Convert.ToDateTime(txtDateStartHIDDEN.Text + " " + drpStartTime.SelectedValue);
            }
            else
            {
                // fromDate = comp.DateStart;
                fromDate = Convert.ToDateTime(comp.DateStart);
            }

            if (!string.IsNullOrEmpty(txtDateEndHIDDEN.Text))
            {
                toDate = Convert.ToDateTime(txtDateEndHIDDEN.Text + " " + drpEndTime.SelectedValue);
            }
            else
            {
                toDate = DateTime.Now;
            }

            //SuperSport.CompetitionLib.Models.CompetitionStats stats = comp.GetStats(fromDate, toDate, "");

            List<SuperSport.CompetitionLib.Models.CompetitionStats> stats = new List<SuperSport.CompetitionLib.Models.CompetitionStats>();

            if (chk_countries.SelectedValue == "00")
            {
                foreach (ListItem item in chk_countries.Items)
                {
                    if (item.Value != "00")
                    {
                        SuperSport.CompetitionLib.Models.CompetitionStats stat = comp.GetStats(fromDate, toDate, item.Value.ToString());
                        stat.countryCode = item.Value.ToString();
                        stat.countryName = item.Text.ToString();
                        stats.Add(stat);
                    }
                }
            }
            else
            {
                foreach (ListItem item in chk_countries.Items)
                {
                    if (item.Selected)
                    {
                        SuperSport.CompetitionLib.Models.CompetitionStats stat = comp.GetStats(fromDate, toDate, item.Value.ToString());
                        stat.countryCode = item.Value.ToString();
                        stat.countryName = item.Text.ToString();
                        stats.Add(stat);
                    }
                }
            }

            if (stats.Count <= 0)
            {
                SuperSport.CompetitionLib.Models.CompetitionStats stat = comp.GetStats(fromDate, toDate, "");
                stats.Add(stat);
                countrySpecific = false;
            }

            int removed = stats.RemoveAll(x => x.TotalEntries < 1);
            stats = stats.OrderByDescending(x => x.TotalEntries).ToList();

            curDiv = new HtmlGenericControl("div");
            curDiv.Attributes.Add("style", "height: 210px;");
            rtp1_stats.DataSource = stats;
            rtp1_stats.DataBind();
            plcStats.Controls.Add(curDiv);
        }

        protected void rtp1_stats_dataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes.Add("class", "txt_white_12");
                if (statsCount % 4 == 0 || statsCount == 0)
                {
                    if (statsCount > 0)
                    {
                        plcStats.Controls.Add(curDiv);
                    }
                    curDiv = new HtmlGenericControl("div");
                    curDiv.Attributes.Add("style", "height: 210px;");
                    if (countrySpecific)
                    {
                        div.Attributes.Add("style", "width: 315px; height: 210px; float: left;");
                    }
                    else
                    {
                        div.Attributes.Add("style", "height: 210px;");
                    }
                }
                else
                {
                    if (countrySpecific)
                    {
                        div.Attributes.Add("style", "width: 315px; height: 210px; float: left; margin: 0 0 0 30px;");
                    }
                    else
                    {
                        div.Attributes.Add("style", "height: 210px; margin: 0 0 0 0px;");
                    }
                }

                SuperSport.CompetitionLib.Models.CompetitionStats item = (SuperSport.CompetitionLib.Models.CompetitionStats)e.Item.DataItem;

                string tdFloat = "center";
                if (!countrySpecific)
                {
                    tdFloat = "left";
                }
                StringBuilder sb = new StringBuilder();
                sb.Append("<div style=\"text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;\"><strong>" + item.countryName + "Stats & Results</strong></div>");
                sb.Append("<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">");
                sb.Append("<tr>");
                if (countrySpecific)
                {
                    sb.Append("<td width=\"80%\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">TOTAL ENTRIES</td>");
                    sb.Append("<td width=\"20%\" align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.TotalEntries.ToString() + "</td>");
                }
                else
                {
                    sb.Append("<td width=\"20%\" align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">TOTAL ENTRIES</td>");
                    sb.Append("<td width=\"80%\" align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.TotalEntries.ToString() + "</td>");
                }
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">UNIQUE ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.UniqueEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">TOTAL WEB ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.WebEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">UNIQUE WEB ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.UniqueWebEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">TOTAL MOBILE ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.MobileEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">UNIQUE MOBILE ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.UniqueMobileEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">TOTAL OPT IN ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.OptInEntries.ToString() + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">UNIQUE OPT IN ENTRIES</td>");
                sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">" + item.UniqueOptInEntries.ToString() + "</td>");
                sb.Append("</tr>");
                if (item.mainCountries != null)
                {
                    sb.Append("<tr>");
                    sb.Append("<td align=\"left\" valign=\"top\" bgcolor=\"#F2F2F2\" class=\"txt_blue_10\">MAIN COUNTRY ENTRIES</td>");
                    sb.Append("<td align=\"" + tdFloat + "\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\"txt_blue_10\">");
                    int count = 0;
                    foreach (Country country in item.mainCountries.OrderByDescending(x => x.Id))
                    {
                        if (count > 0)
                        {
                            sb.Append(", ");
                        }
                        sb.Append(country.CountryName);
                        count += 1;
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
                sb.Append("</table>");
                div.InnerHtml = sb.ToString();

                curDiv.Controls.Add(div);
                statsCount += 1;
            }
        }

        protected void tbStartDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateStartHIDDEN, tbStartDate);
        }

        protected void tbEndDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateEndHIDDEN, tbEndDate);
        }

        protected void changeDateVal(TextBox hiddenDate, Calendar calDate)
        {
            hiddenDate.Text = calDate.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void buildTimeDrop(DropDownList dropList, int interval)
        {
            ext.time_list list = new ext.time_list();
            List<ext.time> timeList = new List<ext.time>();

            timeList = list.getTimeList(interval);

            dropList.DataSource = timeList;
            dropList.DataValueField = "timeval";
            dropList.DataTextField = "timeval";

            dropList.DataBind();
        }
    }
}