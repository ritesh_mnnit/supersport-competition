﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="QuestionDetails.aspx.cs" Inherits="CompetitionTool.QuestionDetails" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" Runat="Server">

    <%= ext.buildPageHeading("QUESTION DETAILS - " + competitionName)%>

</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">

<asp:ScriptManager ID="sm_competitiondetails" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">
        
        <asp:button runat="server" ID="btn_save" CssClass="btn_grey" Text="SAVE DETAILS" onclick="btn_save_Click" />
        <asp:button runat="server" ID="btn_saveback" CssClass="btn_grey" Text="SAVE AND GO BACK TO COMPETITION" onclick="btn_saveback_Click" />
        <%=ext.buildBackCancel("BACK TO COMPETITION","CompetitionDetails.aspx?edit=1&id=" + compId, true) %>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <asp:Panel ID="panException" runat="server" CssClass="txt_red_10" Visible="false">
        <asp:Literal ID="litException" runat="server"></asp:Literal>
        <%=ext.makeSpace(20,"",true) %>
    </asp:Panel>

    <asp:Panel ID="panCompetition" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
		        <td width="200" align="left" valign="top">

                    <asp:UpdatePanel ID="up_datestart" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pan_datestart" runat="server" Visible="true">
                                <asp:TextBox ID="txtDateStartHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>START DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateStart" runat="server" ControlToValidate="txtDateStartHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                <asp:Calendar ID="tbStartDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbStartDate_SelectionChanged"></asp:Calendar>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%=ext.makeSpace(10,"",true) %>

                    <asp:DropDownList ID="drpStartTime" runat="server" CssClass="drop_standard"></asp:DropDownList>

                    <%=ext.makeSpace(10,"",true) %>

                    <asp:UpdatePanel ID="up_dateend" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pan_datesend" runat="server" Visible="true">
                                <asp:TextBox ID="txtDateEndHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>END DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateEnd" runat="server" ControlToValidate="txtDateEndHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                <asp:Calendar ID="tbEndDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbEndDate_SelectionChanged"></asp:Calendar>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%=ext.makeSpace(10,"",true) %>

                    <asp:DropDownList ID="drpEndTime" runat="server" CssClass="drop_standard"></asp:DropDownList>

                </td>

		        <td width="20" align="left" valign="top">&nbsp;</td>

		        <td align="left" valign="top">

                    <div class="txt_black_10">
                        <asp:CheckBox ID="chk_active" runat="server" CssClass="txt_red_10" Text="Activate this question" />
                    </div>

                    <%=ext.makeSpace(10,"",true) %>

                    <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>DETAILS</strong></div>

                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">QUESTION TEXT&nbsp;<asp:RequiredFieldValidator ID="reqQuestionText" runat="server" ControlToValidate="f_questiontext" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="f_questiontext" runat="server" TextMode="MultiLine" Rows="5" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">FORMAT</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:DropDownList ID="drpQuestionFormat" runat="server" CssClass="drop_standard"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">REQUIRED ENTRIES</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:DropDownList ID="drpRequiredEntries" runat="server" CssClass="drop_standard"></asp:DropDownList></td>
                        </tr>
                        <%--<tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DISCLAIMER</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="f_disclaimer" runat="server" TextMode="MultiLine" Rows="4" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>--%>

                    </table>

                    <asp:Panel ID="panQuestions" runat="server">

                        <%=ext.makeSpace(20,"",true) %>

                        <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>ANSWERS</strong></div>

                         <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	                        <tr>
		                        <td width="70" align="center" valign="top" bgcolor="#F2F2F2" class="txt_darkgrey_10"><asp:Literal ID="litCreateNew" runat="server"></asp:Literal></td>
                                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER</td>
                                <td align="center" valign="top" bgcolor="#F2F2F2" class="txt_blue_10" width="60">VOTES</td>
	                        </tr>

                            <asp:Repeater ID="repAnswerList" runat="server" OnItemDataBound="repAnswerList_OnItemDataBound"></asp:Repeater>
      
                        </table>

                    </asp:Panel>

                </td>
	        </tr>
        </table>
    </asp:Panel>

</asp:Content>


