﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="True" CodeBehind="Competitions.aspx.cs" Inherits="CompetitionTool.Competitions" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" Runat="Server">

    <%= ext.buildPageHeading("COMPETITIONS")%>

</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">

    <%=ext.makeSpace(5,"",true) %>

    <div class="wrap_interface_top" style="float:right;">
        
        <asp:Panel ID="pan_page" runat="server" DefaultButton="btn_ok">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="40" class="txt_black_10">&nbsp;PAGE</td>
                    <td width="40"><asp:TextBox CssClass="edit_page" ID="f_page" MaxLength="4" runat="server" /></td>
                    <td width="40" class="txt_black_10"> of <asp:Literal ID="lit_totalpages" runat="server"></asp:Literal></td>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle" style="padding: 0 2px 0 0;"><asp:Button ID="btn_ok" Text="OK" runat="server" CssClass="btn_grey" OnClick="btn_ok_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 2px 0 2px;"><asp:Button ID="btn_prev" Text="PREVIOUS" runat="server" CssClass="btn_grey" OnClick="btn_prev_Click" /></td>
                                <td align="center" valign="middle" style="padding: 0 0 0 2px;"><asp:Button ID="btn_next" Text="NEXT" runat="server" CssClass="btn_grey" OnClick="btn_next_Click" /></td>
                            </tr>

                            <%--<tr>
                                <td class="txt_lightblue_12" style="white-space:nowrap;">
                                    <asp:LinkButton ID="LinkButton1" Text="OK" runat="server" OnClick="btn_ok_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="LinkButton2" Text="PREVIOUS" runat="server" OnClick="btn_prev_Click" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="LinkButton3" Text="NEXT" runat="server" OnClick="btn_next_Click" />
                                </td>
                            </tr>--%>

                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

    <div class="wrap_interface_top" style="float:left">
        <asp:Panel ID="pan_search" runat="server" DefaultButton="btn_search">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="middle" class="txt_black_10">COMPETITION NAME</td>
                    <td width="10" align="left" valign="middle" class="txt_black_10">&nbsp;</td>
                    <td width="150" align="left" valign="middle"><asp:TextBox ID="f_search" MaxLength="50" runat="server" CssClass="edit_search" /></td>
                    <td width="20" align="left" valign="middle">&nbsp;</td>
                    <td align="left" valign="middle"><asp:CheckBox ID="f_activeonly" runat="server" Text="SHOW ACTIVE ONLY" CssClass="txt_black_10" /></td>
                    <td width="20" align="left" valign="middle">&nbsp;</td>
                    <td align="left" valign="middle"><asp:Button ID="btn_search" Text="SEARCH" runat="server" CssClass="btn_grey" OnClick="btn_search_Click" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>

    <div class="wrap_interface_top" style="float:left">
        <asp:Panel ID="pan_status" runat="server" DefaultButton="btn_search">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="middle" class="txt_black_10" style="text-transform: uppercase;">
                        
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>

    <%=ext.makeSpace(10,"",true) %>

    <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	    <tr>
		    <td width="65" align="center" valign="top" bgcolor="#F2F2F2" class="txt_darkgrey_10" style="white-space:nowrap;"><a href="CompetitionDetails.aspx?edit=0">CREATE</a></td>
            <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NAME</td>
            <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">URL NAME</td>
		    <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">START</td>
		    <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">END</td>
	    </tr>

        <asp:Repeater ID="rep_list" runat="server" OnItemDataBound="rep_list_OnItemDataBound"></asp:Repeater>
      

    </table>

</asp:Content>
