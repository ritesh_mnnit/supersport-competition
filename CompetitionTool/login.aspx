﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master2.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CompetitionTool.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp_pageheading" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cp_pagedata" runat="server">

    <%= ext.makeSpace(50, "", true) %>

    <div class="txt_white_12" style="margin:0 auto; width:292px; text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>LOGIN</strong></div>

     <table width="300px" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC" align="center"  style="margin:0 auto;">
        <tr>
            <td colspan="2"   bgcolor="#FFFFFF" align="center"><asp:Label ID="lbErrorMessage" runat="server" ForeColor="Red" Text="Login failed" Visible="false"></asp:Label></td>
        </tr>
	    <tr>
            <td width="50" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">Username</td><td bgcolor="#FFFFFF"><asp:TextBox id="tbUsername" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox> </td>
        </tr>
        <tr>
            <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">Password</td><td bgcolor="#FFFFFF"><asp:TextBox ID="tbPassword" runat="server" TextMode="Password" CssClass="edit_stretch_noborder"></asp:TextBox></td>
	    </tr>
        <tr>
            <td colspan="2"  bgcolor="#FFFFFF" align="center"><asp:Button ID="btLogin" runat="server" Text="LOGIN" 
                    CssClass="btn_grey" onclick="btLogin_Click" /></td>
        </tr>
    </table>

</asp:Content>
