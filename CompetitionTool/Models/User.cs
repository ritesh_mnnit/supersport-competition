﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompetitionTool.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}