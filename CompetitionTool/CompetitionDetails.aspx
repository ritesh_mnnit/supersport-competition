﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="CompetitionDetails.aspx.cs" Inherits="CompetitionTool.CompetitionDetails" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ID="con_pageheading" ContentPlaceHolderID="cp_pageheading" runat="Server">

    <%= ext.buildPageHeading("COMPETITION DETAILS")%>
</asp:Content>

<asp:Content ID="con_pagedata" ContentPlaceHolderID="cp_pagedata" runat="server">

    <asp:ScriptManager ID="sm_competitiondetails" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top" style="float: left;">
        <asp:Button runat="server" ID="btn_save" CssClass="btn_grey" Text="SAVE DETAILS" OnClick="btn_save_Click" />
        <asp:Button runat="server" ID="btn_saveback" CssClass="btn_grey" Text="SAVE AND GO BACK TO LIST" OnClick="btn_saveback_Click" />
        <%=ext.buildBackCancel("CANCEL","Competitions.aspx", true) %>
    </div>
    <div class="wrap_interface_top" style="float: right; margin-left: 4px;">
        <asp:Button runat="server" ID="btoptWinners" CssClass="btn_grey" Text="EXPORT COMPETITION OPT-IN" OnClick="btWinners_Click" />
        <asp:Button runat="server" ID="btoptcompwinner" CssClass="btn_grey" Text="EXPORT MULTICHOICE OPT-IN" OnClick="btmultichoiceWinners_Click" />
        <%=ext.buildBackCancel("CANCEL","Competitions.aspx", true) %>
        <asp:Panel ID="pnlStats" runat="server" Visible="false">
            <%=ext.buildBackCancel("WINNERS","WinnerList.aspx?compid=" + id, true) %>
            <%=ext.buildBackCancel("ENTRIES", "EntryList.aspx?compid=" + id, true)%>
            <%=ext.buildBackCancel("STATS", "CompetitionStats.aspx?compid=" + id, true)%>
            <%=ext.buildBackCancel("BACK TO LIST","Competitions.aspx", true) %>
            <input type="button" class="btn_grey" onclick="window.open('http://www.supersport.com/competitions/<%= urlname %>    ?adminView=true');" value="PREVIEW WEB" />
            <input type="button" class="btn_grey" onclick="window.open('http://mobi.supersport.com/competitions/<%= urlname %>    ?adminView=true');" value="PREVIEW MOBI" />
        </asp:Panel>
    </div>

    <%=ext.makeSpace(20,"",true) %>

    <asp:Panel ID="panException" runat="server" CssClass="txt_red_10" Visible="false">
        <asp:Literal ID="litException" runat="server"></asp:Literal>
        <%=ext.makeSpace(20,"",true) %>
    </asp:Panel>

    <asp:Panel ID="panCompetition" runat="server">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="200" align="left" valign="top">

                    <asp:UpdatePanel ID="up_datestart" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pan_datestart" runat="server" Visible="true">
                                <asp:TextBox ID="f_datestartHIDDEN" runat="server" CssClass="hiddenField" ValidationGroup="valGroup1"></asp:TextBox>
                                <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>START DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateStart" runat="server" ControlToValidate="f_datestartHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                <asp:Calendar ID="cal_datestart" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" OnSelectionChanged="f_datestart_SelectionChanged"></asp:Calendar>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%=ext.makeSpace(10,"",true) %>

                    <asp:UpdatePanel ID="up_dateend" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pan_datesend" runat="server" Visible="true">
                                <asp:TextBox ID="f_dateendHIDDEN" runat="server" CssClass="hiddenField" ValidationGroup="valGroup1"></asp:TextBox>
                                <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>END DATE</strong>&nbsp;<asp:RequiredFieldValidator ID="reqDateEnd" runat="server" ControlToValidate="f_dateendHIDDEN" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></div>
                                <asp:Calendar ID="cal_dateend" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" OnSelectionChanged="f_dateend_SelectionChanged"></asp:Calendar>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

                <td width="20" align="left" valign="top">&nbsp;</td>

                <td width="200" align="left" valign="top">

                    <div class="txt_black_10">
                        <asp:CheckBox ID="chk_multipleentries" runat="server" Text="Allow multiple entries per day" /><br />
                        <asp:CheckBox ID="chk_requirelogon" runat="server" Text="User login required" /><br />
                        <asp:CheckBox ID="chk_feature" runat="server" Text="Feature this competition" /><br />
                        <asp:CheckBox ID="chk_active" runat="server" CssClass="txt_red_10" Text="Activate this competition" />
                    </div>

                    <%=ext.makeSpace(10,"",true) %>

                    <asp:UpdatePanel ID="up_countries" runat="server">
                        <ContentTemplate>

                            <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>COUNTRIES</strong></div>

                            <div class="txt_lightblue_10" style="padding: 3px; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;">

                                <asp:LinkButton ID="btn_wholeworld" runat="server" Text="&bull;&nbsp;SELECT WHOLE WORLD" OnClick="btn_wholeworld_Click" ValidationGroup="valGroup2"></asp:LinkButton><br />
                                <asp:LinkButton ID="btn_subsaharan" runat="server" Text="&bull;&nbsp;SUB-SAHARAN AFRICA" OnClick="btn_subsaharan_Click" ValidationGroup="valGroup2"></asp:LinkButton><br />
                                <asp:LinkButton ID="btn_deselect" runat="server" Text="&bull;&nbsp;DE-SELECT ALL COUNTRIES" OnClick="btn_deselect_Click" ValidationGroup="valGroup2"></asp:LinkButton>

                            </div>

                            <div id="div_countries" style="border: #CCCCCC solid 1px; left: 5; top: 7px; width: 199; height: 207px; overflow: scroll;">
                                <asp:CheckBoxList ID="chk_countries" runat="server" CssClass="txt_black_10" ValidationGroup="valGroup2"></asp:CheckBoxList>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>

                <td width="20" align="left" valign="top">&nbsp;</td>

                <td align="left" valign="top">

                    <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>DETAILS</strong></div>

                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COMPETITION NAME&nbsp;<asp:RequiredFieldValidator ID="reqCompName" runat="server" ControlToValidate="f_compname" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_compname" runat="server" CssClass="edit_stretch_noborder" ValidationGroup="valGroup1"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">URL NAME&nbsp;<asp:RequiredFieldValidator ID="reqUrlName" runat="server" ControlToValidate="f_urlname" Text="#" CssClass="txt_red_10"></asp:RequiredFieldValidator></td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_urlname" runat="server" CssClass="edit_stretch_noborder" ValidationGroup="valGroup1"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EXTERNAL URL&nbsp;(WEB)</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_exturl" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EXTERNAL URL&nbsp;(MOBI)</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_mobiexturl" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">PRIZES</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_prizes" runat="server" TextMode="MultiLine" Rows="3" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DISCLAIMER</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_disclaimer" runat="server" TextMode="MultiLine" Rows="3" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ENTRY RESPONSE</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_response" runat="server" TextMode="MultiLine" Rows="3" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">BLURB</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_optinmessage" runat="server" TextMode="MultiLine" Rows="3" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COMPETITION OPT-IN</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:CheckBox runat="server" ID="chk_optincheckbox" /></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">MULTICHOICE OPT-IN</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:CheckBox runat="server" ID="chk_multichoiceoptincheckbox" /></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">MARKETING MESSAGE</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_marketingmessage" runat="server" TextMode="MultiLine" Rows="3" CssClass="edit_stretch_noborder"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SPORT</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:DropDownList runat="server" ID="ddl_competitionsport"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">VIDEO CATEGORY URL</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:TextBox ID="f_videocategoryurl" runat="server" TextMode="SingleLine" CssClass="edit_stretch_noborder"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">RICH MEDIA COMPETITION</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:CheckBox runat="server" ID="chk_richmedia" /></td>
                        </tr>
                         <tr>
                            <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">AGE VERIFICATION REQUIRED</td>
                            <td align="left" valign="top" bgcolor="#FFFFFF">
                                <asp:CheckBox runat="server" ID="chk_ageverificationrequired" /></td>
                        </tr>

                    </table>

                    <%=ext.makeSpace(20,"",true) %>

                    <asp:UpdatePanel ID="updImages" runat="server">
                        <ContentTemplate>

                            <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>GRAPHICS</strong></div>

                            <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
                                <tr>
                                    <td width="80" align="left" bgcolor="#F2F2F2" class="txt_blue_10">
                                        <label>WEB HEADER</label></td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelWebHeader" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnDelWebHeader_Click"></asp:LinkButton></td>
                                    <td align="left" bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_webheader" runat="server"></asp:Literal></td>
                                    <td width="200" align="left" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_webheader" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="80" bgcolor="#F2F2F2" class="txt_blue_10">WEB FOOTER</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelWebFooter" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnDelWebFooter_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_webfooter" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_webfooter" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="171" bgcolor="#F2F2F2" class="txt_blue_10">Live Video Streaming Replacement</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="lnk_Del_LiveVideoStreamingReplacement" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="lnk_LiveVideoStreamingReplacement_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_LiveVideoStreamingReplacement" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="FileUpload_lnk_LiveVideoStreamingReplacement" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="80" bgcolor="#F2F2F2" class="txt_blue_10">MOBI HEADER</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelMobiHeader" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnDelMobiHeader_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_mobiheader" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_mobiheader" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="80" bgcolor="#F2F2F2" class="txt_blue_10">MOBI FOOTER</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelMobiFooter" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnDelMobiFooter_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_mobifooter" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_mobifooter" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="80" bgcolor="#F2F2F2" class="txt_blue_10">DESKTOP THUMBNAIL</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelThumbNail" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnThumbNail_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_thumbnail" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_thumbnail" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                                <tr align="left">
                                    <td width="80" bgcolor="#F2F2F2" class="txt_blue_10">MOBILE THUMBNAIL</td>
                                    <td width="30" align="center" bgcolor="#F2F2F2" class="txt_red_10">
                                        <asp:LinkButton ID="btnDelMobileThumbnail" runat="server" Text="DEL" Visible="false" CssClass="txt_red_10" OnClick="btnDelMobileThumbnail_Click"></asp:LinkButton></td>
                                    <td bgcolor="#FFFFFF" class="txt_black_10">
                                        <asp:Literal ID="lit_mobilethumbnail" runat="server"></asp:Literal></td>
                                    <td width="200" bgcolor="#FFFFFF">
                                        <asp:FileUpload ID="upl_mobilethumbnail" runat="server" CssClass="txt_black_10" Width="100%" BackColor="#F2F2F2" BorderStyle="Solid" BorderColor="#CCCCCC" BorderWidth="1" /></td>
                                </tr>
                            </table>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="panQuestions" runat="server">

        <%=ext.makeSpace(20,"",true) %>

        <div class="txt_white_12" style="text-align: left; padding: 3px; background-color: #CCC; border-top: 1px solid #CCC; border-left: 1px solid #CCC; border-right: 1px solid #CCC;"><strong>QUESTIONS</strong></div>

        <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
            <tr>
                <td width="70" align="center" valign="top" bgcolor="#F2F2F2" class="txt_darkgrey_10">
                    <asp:Literal ID="litCreateNew" runat="server"></asp:Literal></td>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">QUESTION</td>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">FORMAT</td>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">START</td>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">END</td>
                <%--<td align="left" valign="top" bgcolor="#F2F2F2">LAST MODIFIED</td>--%>
            </tr>

            <asp:Repeater ID="repQuestionList" runat="server" OnItemDataBound="repQuestionList_OnItemDataBound"></asp:Repeater>

        </table>

        <div class="txt_blue_10 printlink">
            <asp:HyperLink ID="lnkprint" runat="server" Text="Print version" />
        </div>

    </asp:Panel>

    <asp:Panel ID="pnlNoQuestions" runat="server" Visible="false">
        <div class="txt_red_10" style="padding: 20px; text-align: center;">
            NO QUESTIONS CREATED
        </div>
    </asp:Panel>

</asp:Content>


