﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool
{
    public partial class WinnersListPrint : System.Web.UI.Page
    {
        public int compId;
        public string competitionName = string.Empty;
        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["compId"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compId"]);
            }

            comp = Competition.GetCompetition(compId);
            ltl1.Text = comp.Name;
            renderPreviousWinners();
        }

        private void renderPreviousWinners()
        {
            if ((comp != null) && (comp.CompetitionWinners != null))
            {
                competitionName = comp.Name;
                rpCompetitionWinners.DataSource = comp.CompetitionWinners;
                rpCompetitionWinners.DataBind();
            }
        }

        protected void rpCompetitionWinners_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                CompetitionWinner item = (CompetitionWinner)e.Item.DataItem;

                ((Label)e.Item.FindControl("lbName")).Text = item.EntryDetails.Name;
                ((Label)e.Item.FindControl("lbSurname")).Text = item.EntryDetails.Surname;
                ((Label)e.Item.FindControl("lbContactNumber")).Text = item.EntryDetails.ContactNumber;
                ((Label)e.Item.FindControl("lbIdNumber")).Text = item.EntryDetails.IdNumber;
                ((Label)e.Item.FindControl("lbEmail")).Text = item.EntryDetails.Email;
                ((Label)e.Item.FindControl("lbCountry")).Text = item.EntryDetails.CountryCode;
                ((Label)e.Item.FindControl("lbTown")).Text = item.EntryDetails.City;
                ((Label)e.Item.FindControl("lbAnswer")).Text = item.EntryDetails.Answer;
                ((Label)e.Item.FindControl("lbDateDrawn")).Text = item.CreatedDate.ToString("dd MMM yyyy HH:mm");
                ((Label)e.Item.FindControl("lbDateEntered")).Text = item.EntryDetails.DateEntered.ToString("dd MMM yyyy HH:mm");
                ((CheckBox)e.Item.FindControl("chkConfirmedWinner")).Checked = item.ClaimedWinner;
                ((Label)e.Item.FindControl("lbComment")).Text = item.Comment;
            }
        }
    }
}