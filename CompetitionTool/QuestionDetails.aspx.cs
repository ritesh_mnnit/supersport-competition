﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using SuperSport.CompetitionLib.Models;
using SuperSport.CompetitionLib.Exceptions;

namespace CompetitionTool
{
    public partial class QuestionDetails : System.Web.UI.Page
    {
        public int id;
        public int compId;
        public int edit;
        public string competitionName = string.Empty;

        private Question selectedQuestion;
        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["id"], "int"))
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (ext.isNumericType(Request.QueryString["compid"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compid"]);
                comp = Competition.GetCompetition(compId);
                competitionName = comp.Name;
            }

            if (ext.isNumericType(Request.QueryString["edit"], "int"))
            {
                edit = Convert.ToInt32(Request.QueryString["edit"]);
            }

            if (edit == 1)
            {
                List<Question> compQuestions = Question.GetQuestions(compId);
                selectedQuestion = compQuestions.Find(q => { return q.Id == id; });
            }

            if (!Page.IsPostBack)
            {
                buildFormatList();
                buildRequiredEntriesList();
                buildTimeDrop(drpStartTime, 15);
                buildTimeDrop(drpEndTime, 15);

                if (edit == 1)
                {
                    List<Question> compQuestions = Question.GetQuestions(compId);
                    Question thisQuestion = compQuestions.Find(q => { return q.Id == id; });

                    if (thisQuestion != null)
                    {
                        tbStartDate.SelectedDate = Convert.ToDateTime(thisQuestion.DateStart.ToString("yyyy-MM-dd"));
                        tbStartDate.VisibleDate = Convert.ToDateTime(thisQuestion.DateStart.ToString("yyyy-MM-dd"));
                        drpStartTime.SelectedValue = thisQuestion.DateStart.ToString("HH:mm");
                        txtDateStartHIDDEN.Text = thisQuestion.DateStart.ToString("yyyy-MM-dd");
                        tbEndDate.SelectedDate = Convert.ToDateTime(thisQuestion.DateEnd.ToString("yyyy-MM-dd"));
                        tbEndDate.VisibleDate = Convert.ToDateTime(thisQuestion.DateEnd.ToString("yyyy-MM-dd"));
                        drpEndTime.SelectedValue = thisQuestion.DateEnd.ToString("HH:mm");
                        txtDateEndHIDDEN.Text = thisQuestion.DateEnd.ToString("yyyy-MM-dd");
                        
                        f_questiontext.Text = thisQuestion.QuestionText;

                        chk_active.Checked = thisQuestion.Active;

                        drpQuestionFormat.SelectedValue = thisQuestion.QuestionFormat.ToString();
                        drpRequiredEntries.SelectedValue = thisQuestion.RequiredAnswersTotal.ToString();

                        litCreateNew.Text = "<a href=\"AnswerDetails.aspx?edit=0&quesid="+ id +"&compid="+ compId +"\">CREATE</a>";

                        repAnswerList.DataSource = thisQuestion.Answers;
                        repAnswerList.DataBind();
                    }
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            saveRecord(false);
        }

        protected void btn_saveback_Click(object sender, EventArgs e)
        {
            saveRecord(true);
        }

        protected void saveRecord(bool goBackToList)
        {
            Question newQuestion = new Question();

            if (edit == 1)
            {
                newQuestion = selectedQuestion;
            }

            newQuestion.CompetitionId = compId;
            newQuestion.DateStart = Convert.ToDateTime(txtDateStartHIDDEN.Text + " " + drpStartTime.SelectedValue);
            newQuestion.DateEnd = Convert.ToDateTime(txtDateEndHIDDEN.Text + " " + drpEndTime.SelectedValue);
            newQuestion.QuestionText = f_questiontext.Text;
            newQuestion.Active = chk_active.Checked;

            newQuestion.QuestionFormat = (Question.QuestionType)Enum.Parse(typeof(Question.QuestionType), drpQuestionFormat.SelectedValue);
            newQuestion.RequiredAnswersTotal = Convert.ToInt32(drpRequiredEntries.SelectedValue);

            try
            {
                newQuestion.Save();

                if (goBackToList == true)
                {
                    Response.Redirect("CompetitionDetails.aspx?edit=1&id=" + newQuestion.CompetitionId);
                }
                else
                {
                    Response.Redirect("QuestionDetails.aspx?edit=1&id=" + newQuestion.Id + "&compid=" + compId);
                }
            }
            catch (ValidationException ex)
            {
                litException.Text = "";

                foreach (ValidationMessage item in ex.ValidationList)
                {
                    litException.Text += item.ErrorMessage + "<br />";
                }
                panException.Visible = true;
            }
        }

        protected void buildFormatList()
        {
            drpQuestionFormat.DataSource = Enum.GetNames(typeof(Question.QuestionType));
            drpQuestionFormat.DataBind();

            drpQuestionFormat.SelectedValue = Question.QuestionType.SingleSelection.ToString();
        }

        protected void buildRequiredEntriesList()
        {
            for(int i =1;i< 15;i++)
            {
                drpRequiredEntries.Items.Add(i.ToString());
            }
        }

        protected void tbStartDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateStartHIDDEN, tbStartDate);
        }

        protected void tbEndDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateEndHIDDEN, tbEndDate);
        }

        protected void changeDateVal(TextBox hiddenDate, Calendar calDate)
        {
            hiddenDate.Text = calDate.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void buildTimeDrop(DropDownList dropList, int interval)
        {
            ext.time_list list = new ext.time_list();
            List<ext.time> timeList = new List<ext.time>();

            timeList = list.getTimeList(interval);

            dropList.DataSource = timeList;
            dropList.DataValueField = "timeval";
            dropList.DataTextField = "timeval";

            dropList.DataBind();
        }

        protected void repAnswerList_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                SuperSport.CompetitionLib.Models.Answer item = (SuperSport.CompetitionLib.Models.Answer)e.Item.DataItem;

                HtmlGenericControl wrapList = new HtmlGenericControl("div");

                StringBuilder outStr = new StringBuilder();

                string rowCol = "txt_black_10";

                if (item.Active == false)
                {
                    rowCol = "txt_red_10";
                }

                outStr.Append("<tr style=\"background-color:#FFFFFF;\" id=\"row_" + item.Id + "\" onmouseover=\"ChangeRowColour(this.id,'#F2F2F2')\" onmouseout=\"ChangeRowColour(this.id,'#FFFFFF')\">");

                outStr.Append("    <td align=\"center\" valign=\"top\" bgcolor=\"#F2F2F2\">");
                outStr.Append("        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
                outStr.Append("	            <tr>");
                outStr.Append("		            <td class=\"txt_black_10\" style=\"padding:0 3px 0 0\"><a href=\"AnswerDetails.aspx?edit=1&id=" + item.Id + "&quesid="+ id +"&compid=" + compId + "\">OPEN / EDIT</a></td>");
                //outStr.Append("		        <td class=\"txt_red_10\" style=\"padding:0 0 0 3px\"><a href=\"xxx\">DELETE</a></td>");
                outStr.Append("	            </tr>");
                outStr.Append("        </table>");
                outStr.Append("    </td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.AnswerText + "</td>");
                outStr.Append("    <td align=\"center\" valign=\"top\" class=\"" + rowCol + "\">" + item.GetVotes() + "</td>");
                outStr.Append("</tr>");

                wrapList.InnerHtml = outStr.ToString();

                e.Item.Controls.Add(wrapList);
            }
        }
    }
}