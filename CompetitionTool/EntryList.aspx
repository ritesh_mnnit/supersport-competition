﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="EntryList.aspx.cs" Inherits="CompetitionTool.EntryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp_pageheading" runat="server">

    <%= ext.buildPageHeading("COMPETITION ENTRIES - " + competitionName)%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cp_pagedata" runat="server">

    <asp:ScriptManager ID="sm_entrylist" runat="server"></asp:ScriptManager>

    <div class="wrap_interface_top">
                
        <%=ext.buildBackCancel("BACK TO COMPETITION","CompetitionDetails.aspx?edit=1&id=" + compId, true) %>

    </div>

    <%=ext.makeSpace(20,"",true) %>

    <div>   
        <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>SEARCH DETAILS</strong></div>

        <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#CCCCCC">

            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATES&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF">

                    <table>
                        <tr>
                            <td width="200">
                                <asp:UpdatePanel ID="up_datestart" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datestart" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateStartHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>START DATE</strong></div>
                                            <asp:Calendar ID="tbStartDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbStartDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpStartTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                            <td width="1"></td>
                            <td width="200">
                                <asp:UpdatePanel ID="up_dateend" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="pan_datesend" runat="server" Visible="true">
                                            <asp:TextBox ID="txtDateEndHIDDEN" runat="server" CssClass="hiddenField"></asp:TextBox>
                                            <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>END DATE</strong></div>
                                            <asp:Calendar ID="tbEndDate" runat="server" Width="100%" CssClass="txt_black_10" BorderColor="#cccccc" DayHeaderStyle-BackColor="#F2F2F2" TitleStyle-BackColor="#F2F2F2" ShowGridLines="true" onselectionchanged="tbEndDate_SelectionChanged"></asp:Calendar>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <%=ext.makeSpace(10,"",true) %>

                                <asp:DropDownList ID="drpEndTime" runat="server" CssClass="drop_standard"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="120" align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbAnswer" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>

            <tr>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NAME&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbName" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SURNAME&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbSurname" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONTACT NUMBER&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbContactNumber" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">E-MAIL&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><asp:TextBox ID="tbEmail" runat="server" CssClass="edit_stretch_noborder"></asp:TextBox></td>
            </tr>

        </table>
        <%=ext.makeSpace(10,"",true) %>

        <asp:button runat="server" ID="btnSearch" CssClass="btn_grey" Text="SEARCH" onclick="btnSearch_Click"  />
    </div>

    <%=ext.makeSpace(20,"",true) %>

    <asp:Panel ID="pnSearchResults" runat="server" Visible="false">
    <div class="txt_black_12" style="margin-bottom:5px;">
        <div><strong>Displaying page </strong><asp:Label ID="lbCurrentPage" runat="server"></asp:Label> <strong> of </strong> <asp:Label ID="lbTotalPages" runat="server"></asp:Label> <strong> pages. Total results:</strong> <asp:Label ID="lbTotalResults" runat="server"></asp:Label></div>
        <asp:LinkButton ID="btnPrev" runat="server" Text="<< Prev" 
            onclick="btnPrev_Click"></asp:LinkButton> <asp:LinkButton ID="btnNext" 
            runat="server" Text="Next >>" onclick="btnNext_Click"></asp:LinkButton>
    </div>
    <div>   
        <div class="txt_white_12" style="text-align:left; padding:3px; background-color:#CCC; border-top:1px solid #CCC; border-left:1px solid #CCC; border-right:1px solid #CCC;"><strong>SEARCH RESULTS</strong></div>
        <asp:Repeater ID="rpCompetitionEntries" runat="server" OnItemDataBound="rpCompetitionEntries_OnItemDataBound">
            <HeaderTemplate>
                 <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
	                <tr>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">NAME</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SURNAME</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONTACT NUMBER</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ID NUMBER</td>
		                <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">EMAIL</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">COUNTRY</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CITY</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">SOURCE</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">DATE ENTERED</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">CONNECT ID</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">OPTED IN</td>
                        <td align="left" valign="top" bgcolor="#F2F2F2" class="txt_blue_10">ANSWER</td>
	                </tr>
            </HeaderTemplate>
            <ItemTemplate>
	                <tr runat="server" style="background-color:#FFFFFF;" id="XXX" onmouseover="ChangeRowColour(this.id,'#F2F2F2')" onmouseout="ChangeRowColour(this.id,'#FFFFFF')">
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbName" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbSurname" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbContactNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbIdNumber" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbEmail" runat="server"></asp:Label></td>                        
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbCountry" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lblCity" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbSource" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbDateEntered" runat="server"></asp:Label></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbConnectId" runat="server"></asp:Label></td>                    
                        <td align="left" valign="top" class="txt_black_10"><asp:CheckBox ID="chkOptIn" runat="server" /></td>
                        <td align="left" valign="top" class="txt_black_10"><asp:Label ID="lbAnswer" runat="server"></asp:Label></td>                    
	                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>                
            </FooterTemplate>
        </asp:Repeater>

    </div>
    </asp:Panel>
</asp:Content>
