function gotoURL(url)
	{
		window.location=url;
	}
	
function dropMenu(menuToDrop,state)
    {
	    document.getElementById(menuToDrop).style.display = state;
    }

function deleteEntry(type, id, curPage)
{
    if (confirm("Are you sure you want to permanently delete that entry?")) 
    {
        gotoURL('delete_entry.aspx?type=' + type + '&id=' + id + '&page=' + curPage);
    }
}

function ShowHideDiv(elementID)
{
    switch (document.getElementById(elementID).style.display) 
    {
        case "block":
            document.getElementById(elementID).style.display = "none";
            break;

        case "none":
            document.getElementById(elementID).style.display = "block";
            break;
    }
}

function ChangeRowColour(rowID,newColour)	
{
	window.document.getElementById(rowID).style.backgroundColor = newColour;
}