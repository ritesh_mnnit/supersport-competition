﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.BLL;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool.Competitions
{
    public partial class ListCompetitions : System.Web.UI.Page
    {
        private static readonly SuperSport.CompetitionLib.BLL.Competition comp = new SuperSport.CompetitionLib.BLL.Competition();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<SuperSport.CompetitionLib.Models.Competition> comps = comp.GetCompetitions("comp", false);

            grdListComps.DataSource = comps;
            grdListComps.DataBind();

        }
    }
}