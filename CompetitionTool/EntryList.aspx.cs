﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;

namespace CompetitionTool
{
    public partial class EntryList : System.Web.UI.Page
    {
        public int compId;
        public string competitionName = string.Empty;
        private int currentPage = 1;

        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["compId"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compId"]);
            }

            if ((ViewState != null) && (ViewState["currentPage"] != null))
            {
                currentPage = Convert.ToInt32(ViewState["currentPage"]);
            }

            comp = Competition.GetCompetition(compId);
            if (comp != null)
            {
                competitionName = comp.Name;
            }

            if (!Page.IsPostBack)
            {
                buildTimeDrop(drpStartTime, 15);
                buildTimeDrop(drpEndTime, 15);
            }   
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ViewState["currentPage"] = 1;
            RenderResults();
        }

        private void RenderResults()
        {
            pnSearchResults.Visible = true;
            DateTime startDate;
            DateTime endDate;

            if (!string.IsNullOrEmpty(txtDateStartHIDDEN.Text))
            {
                startDate = Convert.ToDateTime(txtDateStartHIDDEN.Text + " " + drpStartTime.SelectedValue);
            }
            else
            {
                startDate = Convert.ToDateTime("1/1/1753 12:00:00");
            }

            if (!string.IsNullOrEmpty(txtDateEndHIDDEN.Text))
            {
                endDate = Convert.ToDateTime(txtDateEndHIDDEN.Text + " " + drpEndTime.SelectedValue);
            }
            else
            {
                endDate = DateTime.MaxValue;
            }

            List<SuperSport.CompetitionLib.Models.CompetitionEntry> entries = comp.SearchEntries(100, tbName.Text, tbSurname.Text, tbContactNumber.Text, tbEmail.Text, startDate, endDate);
            
            PagedDataSource pageDs = new PagedDataSource();
            pageDs.DataSource = entries;
            pageDs.AllowPaging = true;
            pageDs.CurrentPageIndex = currentPage - 1;
            pageDs.PageSize = 100;

            btnPrev.Visible = true;
            btnNext.Visible = true;

            if (pageDs.IsFirstPage)
            {
                btnPrev.Visible = false;
            }
            if (pageDs.IsLastPage)
            {
                btnNext.Visible = false;
            }

            lbCurrentPage.Text = currentPage.ToString();
            lbTotalPages.Text = pageDs.PageCount.ToString();
            lbTotalResults.Text = entries.Count.ToString();

            rpCompetitionEntries.DataSource = pageDs;
            rpCompetitionEntries.DataBind();

        }

        protected void rpCompetitionEntries_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                SuperSport.CompetitionLib.Models.CompetitionEntry item = (SuperSport.CompetitionLib.Models.CompetitionEntry)e.Item.DataItem;

                ((Label)e.Item.FindControl("lbName")).Text = item.Name;
                ((Label)e.Item.FindControl("lbSurname")).Text = item.Surname;
                ((Label)e.Item.FindControl("lbContactNumber")).Text = item.ContactNumber;
                ((Label)e.Item.FindControl("lbIdNumber")).Text = item.IdNumber;
                ((Label)e.Item.FindControl("lbEmail")).Text = item.Email;
                ((Label)e.Item.FindControl("lbConnectId")).Text = item.ConnectId;
                ((Label)e.Item.FindControl("lbCountry")).Text = item.CountryCode;
                ((Label)e.Item.FindControl("lblCity")).Text = item.City;
                ((Label)e.Item.FindControl("lbSource")).Text = item.Source.ToString();
                ((CheckBox)e.Item.FindControl("chkOptIn")).Checked = item.ReceiveMarketing;
                ((Label)e.Item.FindControl("lbDateEntered")).Text = item.DateEntered.ToString("dd MMM yyyy HH:mm");
                ((Label)e.Item.FindControl("lbAnswer")).Text = item.Answer;
            }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            currentPage -= 1;
            ViewState["currentPage"] = currentPage;

            RenderResults();
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            currentPage += 1;
            ViewState["currentPage"] = currentPage;

            RenderResults();
        }

        protected void tbStartDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateStartHIDDEN, tbStartDate);
        }

        protected void tbEndDate_SelectionChanged(object sender, EventArgs e)
        {
            changeDateVal(txtDateEndHIDDEN, tbEndDate);
        }

        protected void changeDateVal(TextBox hiddenDate, Calendar calDate)
        {
            hiddenDate.Text = calDate.SelectedDate.ToString("yyyy-MM-dd");
        }

        protected void buildTimeDrop(DropDownList dropList, int interval)
        {
            ext.time_list list = new ext.time_list();
            List<ext.time> timeList = new List<ext.time>();

            timeList = list.getTimeList(interval);

            dropList.DataSource = timeList;
            dropList.DataValueField = "timeval";
            dropList.DataTextField = "timeval";

            dropList.DataBind();
        }
    }
}