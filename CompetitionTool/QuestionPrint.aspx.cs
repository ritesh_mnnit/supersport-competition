﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CompetitionLib.Models;
using System.Web.UI.HtmlControls;
using System.Text;

namespace CompetitionTool
{
    public partial class QuestionPrint : System.Web.UI.Page
    {
        public int compId;
        public string competitionName = string.Empty;
        private Competition comp;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ext.isNumericType(Request.QueryString["compId"], "int"))
            {
                compId = Convert.ToInt32(Request.QueryString["compId"]);
            }

            comp = Competition.GetCompetition(compId);

            repQuestionList.DataSource = comp.Questions;
            repQuestionList.DataBind();
        }

        protected void repQuestionList_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                SuperSport.CompetitionLib.Models.Question item = (SuperSport.CompetitionLib.Models.Question)e.Item.DataItem;

                HtmlGenericControl wrapList = new HtmlGenericControl("div");

                StringBuilder outStr = new StringBuilder();

                string rowCol = "txt_black_10";

                if (item.Active == false)
                {
                    rowCol = "txt_red_10";
                }

                outStr.Append("<tr style=\"background-color:#FFFFFF;\" id=\"row_" + item.Id + "\" onmouseover=\"ChangeRowColour(this.id,'#F2F2F2')\" onmouseout=\"ChangeRowColour(this.id,'#FFFFFF')\">");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.QuestionText + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.QuestionFormat + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateStart.ToString("dd MMMM yyyy") + "</td>");
                outStr.Append("    <td align=\"left\" valign=\"top\" class=\"" + rowCol + "\">" + item.DateEnd.ToString("dd MMMM yyyy") + "</td>");
                //outStr.Append("    <td align=\"left\" valign=\"top\" bgcolor=\"#FFFFFF\" class=\""+ rowCol +"\">Modified XXXX</td>");
                outStr.Append("</tr>");

                wrapList.InnerHtml = outStr.ToString();

                e.Item.Controls.Add(wrapList);
            }
        }
    }
}